/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PhanQuangHuy59
 */
public class Slider {
    private int id_Slider;
    private String title_Slider;
    private String thumbnail_Image;
    private Date create_Date;
    private Date update_Date;
    private boolean status;
    private String notes;
    private String backlink;
    private String content_Slider;
    private int id_User;
   public Slider() {
    }

    public Slider(int id_Slider, String title_Slider, String thumbnail_Image, Date update_Date, boolean status, String notes, String backlink, String content_Slider, int id_User) {
        this.id_Slider = id_Slider;
        this.title_Slider = title_Slider;
        this.thumbnail_Image = thumbnail_Image;
        this.update_Date = update_Date;
        this.status = status;
        this.notes = notes;
        this.backlink = backlink;
        this.content_Slider = content_Slider;
        this.id_User = id_User;
    }
    
    public Slider(int id_Slider, String title_Slider, String thumbnail_Image, Date create_Date, Date update_Date, boolean status, String notes, String backlink, String content_Slider, int id_User) {
        this.id_Slider = id_Slider;
        this.title_Slider = title_Slider;
        this.thumbnail_Image = thumbnail_Image;
        this.create_Date = create_Date;
        this.update_Date = update_Date;
        this.status = status;
        this.notes = notes;
        this.backlink = backlink;
        this.content_Slider = content_Slider;
        this.id_User = id_User;
    }

    

    public int getId_Slider() {
        return id_Slider;
    }

    public void setId_Slider(int id_Slider) {
        this.id_Slider = id_Slider;
    }

    public String getTitle_Slider() {
        return title_Slider;
    }

    public void setTitle_Slider(String title_Slider) {
        this.title_Slider = title_Slider;
    }

    public String getThumbnail_Image() {
        return thumbnail_Image;
    }

    public void setThumbnail_Image(String thumbnail_Image) {
        this.thumbnail_Image = thumbnail_Image;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getBacklink() {
        return backlink;
    }

    public void setBacklink(String backlink) {
        this.backlink = backlink;
    }

    public String getContent_Slider() {
        return content_Slider;
    }

    public void setContent_Slider(String content_Slider) {
        this.content_Slider = content_Slider;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }
    
}
