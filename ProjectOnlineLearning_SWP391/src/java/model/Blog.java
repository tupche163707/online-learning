/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.io.Serializable;
import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author PhanQuangHuy59
 */
public class Blog implements Serializable{

    private int id_Blog;
    private String thumbnail_Blog;
    private String title;
    private String content_Blog;
    private String brief_Infor_Blog;
    private Date update_Date;
    private int id_User;
    private int view;
    private int id_CatergoryBlog;
    private Date create_Date;
    private boolean status;
   

    public Blog() {
    }

    public Blog(int id_Blog, String thumbnail_Blog, String title, String content_Blog, String brief_Infor_Blog, Date update_Date, int id_User, int view, int id_CatergoryBlog) {
        this.id_Blog = id_Blog;
        this.thumbnail_Blog = thumbnail_Blog;
        this.title = title;
        this.content_Blog = content_Blog;
        this.brief_Infor_Blog = brief_Infor_Blog;
        this.update_Date = update_Date;
        this.id_User = id_User;
        this.view = view;
        this.id_CatergoryBlog = id_CatergoryBlog;
    }
    public Blog(int id_Blog, String thumbnail_Blog, String title, String content_Blog, String brief_Infor_Blog, Date update_Date, int id_User, int view, int id_CatergoryBlog, Date create_Date) {
        this.id_Blog = id_Blog;
        this.thumbnail_Blog = thumbnail_Blog;
        this.title = title;
        this.content_Blog = content_Blog;
        this.brief_Infor_Blog = brief_Infor_Blog;
        this.update_Date = update_Date;
        this.id_User = id_User;
        this.view = view;
        this.id_CatergoryBlog = id_CatergoryBlog;
        this.create_Date = create_Date;
    }

    public Blog(int id_Blog, String thumbnail_Blog, String title, String content_Blog, String brief_Infor_Blog, Date update_Date, int id_User, int view, int id_CatergoryBlog, Date create_Date, boolean status) {
        this.id_Blog = id_Blog;
        this.thumbnail_Blog = thumbnail_Blog;
        this.title = title;
        this.content_Blog = content_Blog;
        this.brief_Infor_Blog = brief_Infor_Blog;
        this.update_Date = update_Date;
        this.id_User = id_User;
        this.view = view;
        this.id_CatergoryBlog = id_CatergoryBlog;
        this.create_Date = create_Date;
        this.status = status;
    }
    

    

   

    public int getId_Blog() {
        return id_Blog;
    }

    public void setId_Blog(int id_Blog) {
        this.id_Blog = id_Blog;
    }

    public String getThumbnail_Blog() {
        return thumbnail_Blog;
    }

    public void setThumbnail_Blog(String thumbnail_Blog) {
        this.thumbnail_Blog = thumbnail_Blog;
    }

    public String getContent_Blog() {
        return content_Blog;
    }

    public void setContent_Blog(String content_Blog) {
        this.content_Blog = content_Blog;
    }

    public String getBrief_Infor_Blog() {
        return brief_Infor_Blog;
    }

    public void setBrief_Infor_Blog(String brief_Infor_Blog) {
        this.brief_Infor_Blog = brief_Infor_Blog;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public int getId_CatergoryBlog() {
        return id_CatergoryBlog;
    }

    public void setId_CatergoryBlog(int id_CatergoryBlog) {
        this.id_CatergoryBlog = id_CatergoryBlog;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Blog{" + "id_Blog=" + id_Blog + ", thumbnail_Blog=" + thumbnail_Blog + ", title=" + title + ", content_Blog=" + content_Blog + ", brief_Infor_Blog=" + brief_Infor_Blog + ", update_Date=" + update_Date + ", id_User=" + id_User + ", view=" + view + ", id_CatergoryBlog=" + id_CatergoryBlog + '}';
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    
    
    public String formatDate() {
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        return f.format(create_Date);
    }
     public String formatUpdateDate() {
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        return f.format(update_Date);
    }

}
