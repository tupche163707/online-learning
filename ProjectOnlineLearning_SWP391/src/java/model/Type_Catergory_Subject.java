/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author PhanQuangHuy59
 */
public class Type_Catergory_Subject {
    private int id_Type_Catergory_Subjec;
    private String name_Catergory_Subject;
    private String description;

    public Type_Catergory_Subject() {
    }

    public Type_Catergory_Subject(int id_Type_Catergory_Subjec, String name_Catergory_Subject, String description) {
        this.id_Type_Catergory_Subjec = id_Type_Catergory_Subjec;
        this.name_Catergory_Subject = name_Catergory_Subject;
        this.description = description;
    }

    public int getId_Type_Catergory_Subjec() {
        return id_Type_Catergory_Subjec;
    }

    public void setId_Type_Catergory_Subjec(int id_Type_Catergory_Subjec) {
        this.id_Type_Catergory_Subjec = id_Type_Catergory_Subjec;
    }

    public String getName_Catergory_Subject() {
        return name_Catergory_Subject;
    }

    public void setName_Catergory_Subject(String name_Catergory_Subject) {
        this.name_Catergory_Subject = name_Catergory_Subject;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
