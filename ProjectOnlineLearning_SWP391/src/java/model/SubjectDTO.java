/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author PCT
 */
public class SubjectDTO {
    private int id_Subject;
    private String subject_Name;
    private String description;
    private int numberLesson;
    private Date create_Date;
    private boolean status;
    private String image_subject;
    private String courseName;
    private int id_User;
    private String name_Catergory_Subject;

    public SubjectDTO() {
    }

    public SubjectDTO(int id_Subject, String subject_Name, String description, int numberLesson, Date create_Date, boolean status, String image_subject, String courseName, int id_User, String name_Catergory_Subject) {
        this.id_Subject = id_Subject;
        this.subject_Name = subject_Name;
        this.description = description;
        this.numberLesson = numberLesson;
        this.create_Date = create_Date;
        this.status = status;
        this.image_subject = image_subject;
        this.courseName = courseName;
        this.id_User = id_User;
        this.name_Catergory_Subject = name_Catergory_Subject;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    
    public int getId_Subject() {
        return id_Subject;
    }

    public void setId_Subject(int id_Subject) {
        this.id_Subject = id_Subject;
    }

    public String getSubject_Name() {
        return subject_Name;
    }

    public void setSubject_Name(String subject_Name) {
        this.subject_Name = subject_Name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberLesson() {
        return numberLesson;
    }

    public void setNumberLesson(int numberLesson) {
        this.numberLesson = numberLesson;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getImage_subject() {
        return image_subject;
    }

    public void setImage_subject(String image_subject) {
        this.image_subject = image_subject;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public String getName_Catergory_Subject() {
        return name_Catergory_Subject;
    }

    public void setName_Catergory_Subject(String name_Catergory_Subject) {
        this.name_Catergory_Subject = name_Catergory_Subject;
    }
    
}

