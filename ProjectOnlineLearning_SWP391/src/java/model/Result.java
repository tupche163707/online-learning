/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author PhanQuangHuy59
 */
public class Result {

    private int id_Result;
    private Timestamp start_Time;
    private int duration;
    private String doResult;
    private String correctAnwser;
    private int id_User;
    private int id_Quiz;

    public Result() {
    }

    public Result(Timestamp start_Time, int duration, String doResult, String correctAnwser, int id_User, int id_Quiz) {
        this.start_Time = start_Time;
        this.duration = duration;
        this.doResult = doResult;
        this.correctAnwser = correctAnwser;
        this.id_User = id_User;
        this.id_Quiz = id_Quiz;
    }

    public Result(int id_Result, Timestamp start_Time, int duration, String doResult, String correctAnwser, int id_User, int id_Quiz) {
        this.id_Result = id_Result;
        this.start_Time = start_Time;
        this.duration = duration;
        this.doResult = doResult;
        this.correctAnwser = correctAnwser;
        this.id_User = id_User;
        this.id_Quiz = id_Quiz;
    }

    public int getId_Result() {
        return id_Result;
    }

    public void setId_Result(int id_Result) {
        this.id_Result = id_Result;
    }

    public Timestamp getStart_Time() {
        return start_Time;
    }

    public void setStart_Time(Timestamp start_Time) {
        this.start_Time = start_Time;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDoResult() {
        return doResult;
    }

    public void setDoResult(String doResult) {
        this.doResult = doResult;
    }

    public String getCorrectAnwser() {
        return correctAnwser;
    }

    public void setCorrectAnwser(String correctAnwser) {
        this.correctAnwser = correctAnwser;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public int getId_Quiz() {
        return id_Quiz;
    }

    public void setId_Quiz(int id_Quiz) {
        this.id_Quiz = id_Quiz;
    }

}
