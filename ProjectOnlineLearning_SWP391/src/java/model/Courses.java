/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author PCT
 */
public class Courses {

    private int id_Course;
    private String course_Name;
    private String description_Course;
    private Date create_Date;
    private Date update_date;
    private boolean status;
    private String image;
    private int id_Category;
    private int id_User;
    private int list_Price;
    private int sale_Price;
    private int id_PackagePrice;

    public Courses() {
    }

    public Courses(int id_Course, String course_Name, String description_Course, Date create_Date, Date update_date, boolean status, String image, int id_Category, int id_User, int list_Price, int sale_Price, int id_PackagePrice) {
        this.id_Course = id_Course;
        this.course_Name = course_Name;
        this.description_Course = description_Course;
        this.create_Date = create_Date;
        this.update_date = update_date;
        this.status = status;
        this.image = image;
        this.id_Category = id_Category;
        this.id_User = id_User;
        this.list_Price = list_Price;
        this.sale_Price = sale_Price;
        this.id_PackagePrice = id_PackagePrice;
    }

    public Date getUpdate_date() {
        return update_date;
    }

    public void setUpdate_date(Date update_date) {
        this.update_date = update_date;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

   

    public int getId_PackagePrice() {
        return id_PackagePrice;
    }

    public void setId_PackagePrice(int id_PackagePrice) {
        this.id_PackagePrice = id_PackagePrice;
    }

    public void setList_Price(int list_Price) {
        this.list_Price = list_Price;
    }

    public int getSale_Price() {
        return sale_Price;
    }

    public void setSale_Price(int sale_Price) {
        this.sale_Price = sale_Price;
    }

    public int getList_Price() {
        return list_Price;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
    }

    public String getCourse_Name() {
        return course_Name;
    }

    public void setCourse_Name(String course_Name) {
        this.course_Name = course_Name;
    }

    public String getDescription_Course() {
        return description_Course;
    }

    public void setDescription_Course(String description_Course) {
        this.description_Course = description_Course;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getId_Category() {
        return id_Category;
    }

    public void setId_Category(int id_Category) {
        this.id_Category = id_Category;
    }
    
}
