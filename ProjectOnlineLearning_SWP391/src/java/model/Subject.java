/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;
import java.text.SimpleDateFormat;

/**
 *
 * @author PhanQuangHuy59
 */
public class Subject {
    private int id_Subject;
    private String subject_Name;
    private String description;
    private int numberLesson;
    private Date create_Date;
    private Date update_Date;
    private boolean status;
    private String image_subject;
    private int id_Course;
    private int id_Catergory_Subject;
    private int id_User;

    public Subject() {
    }

    public Subject(int id_Subject, String subject_Name, String description, int numberLesson, Date create_Date, Date update_Date, boolean status, String image_subject, int id_Course, int id_Catergory_Subject, int id_User) {
        this.id_Subject = id_Subject;
        this.subject_Name = subject_Name;
        this.description = description;
        this.numberLesson = numberLesson;
        this.create_Date = create_Date;
        this.update_Date = update_Date;
        this.status = status;
        this.image_subject = image_subject;
        this.id_Course = id_Course;
        this.id_Catergory_Subject = id_Catergory_Subject;
        this.id_User = id_User;
    }

   
    

    public int getId_Subject() {
        return id_Subject;
    }

    public void setId_Subject(int id_Subject) {
        this.id_Subject = id_Subject;
    }

    public String getSubject_Name() {
        return subject_Name;
    }

    public void setSubject_Name(String subject_Name) {
        this.subject_Name = subject_Name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getNumberLesson() {
        return numberLesson;
    }

    public void setNumberLesson(int numberLesson) {
        this.numberLesson = numberLesson;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
        
    }

    public String getImage_subject() {
        return image_subject;
    }

    public void setImage_subject(String image_subject) {
        this.image_subject = image_subject;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
    }

   

    public int getId_Catergory_Subject() {
        return id_Catergory_Subject;
    }

    public void setId_Catergory_Subject(int id_Catergory_Subject) {
        this.id_Catergory_Subject = id_Catergory_Subject;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public Date getUpdate_Date() {
        return update_Date;
    }

    public void setUpdate_Date(Date update_Date) {
        this.update_Date = update_Date;
    }
    public String formatCreateDate(){
        SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");
        return f.format(create_Date);
    }
    
    
}
