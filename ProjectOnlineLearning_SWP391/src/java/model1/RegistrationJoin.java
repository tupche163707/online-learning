/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model1;

import java.sql.Date;

/**
 *
 * @author PCT
 */
public class RegistrationJoin {

    private int id_Register;
    private int id_User;
    private String email;
    private int id_Course;
    private String course_Name;
    private Date registration_Date;
    private int id_packagePrice;
    private int list_Price;
    private int sale_Price;
    private Boolean status;
    private Date valid_From;
    private Date valid_To;

    public RegistrationJoin() {
    }

    public RegistrationJoin(int id_Register, int id_User, String email, int id_Course, String course_Name, Date registration_Date, int id_packagePrice, int list_Price, int sale_Price, Boolean status, Date valid_From, Date valid_To) {
        this.id_Register = id_Register;
        this.id_User = id_User;
        this.email = email;
        this.id_Course = id_Course;
        this.course_Name = course_Name;
        this.registration_Date = registration_Date;
        this.id_packagePrice = id_packagePrice;
        this.list_Price = list_Price;
        this.sale_Price = sale_Price;
        this.status = status;
        this.valid_From = valid_From;
        this.valid_To = valid_To;
    }

    public int getId_Register() {
        return id_Register;
    }

    public void setId_Register(int id_Register) {
        this.id_Register = id_Register;
    }

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getId_Course() {
        return id_Course;
    }

    public void setId_Course(int id_Course) {
        this.id_Course = id_Course;
    }

    public String getCourse_Name() {
        return course_Name;
    }

    public void setCourse_Name(String course_Name) {
        this.course_Name = course_Name;
    }

    public Date getRegistration_Date() {
        return registration_Date;
    }

    public void setRegistration_Date(Date registration_Date) {
        this.registration_Date = registration_Date;
    }

    public int getId_packagePrice() {
        return id_packagePrice;
    }

    public void setId_packagePrice(int id_packagePrice) {
        this.id_packagePrice = id_packagePrice;
    }

    public int getList_Price() {
        return list_Price;
    }

    public void setList_Price(int list_Price) {
        this.list_Price = list_Price;
    }

    public int getSale_Price() {
        return sale_Price;
    }

    public void setSale_Price(int sale_Price) {
        this.sale_Price = sale_Price;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Date getValid_From() {
        return valid_From;
    }

    public void setValid_From(Date valid_From) {
        this.valid_From = valid_From;
    }

    public Date getValid_To() {
        return valid_To;
    }

    public void setValid_To(Date valid_To) {
        this.valid_To = valid_To;
    }
    
}
