/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model1;

import java.sql.Date;

/**
 *
 * @author PCT
 */
public class UserJoin {
    private int id_User;
    private String email;
    private String password;
    private String full_Name;
    private String phone_Number;
    private boolean gender;
    private String avatar;
    private String address;
    private boolean status;
    private int id_role;
    private Date create_Date;
    private String nameRole;

    public UserJoin() {
    }

    public UserJoin(int id_User, String email, String password, String full_Name, String phone_Number, boolean gender, String avatar, String address, boolean status, int id_role, Date create_Date, String nameRole) {
        this.id_User = id_User;
        this.email = email;
        this.password = password;
        this.full_Name = full_Name;
        this.phone_Number = phone_Number;
        this.gender = gender;
        this.avatar = avatar;
        this.address = address;
        this.status = status;
        this.id_role = id_role;
        this.create_Date = create_Date;
        this.nameRole = nameRole;
    }

    public Date getCreate_Date() {
        return create_Date;
    }

    public void setCreate_Date(Date create_Date) {
        this.create_Date = create_Date;
    }

   

    public int getId_User() {
        return id_User;
    }

    public void setId_User(int id_User) {
        this.id_User = id_User;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFull_Name() {
        return full_Name;
    }

    public void setFull_Name(String full_Name) {
        this.full_Name = full_Name;
    }

    public String getPhone_Number() {
        return phone_Number;
    }

    public void setPhone_Number(String phone_Number) {
        this.phone_Number = phone_Number;
    }

    public boolean isGender() {
        return gender;
    }

    public void setGender(boolean gender) {
        this.gender = gender;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getId_role() {
        return id_role;
    }

    public void setId_role(int id_role) {
        this.id_role = id_role;
    }

    public String getNameRole() {
        return nameRole;
    }

    public void setNameRole(String nameRole) {
        this.nameRole = nameRole;
    }
    
}

