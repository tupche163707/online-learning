/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Answer;
import model.Question;

/**
 *
 * @author PhanQuangHuy59
 */
public class AnswerDAO extends DBContext {

    /**
     * @author Naviank
     * @param id_Question
     * @return the number of correct answers of a question
     */
    public int getNumOfCorrectAnswerOfQuestion(int id_Question) {
        String sql = "select * from Answer \n"
                + "where is_Correct like '%t%' and id_Question = ?";
        int count = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if ("t".equals(rs.getString("is_Correct"))) {
                    count++;
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    /**
     * @author Naviank
     * @param cur_number
     * @param cur_answer_string
     * @return
     */
    public int getPositionInAnswerString(int cur_number, String cur_answer_string) {
        int res = 0;
        if (cur_number == 1) {
            return 0;
        }
        String[] arr = cur_answer_string.split(",");
        for (int i = 0; i < cur_number - 1; i++) {
            res += (arr[i].length() + 1);
        }
        return res;
    }

    /**
     * @author Naviank
     * @param arr
     * @return
     */
    public String combineStringArray(String[] arr) {
        String res = "";
        int i = 0;
        for (String s : arr) {
            res += s;
            i++;
            if (i < arr.length) {
                res += ",";
            }
        }
        return res;
    }

    /**
     * @author Naviank
     * @param cur_answer_string
     * @param cur_number
     * @param value
     * @return
     */
    public String modifyAnswerString(String cur_answer_string, int cur_number, String value) {
        String res;
        String arr[] = cur_answer_string.split(",");
        arr[cur_number - 1] = value;
        res = combineStringArray(arr);
        return res;
    }

    /**
     * @author Naviank
     * @param cur_answer_string
     * @return
     */
    public boolean[] classifyAnsweredQuestion(String cur_answer_string) {
        if (cur_answer_string == null) {
            return new boolean[]{false};
        }
        String[] arr = cur_answer_string.split(",");
        boolean barr[] = new boolean[arr.length];
        for (int i = 0; i < arr.length; i++) {
            barr[i] = !Utils.isAllStars(arr[i]);
        }
        return barr;
    }

    /**
     * @author Naviank
     * @param id_Question - specify a question in database
     * @return the correct string answer of a question specify by its
     * id_Question
     */
    public String getCorrectStringAnswerOfQuestion(int id_Question) {
        String sql = "select * from Answer where id_Question = ?";
        String result = "";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if ("t".equals(rs.getString(3))) {
                    result += rs.getString("answer_Content").charAt(0);
                } else {
                    result += "*";
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return result;
    }

    /**
     * @author Naviank
     * @param id_Question - specify a question in database
     * @return list of all answers of a question specify by its id_Question
     */
    public List<Answer> getAllAnswerOfQuestion(int id_Question) {
        List<Answer> list = new ArrayList<>();
        String sql = "select * from Answer where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Answer(rs.getInt(1),
                        rs.getString(2),
                        "t",
                        id_Question));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Answer> getAllAnswerOfQuestion2(int id_Question) {
        List<Answer> list = new ArrayList<>();
        String sql = "select * from Answer where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Answer(rs.getInt(1),
                        rs.getString(2),
                        rs.getString("is_Correct"),
                        id_Question));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public ArrayList<Answer> getAllCorrectAnswerOfQuestion(int id_Question) {
        ArrayList<Answer> list = new ArrayList<>();
        String sql = "select * from Answer where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if ("t".equals(rs.getString(3))) {
                    list.add(new Answer(rs.getInt(1), rs.getString(2), "t", id_Question));
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Answer> getAllIncorrectAnswerOfQuestion(int id_Question) {
        ArrayList<Answer> list = new ArrayList<>();
        String sql = "select * from Answer where id_Question = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Question);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if ("f".equals(rs.getString(3))) {
                    list.add(new Answer(rs.getInt(1), rs.getString(2), "f", id_Question));
                }
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void addAnswerByIdquest(int id, String content, String correct) {

        String sql = "INSERT INTO [dbo].[Answer]\n"
                + "           ([answer_Content]\n"
                + "           ,[is_Correct]\n"
                + "           ,[id_Question])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setString(2, correct);
            st.setInt(3, id);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(AnswerDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    public void deleteAnswerById(int id) {
        String sql = "DELETE FROM [dbo].[Answer]\n"
                + "      WHERE id_answer=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        } 
    }

    public void editAnswerById(String content, String correct, int id) {
        String sql = "UPDATE [dbo].[Answer]\n"
                + "   SET [answer_Content] = ?\n"
                + "      ,[is_Correct] = ?\n"
                + " WHERE id_answer=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1,content);
            st.setString(2,correct);
            st.setInt(3, id);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(AnswerDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * @author Naviank
     * @param id_Question
     * @return the number of records deleted from table Answer in database
     * 
     */
    public int deleteAnswerOfQuestion(int id_Question) {
        int count = 0;
        List<Answer> list = getAllAnswerOfQuestion(id_Question);
        for (Answer a : list) {
            deleteAnswerById(a.getId_Answer());
            count++;
        }
        return count;
    }
    
    public ArrayList<Answer> getAnswerByRangeIdQuesstion(ArrayList<Question> list) {
        String id_Subject = "";
        ArrayList<Answer> listAnswer = new ArrayList<>();
        String sql = "SELECT  [id_answer],[answer_Content],[is_Correct],[id_Question]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Answer] where id_Question in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Question()+ ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Question() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Question() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setId_Answer(rs.getInt("id_answer"));
                a.setAnswer_Content(rs.getString("answer_Content"));
                a.setIs_Corret(rs.getString("is_Correct"));
                a.setId_Question(rs.getInt("id_Question"));
                listAnswer.add(a);   
            }
            return listAnswer;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

    public void DeleteAnswerByRangeidQuestion(ArrayList<Question> list) {
        String id_Subject = "";
        String sql = "DELETE FROM [dbo].[Answer]\n"
                + "      WHERE id_Question in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Question() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Question() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Question() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);

        }
    }
}
