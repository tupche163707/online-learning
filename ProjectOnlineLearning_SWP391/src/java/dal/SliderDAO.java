/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Slider;

/**
 *
 * @author PhanQuangHuy59
 */
public class SliderDAO extends DBContext {

    /**
     * @author Naviank
     * @return List of all sliders
     */
    public List<Slider> getAllSliders() {
        List<Slider> list = new ArrayList<>();
        String sql = "SELECT [id_Slider]\n"
                + "      ,[title_Slider]\n"
                + "      ,[thumbnail_Image]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[notes]\n"
                + "      ,[backlink]\n"
                + "      ,[content_Slider]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Slider]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Slider b = new Slider(rs.getInt("id_Slider"),
                        rs.getString("title_Slider"),
                        rs.getString("thumbnail_Image"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getBoolean("status"),
                        rs.getString("notes"),
                        rs.getString("backlink"),
                        rs.getString("content_Slider"),
                        rs.getInt("id_User"));

                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @return List of all public sliders (status = 1)
     */
    public List<Slider> getDisplayedSliders() {
        List<Slider> list = new ArrayList<>();
        String sql = "SELECT [id_Slider]\n"
                + "      ,[title_Slider]\n"
                + "      ,[thumbnail_Image]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[notes]\n"
                + "      ,[backlink]\n"
                + "      ,[content_Slider]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Slider]"
                + "  WHERE [status] = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Slider b = new Slider(rs.getInt("id_Slider"),
                        rs.getString("title_Slider"),
                        rs.getString("thumbnail_Image"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getBoolean("status"),
                        rs.getString("notes"),
                        rs.getString("backlink"),
                        rs.getString("content_Slider"),
                        rs.getInt("id_User"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param status
     * @return List of all sliders which are published (status = 1) or
     * unpublished (status = 0)
     */
    public List<Slider> getSlidersOfStatus(int status) {
        List<Slider> list = new ArrayList<>();
        String sql = "SELECT [id_Slider]\n"
                + "      ,[title_Slider]\n"
                + "      ,[thumbnail_Image]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[notes]\n"
                + "      ,[backlink]\n"
                + "      ,[content_Slider]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Slider]"
                + "  WHERE [status] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (status > 1) {
                return getAllSliders();
            }
            st.setInt(1, status);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {

                Slider b = new Slider(rs.getInt("id_Slider"),
                        rs.getString("title_Slider"),
                        rs.getString("thumbnail_Image"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getBoolean("status"),
                        rs.getString("notes"),
                        rs.getString("backlink"),
                        rs.getString("content_Slider"),
                        rs.getInt("id_User"));
                list.add(b);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Slider getSliderByID(int id) {

        String sql = "SELECT [id_Slider]\n"
                + "      ,[title_Slider]\n"
                + "      ,[thumbnail_Image]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[notes]\n"
                + "      ,[backlink]\n"
                + "      ,[content_Slider]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Slider]"
                + "  WHERE id_Slider = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {

                Slider b = new Slider(rs.getInt("id_Slider"),
                        rs.getString("title_Slider"),
                        rs.getString("thumbnail_Image"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getBoolean("status"),
                        rs.getString("notes"),
                        rs.getString("backlink"),
                        rs.getString("content_Slider"),
                        rs.getInt("id_User"));
                return b;
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public void updateSliderById(String id_Slider, String title_Slider, String thumbnail_Image, String create_Date, String update_Date, String status, String notes, String blacklink, String content_Slider, String id_User) {

        String sql = "UPDATE [dbo].[Slider]\n"
                + "   SET [title_Slider] = ?\n"
                + "      ,[thumbnail_Image] = ?\n"
                + "      ,[create_Date] = ?\n"
                + "      ,[update_Date] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[notes] = ?\n"
                + "      ,[backlink] = ?\n"
                + "      ,[content_Slider] = ?\n"
                + "      ,[id_User] = ?\n"
                + " WHERE id_Slider = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title_Slider);
            st.setString(2, thumbnail_Image);
            st.setString(3, create_Date);
            st.setString(4, update_Date);
            st.setString(5, status);
            st.setString(6, notes);
            st.setString(7, blacklink);
            st.setString(8, content_Slider);
            st.setString(9, id_User);
            st.setString(10, id_Slider);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Slider> SearchTitleSlider(String name) {
        List<Slider> list = new ArrayList<>();
        String sql = "select * from Slider where title_Silder like ? ";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + name + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Slider b = new Slider(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getString(8),
                        rs.getString(9),
                        rs.getInt(10));
                list.add(b);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    /**
     * @author Naviank
     * @param listS
     * @return
     */
    public List<String> getAllSliderAuthorName(List<Slider> listS) {
        List<String> list = new ArrayList<>();
        UserDAO us = new UserDAO();
        for (Slider q : listS) {
            String tmp = us.getUserByIdUser(q.getId_User()).getFull_Name();
            list.add(tmp);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param searchvalue
     * @param list
     * @return
     */
    public List<Slider> searchSliderByNameInAList(String searchvalue, List<Slider> list) {
        List<Slider> qlist = new ArrayList<>();
        for (Slider q : list) {
            if (q.getBacklink().contains(searchvalue) || q.getTitle_Slider().contains(searchvalue)) {
                qlist.add(q);
            }
        }
        return qlist;
    }

    /**
     * @author Naviank
     * @param list
     * @param start
     * @param end
     * @return the sub-list of the received list from position start (inclusive)
     * to position end (exclusive)
     */
    public List<Slider> getListByPage(List<Slider> list, int start, int end) {
        List<Slider> lit = new ArrayList<>();
        for (int i = start; i < end; i++) {
            lit.add(list.get(i));
        }
        return lit;
    }

    public void changeSliderStatus(Slider s) {

        String sql = "UPDATE [dbo].[Slider]\n"
                + "   SET [title_Slider] = ?\n"
                + "      ,[thumbnail_Image] = ?\n"
                + "      ,[create_Date] = ?\n"
                + "      ,[update_Date] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[notes] = ?\n"
                + "      ,[backlink] = ?\n"
                + "      ,[content_Slider] = ?\n"
                + "      ,[id_User] = ?\n"
                + " WHERE id_Slider = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getTitle_Slider());
            st.setString(2, s.getThumbnail_Image());
            st.setDate(3, s.getCreate_Date());
            st.setDate(4, s.getUpdate_Date());
            st.setBoolean(5, s.isStatus());
            st.setString(6, s.getNotes());
            st.setString(7, s.getBacklink());
            st.setString(8, s.getContent_Slider());
            st.setInt(9, s.getId_User());
            st.setInt(10, s.getId_Slider());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    /**
     * @author Naviank
     * @param s
     * @return
     */
    public boolean checkDuplicateSlider(Slider s) {
        String sql = "SELECT [id_Slider]\n"
                + "      ,[title_Slider]\n"
                + "      ,[thumbnail_Image]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[notes]\n"
                + "      ,[backlink]\n"
                + "      ,[content_Slider]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Slider]"
                + "  WHERE title_Slider like ?\n"
                + "  AND backlink like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getTitle_Slider());
            st.setString(2, s.getBacklink());
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return false;
    }

    /**
     * @author Naviank
     * @param s
     * @return
     */
    public int addNewSlider(Slider s) {
        if (checkDuplicateSlider(s)) {
            return 0;
        }
        String sql = "INSERT INTO [dbo].[Slider]\n"
                + "           ([title_Slider]\n"
                + "           ,[thumbnail_Image]\n"
                + "           ,[create_Date]\n"
                + "           ,[update_Date]\n"
                + "           ,[status]\n"
                + "           ,[notes]\n"
                + "           ,[backlink]\n"
                + "           ,[content_Slider]\n"
                + "           ,[id_User])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, s.getTitle_Slider());
            st.setString(2, s.getThumbnail_Image());
            st.setDate(3, s.getCreate_Date());
            st.setDate(4, s.getUpdate_Date());
            st.setBoolean(5, s.isStatus());
            st.setString(6, s.getNotes());
            st.setString(7, s.getBacklink());
            st.setString(8, s.getContent_Slider());
            st.setInt(9, s.getId_User());
            return st.executeUpdate();
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return 0;
    }
}
