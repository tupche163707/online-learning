/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.YearMonth;
import model.Course;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Course;
import model.CatergoryCourse;
import model.Courses;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
public class CourseDAO extends DBContext {

    public Course getCourseByID(int id) {

        String sql = "SELECT [id_Course],[course_Name],[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status],[image],[id_Catergory]\n"
                + "FROM [dbo].[Course] where id_Course = ? and status = 1 ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Course cour = new Course();
            if (rs.next()) {
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
            }
            return cour;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public static void main(String[] args) {
        CourseDAO s = new CourseDAO();
        //Course c = s.getCourseByIDToUpdate(1);
        List<Course> ss = s.searchCourse("H");
        for (Course s1 : ss) {
            System.out.println(s1.getCourse_Name() + s1.getId_Course());
        }
        //System.out.println(c.getCourse_Name());
    }

    /**
     * @author Naviank
     * @return list of all courses
     */
    public List<Course> getAllCourses() {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @return list of public courses(status = 1)
     */
    public List<Course> getPublicCourses() {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course where [status] = 1";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param desc
     * @return list of public courses(status = 1) order by the created date in
     * descending or ascending order
     */
    public List<Course> getCoursesByCreatedDate(boolean desc) {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course \n"
                + "where Course.status = 1\n"
                + "order by Course.create_Date " + (desc ? "desc" : "asc");
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            int count = 0;
            while (rs.next() && count < 8) {
                Course c = new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9));
                list.add(c);
                count++;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param
     * @return Category of a Course(specific by its id) if exist. If not, return
     * null
     */
    public CatergoryCourse getCourseCateByID(int id) {
        String sql = "select * from Course_Catergory where id_Catergory = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new CatergoryCourse(id, rs.getString(2));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }

        return null;
    }

    /**
     * @author Naviank
     * @param num
     * @return list of (num) courses order by
     */
    public List<Course> getPopularCourse(int num) {
        List<Course> list = new ArrayList<>();
        String sql = "select * from Course \n"
                + "where Course.status = 1\n"
                + "order by Course.create_Date desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            int count = 0;
            while (rs.next() && list.size() < num) {
                Course c = new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9));
                list.add(c);
                count++;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @return The number of courses existing in database
     */
    public int countCourse() {
        String sql = "select count(*) from [Course]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt(1);
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public Vector<Courses> getAllCourse() {
        Vector<Courses> vector = new Vector<Courses>();
        String sql = "select nana.id_Course, nana.course_Name, nana.description_Course, nana.create_Date, nana.update_Date, nana.status, nana.image, nana.id_Catergory, nana.id_User,\n"
                + "nana.min_list_Price, nana.min_sale_Price, pp.id_Package \n"
                + "from \n"
                + "(\n"
                + "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, \n"
                + "c.min_list_Price, c.min_sale_Price from Course u join\n"
                + "(	\n"
                + "	select in2.id_Course, in2.min_sale_Price, in1.list_Price as min_list_Price\n"
                + "	from PackagePrice in1\n"
                + "	join (		select Min(sale_Price) as min_sale_Price, min(list_Price ) as a, id_Course\n"
                + "		from PackagePrice\n"
                + "		group by id_Course) as in2\n"
                + "		on in1.id_Course = in2.id_Course and in2.min_sale_Price = in1.sale_Price  and in2.a = in1.list_Price\n"
                + ") as c\n"
                + "on u.id_Course = c.id_Course\n"
                + ") as nana join PackagePrice pp\n"
                + "on nana.id_Course = pp.id_Course and nana.min_sale_Price = pp.sale_Price\n"
                + "order by nana.create_Date desc";

        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }

        } catch (SQLException ex) {
            Logger.getLogger(CourseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vector;
    }

    public Vector<Courses> SearchNameASC() {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course order by u.course_Name asc";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Vector<Courses> SearchNameDESC() {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course order by u.course_Name desc";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Vector<Courses> SearchPriceASC() {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course order by c.sale_Price asc";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Vector<Courses> SearchPriceDESC() {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course order by c.sale_Price desc";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Vector<Courses> SearchTop3Create_DateASC() {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course order by u.create_Date asc";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);
        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Vector<Courses> SearchCourseName(String name) {
        String sql = "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, c.list_Price,c.sale_Price, c.id_Package\n"
                + "from Course u inner join PackagePrice c on c.id_Course  = u.id_Course and u.course_Name like '%" + name + "%'";

        Vector<Courses> vector = new Vector<>();
        ResultSet rs = this.getData(sql);

        try {

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                int list_price = rs.getInt(10);
                int sale_price = rs.getInt(11);
                int package_price = rs.getInt(12);
                // create object
                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User, list_price, sale_price, package_price);
                vector.add(cour);
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return vector;
    }

    public Course getCourseID(int id) {
        String sql = "select * from Course where id_Course =?";
//     Vector<Product> vector = new Vector<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString(2);
                String description_course = rs.getString(3);
                Date create_date = rs.getDate(4);
                Date update_date = rs.getDate(5);
                Boolean status = rs.getBoolean(6);
                String image = rs.getString(7);
                int id_category = rs.getInt(8);
                int id_User = rs.getInt(9);
                Course cour = new Course(idcourse, course_name, description_course, create_date, update_date, true, image, id_category, id_User);
                return null;

            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    public int getNumberSubejectByIdCourse(int id) {
        String sql = "SELECT count(id_Subject) as NumberSubject\n"
                + "FROM [OnlineLearningSystem].[dbo].[Subject] where id_Course = ? and status = 1\n"
                + "group by id_Course";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {

                return rs.getInt("NumberSubject");
            }
        } catch (SQLException e) {
        }
        return 0;
    }

    public Course getCourseNameandid(String name , int idc) {
        String sql = "SELECT [id_Course],[course_Name],[description_Course],[create_Date],[update_Date]\n"
                + ",[status],[image],[id_Catergory],[id_User]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Course] where [course_Name] = ? and id_Course != ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, idc);
            ResultSet rs = st.executeQuery();
            Course cour = new Course();
            if (rs.next()) {
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
            }
            return cour;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }
    public Course getCourseName(String name) {
        String sql = "SELECT [id_Course],[course_Name],[description_Course],[create_Date],[update_Date]\n"
                + ",[status],[image],[id_Catergory],[id_User]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Course] where [course_Name] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            
            ResultSet rs = st.executeQuery();
            Course cour = new Course();
            if (rs.next()) {
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
            }
            return cour;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Course> searchCourse(String txtSearch) {//update hungct11/7/2023
        List<Course> listcourse = new ArrayList<>();
        String sql = "SELECT [id_Course]\n"
                + "      ,[course_Name]\n"
                + "      ,[description_Course]\n"
                + "      ,[create_Date]\n"
                + "      ,update_Date\n"
                + "      ,[status]\n"
                + "      ,[image]\n"
                + "      ,[id_Catergory]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Course]\n"
                + "  where course_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + txtSearch + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course c = new Course(rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getDate(4),
                        rs.getDate(5),
                        rs.getBoolean(6),
                        rs.getString(7),
                        rs.getInt(8),
                        rs.getInt(9));
                listcourse.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return listcourse;
    }

    public void updateCourseWithIdSubject(Course c) {
        String sql = "UPDATE [dbo].[Course]\n"
                + "   SET [course_Name] = ?\n"
                + "      ,[description_Course] = ?\n"
                + "      ,[create_Date] = ?\n"
                + "      ,[update_Date] = ?\n"
                + "      ,[status] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[id_Catergory] = ?\n"
                + " WHERE [id_Course] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getCourse_Name());
            st.setString(2, c.getDescription_Course());
            st.setDate(3, c.getCreate_Date());
            st.setDate(4, c.getUpdate_Date());
            st.setBoolean(5, c.isStatus());
            st.setString(6, c.getImage());
            st.setInt(7, c.getId_Category());
            st.setInt(8, c.getId_Course());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Course> getCourseNameWithPP(int pagenumber) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT	c.course_Name,\n"
                + "		p.id_Package,\n"
                + "		p.[id_Course]\n"
                + "FROM [OnlineLearningSystem].[dbo].Course c\n"
                + "JOIN [OnlineLearningSystem].[dbo].[PackagePrice] p ON p.[id_Course] = c.[id_Course]\n"
                + "Order by p.id_Package\n"
                + "OFFSET ? ROWS FETCH NEXT 5 ROWS ONLY;";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, (pagenumber - 1) * 5);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course c = new Course();
                c.setCourse_Name(rs.getString("course_Name"));
                c.setId_Course(rs.getInt("id_Course"));
                c.setId_User(rs.getInt("id_Package"));
                list.add(c);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Course getCourseByIDToUpdate(int id) {

        String sql = "SELECT [id_Course],[course_Name],[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status],[image],[id_Catergory]\n"
                + "FROM [dbo].[Course] where id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Course cour = new Course();
            if (rs.next()) {
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
            }
            return cour;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public void InsertCourse(Course c, int check) {
        String sql = "";
        if (check == -1) {
            sql = " INSERT INTO [dbo].[Course]([course_Name],[description_Course],[create_Date]\n"
                    + ",[update_Date],[status],[image],[id_Catergory],[id_User])\n"
                    + "VALUES(?,?,GETDATE(),GETDATE(),false,?,?,?)";
        } else {
            sql = " INSERT INTO [dbo].[Course]([course_Name],[description_Course],[create_Date]\n"
                    + ",[update_Date],[status],[image],[id_Catergory],[id_User])\n"
                    + "VALUES(?,?,GETDATE(),GETDATE(),?,?,?,?)";
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql);

            if (check == -1) {

                st.setString(1, c.getCourse_Name());
                st.setString(2, c.getDescription_Course());
                st.setString(3, c.getImage());
                st.setInt(4, c.getId_Category());
                st.setInt(5, c.getId_User());

                st.executeUpdate();
            } else {
                st.setString(1, c.getCourse_Name());
                st.setString(2, c.getDescription_Course());
                st.setBoolean(3, c.isStatus());
                st.setString(4, c.getImage());
                st.setInt(5, c.getId_Category());
                st.setInt(6, c.getId_User());
                st.executeUpdate();
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public int countCountActiveandUnActive() {
        int count = 0;
        String sql = "select count(id_Course) as numberCourse from Course\n"
                + "where status in(1,0)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                count = rs.getInt("numberCourse");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return count;
    }

    public ArrayList<Course> getNewCourse(int check) {
        int count = 0;
        LocalDate now = LocalDate.now();
        System.out.println(now.toString());
        LocalDate monthBefore1 = now.plusMonths(-1);
        int month = monthBefore1.getMonthValue();
        int year = monthBefore1.getYear();
        YearMonth yearMonth = YearMonth.of(year, month);
        LocalDate monthBefore = yearMonth.atDay(1);
        System.out.println(monthBefore.toString());
        String sql = "";
        if (check == 1) {
            sql = "SELECT  [id_Course],[course_Name],[description_Course]\n"
                    + "      ,[create_Date],[update_Date],[status]\n"
                    + "      ,[image],[id_Catergory],[id_User]\n"
                    + "  FROM [OnlineLearningSystem].[dbo].[Course]\n"
                    + "  where status in (0,1) and create_Date BETWEEN ? AND ? order by create_Date desc";
        } else if (check == 2) {
            sql = "SELECT  [id_Course],[course_Name],[description_Course]\n"
                    + "      ,[create_Date],[update_Date],[status]\n"
                    + "      ,[image],[id_Catergory],[id_User]\n"
                    + "  FROM [OnlineLearningSystem].[dbo].[Course]\n"
                    + "  where status in (0,1) order by create_Date desc";
        }

        ArrayList<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (check == 1) {
                st.setDate(1, Date.valueOf(monthBefore));
                st.setDate(2, Date.valueOf(now));
            }

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Course> getCourseByName(String name) {
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course],[course_Name],[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status]\n"
                + "      ,[image],[id_Catergory],[id_User]\n"
                + "FROM [dbo].[Course]";
        sql = sql + " where course_Name like \'%" + name + "%\'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Course> getFullCourseHuy(User u) {
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course],[course_Name],[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status]\n"
                + "      ,[image],[id_Catergory],[id_User]\n"
                + "FROM [dbo].[Course] where status in (1,0)";
        if (u.getId_role() == 2) {
            sql = sql + " and [id_User] = " + u.getId_User();
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public List<Course> getAllNormalCOurse() {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course]\n"
                + "      ,[course_Name]\n"
                + "      ,[description_Course]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image]\n"
                + "      ,[id_Catergory]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Course]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Course> getFullCourseByCategoryHuy(User u, int idcate) {
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course],[course_Name] ,[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status]\n"
                + "      ,[image],[id_Catergory],[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Course]\n"
                + "  where status in (1,0) and id_Catergory = ?";
        if (u.getId_role() == 2) {
            sql = sql + " and [id_User] = " + u.getId_User();
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idcate);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Course> getFullCourseByStatusHuy(User u, int status) {
        ArrayList<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course],[course_Name] ,[description_Course]\n"
                + "      ,[create_Date],[update_Date],[status]\n"
                + "      ,[image],[id_Catergory],[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Course]\n"
                + "  where status = ?";
        if (u.getId_role() == 2) {
            sql = sql + " and [id_User] = " + u.getId_User();
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Course cour = new Course();
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
                list.add(cour);
            }
            return list;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public boolean deleteCourseAdmin(int id) {
        String sql = "DELETE FROM [dbo].[Course]\n"
                + "      WHERE id_Course =?";
        boolean check = false;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.executeUpdate();
            check = true;
        } catch (SQLException e) {
            check = false;
            System.out.println(e);
        }
        return check;
    }

    public List<Courses> getPublicCourseWithConditions(String searchName, String categoryID, String sortID) {
        List<Courses> list = new ArrayList<>();
        StringBuilder sql = new StringBuilder();
        sql.append("select nana.id_Course, nana.course_Name, nana.description_Course, nana.create_Date, nana.update_Date, nana.status, nana.image, nana.id_Catergory, nana.id_User,\n"
                + "nana.min_list_Price, nana.min_sale_Price, pp.id_Package \n"
                + "from \n"
                + "(\n"
                + "select u.id_Course, u.course_Name,u.description_Course, u.create_Date,u.update_Date,u.status, u.image, u.id_Catergory,u.id_User, \n"
                + "c.min_list_Price, c.min_sale_Price from Course u join\n"
                + "(	\n"
                + "	select in2.id_Course, in2.min_sale_Price, in1.list_Price as min_list_Price\n"
                + "	from PackagePrice in1\n"
                + "	join (		select Min(sale_Price) as min_sale_Price, min(list_Price ) as a, id_Course\n"
                + "		from PackagePrice\n"
                + "		group by id_Course) as in2\n"
                + "		on in1.id_Course = in2.id_Course and in2.min_sale_Price = in1.sale_Price  and in2.a = in1.list_Price\n"
                + ") as c\n"
                + "on u.id_Course = c.id_Course\n"
                + ") as nana join PackagePrice pp\n"
                + "on nana.id_Course = pp.id_Course and nana.min_sale_Price = pp.sale_Price");

        if (searchName != null && !searchName.trim().isEmpty()) {
            sql.append(" AND nana.course_Name LIKE ?");
        }

        if (categoryID != null && !categoryID.trim().isEmpty()) {
            sql.append(" AND nana.id_Catergory = ?");
        }

        if (sortID != null) {
            if (sortID.equals("1")) {
                sql.append(" ORDER BY nana.course_Name");
            } else if (sortID.equals("2")) {
                sql.append(" ORDER BY nana.course_Name DESC");
            } else if (sortID.equals("3")) {
                sql.append(" ORDER BY nana.create_Date");
            } else if (sortID.equals("4")) {
                sql.append(" ORDER BY nana.min_sale_Price");
            } else if (sortID.equals("5")) {
                sql.append(" ORDER BY nana.min_sale_Price DESC");
            }
        }

        try {
            PreparedStatement st = connection.prepareStatement(sql.toString());

            int parameterIndex = 1;

            if (searchName != null && !searchName.trim().isEmpty()) {
                st.setString(parameterIndex, "%" + searchName + "%");
                parameterIndex++;
            }

            if (categoryID != null && !categoryID.trim().isEmpty()) {
                st.setString(parameterIndex, categoryID);
                parameterIndex++;
            }

            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                int idcourse = rs.getInt("id_Course");
                String course_name = rs.getString("course_Name");
                String description_course = rs.getString("description_Course");
                Date create_date = rs.getDate("create_Date");
                Date update_date = rs.getDate("create_Date");
                Boolean status = rs.getBoolean("status");
                String image = rs.getString("image");
                int id_category = rs.getInt("id_Catergory");
                int id_User = rs.getInt("id_User");
                int list_price = rs.getInt("min_list_Price");
                int sale_price = rs.getInt("min_sale_Price");
                int package_price = rs.getInt("id_Package");

                Courses cour = new Courses(idcourse, course_name, description_course, create_date, update_date, status, image, id_category, id_User, list_price, sale_price, package_price);
                list.add(cour);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }

        return list;
    }

    //huy
    public Course getCourseByIDH(int id) {

        String sql = "SELECT [id_Course],[course_Name],[description_Course],[create_Date],[update_Date]\n"
                + ",[status],[image],[id_Catergory],[id_User]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Course] where id_Course =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            Course cour = new Course();
            if (rs.next()) {
                cour.setId_Course(rs.getInt("id_Course"));
                cour.setCourse_Name(rs.getString("course_Name"));
                cour.setDescription_Course(rs.getString("description_Course"));
                cour.setCreate_Date(rs.getDate("create_Date"));
                cour.setUpdate_Date(rs.getDate("update_Date"));
                cour.setStatus(rs.getBoolean("status"));
                cour.setImage(rs.getString("image"));
                cour.setId_Category(rs.getInt("id_Catergory"));
                cour.setId_User(rs.getInt("id_User"));
            }
            return cour;
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;

    }

    //huy
    public void UpdateCouseAdminH(Course c) {
        String sql = " UPDATE [dbo].[Course]\n"
                + "SET [course_Name] = ?\n"
                + ",[description_Course] = ?\n"
                + ",[update_Date] = GETDATE()\n"
                + ",[status] = ?\n"
                + ",[image] = ?\n"
                + ",[id_Catergory] = ?\n"
                + ",[id_User] = ?\n"
                + " WHERE id_Course =?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, c.getCourse_Name());
            st.setString(2, c.getDescription_Course());
            st.setBoolean(3, c.isStatus());
            st.setString(4, c.getImage());
            st.setInt(5, c.getId_Category());
            st.setInt(6, c.getId_User());
            st.setInt(7, c.getId_Course());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<Courses> getListByPage1(List<Courses> list,
            int start, int end) {
        List<Courses> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }
    //caohung

    public List<Course> searchCourseByCourseName(String text) {
        List<Course> list = new ArrayList<>();
        String sql = "SELECT [id_Course]\n"
                + "      ,[course_Name]\n"
                + "      ,[description_Course]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[status]\n"
                + "      ,[image]\n"
                + "      ,[id_Catergory]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Course]\n"
                + "  where course_Name like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + text + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course c = new Course();
                c.setId_Course(rs.getInt("id_Course"));
                c.setCourse_Name(rs.getString("course_Name"));
                c.setDescription_Course(rs.getString("description_Course"));
                c.setCreate_Date(rs.getDate("create_Date"));
                c.setUpdate_Date(rs.getDate("update_Date"));
                c.setStatus(rs.getBoolean("status"));
                c.setImage(rs.getString("image"));
                c.setId_Category(rs.getInt("id_Catergory"));
                c.setId_User(rs.getInt("id_User"));
                list.add(c);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public void updateCourseName(String name, int id) {
        String sql = "UPDATE [dbo].[Course]\n"
                + "   SET [course_Name] = ?\n"
                + " WHERE id_Course = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, id);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    public int getmaxidcourse() {
        String sql = "SELECT [id_Course]\n"
                + "FROM [OnlineLearningSystem].[dbo].[Course]\n"
                + "order by [id_Course] desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet r = st.executeQuery();
            if (r.next()) {
                return r.getInt("id_Course");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

//caohung

}
