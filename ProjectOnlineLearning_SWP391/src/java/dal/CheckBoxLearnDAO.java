/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import model.CheckBoxLearn;
import model.Lesson;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
public class CheckBoxLearnDAO extends DBContext {

    public int getNumberLearn(int idSubjct, int idUser) {
        String sql = "select count(id_Lesson) as numberLession from Checkbox_Learn \n"
                + "where id_User = ? and id_Lesson in (\n"
                + "select id_Lesson from Lesson \n"
                + "where id_Subject = ?)\n"
                + "group by id_User";
        int numberLes = 0;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, idUser);
            st.setInt(2, idSubjct);

            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                numberLes = rs.getInt("numberLession");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return numberLes;
    }

    public static void main(String[] args) {

    }

    public void insertCheckBoxOfUser(CheckBoxLearn cbl) {
        String sql = "INSERT INTO [dbo].[Checkbox_Learn]\n"
                + "           ([id_Lesson]\n"
                + "           ,[id_User]\n"
                + "           ,[learn_Date])\n"
                + "     VALUES\n"
                + "           (?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cbl.getId_Lesson());
            st.setInt(2, cbl.getId_User());
            st.setDate(3, cbl.getLearn_Date());
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }
    
    /**
     * @author Naviank
     * @param id_User
     * @param id_Lesson
     */
    public void insertIntoCheckBoxOfUser(int id_User, int id_Lesson) {
        String sql = "INSERT INTO [dbo].[Checkbox_Learn]\n"
                + "           ([id_Lesson]\n"
                + "           ,[id_User]\n"
                + "           ,[learn_Date])\n"
                + "     VALUES\n"
                + "           (?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Lesson);
            st.setInt(2, id_User);
            st.setDate(3, new java.sql.Date(new Date().getTime()));
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public List<CheckBoxLearn> getAll() {
        List<CheckBoxLearn> list = new ArrayList<>();
        String sql = "SELECT [id_Lesson]\n"
                + "      ,[id_User]\n"
                + "      ,[learn_Date]\n"
                + "  FROM [dbo].[Checkbox_Learn]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                CheckBoxLearn cbl = new CheckBoxLearn();
                cbl.setId_Lesson(rs.getInt("id_Lesson"));
                cbl.setId_User(rs.getInt("id_User"));
                cbl.setLearn_Date(rs.getDate("learn_Date"));
                list.add(cbl);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public CheckBoxLearn getIdUserAndIdLearn(int iduser, int idlesson) {
        CheckBoxLearn cb = new CheckBoxLearn();
        String sql = "SELECT *\n"
                + "  FROM [dbo].[Checkbox_Learn]\n"
                + "  where id_User = ? and id_Lesson = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, iduser);
            st.setInt(2, idlesson);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                cb.setId_Lesson(idlesson);
                cb.setId_User(iduser);
                cb.setLearn_Date(rs.getDate("learn_Date"));
                return cb;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public ArrayList<Boolean> test() {
        CheckBoxLearnDAO cbldao = new CheckBoxLearnDAO();
        List<CheckBoxLearn> list = cbldao.getAll();
        LessonDAO ldao = new LessonDAO();
        List<Lesson> listlesson = ldao.getLessonBySubjectId(1);
        UserDAO udao = new UserDAO();
        User u = udao.getUserByIdUser(21);
        ArrayList<Boolean> resultList = new ArrayList<>();
        for (Lesson item : listlesson) {
            boolean contains = false;
            for (CheckBoxLearn listItem : list) {
                // Kiểm tra xem giá trị id_lesson của hai phần tử có bằng nhau hay không
                if ((item.getId_Lesson() == (listItem.getId_Lesson())) && (listItem.getId_User() == u.getId_User())) {
                    contains = true;
                    break;
                }
            }

            // Thêm giá trị true hoặc false vào danh sách kết quả
            resultList.add(contains);
        }
        return resultList;
    }

    public void DeleteCheckBoxLearnRange(ArrayList<Lesson> list) {
        String id_Subject = "";

        String sql = "DELETE FROM [dbo].[Checkbox_Learn]\n"
                + "      WHERE id_Lesson in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Lesson() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }
}

