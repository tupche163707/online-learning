/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

/**
 *
 * @author Naviank
 */
public class Utils {

    /**
     * @author Naviank
     * @param str
     * @return
     */
    public static boolean isAllStars(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '*') {
                count++;
            }
        }
        return (count == str.length());
    }

    /**
     * @author Naviank
     * @param str
     * @return
     */
    public static int countStars(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == '*') {
                count++;
            }
        }
        return (count);
    }

    /**
     * @author Naviank
     * @param dstr
     * @param cstr
     * @return
     */
    public static float gradeAQuestion(String dstr, String cstr) {
        int count = 0;
        int ncorrect = cstr.length() - Utils.countStars(cstr);
        if (ncorrect == 1) {
            dstr = dstr.replace("*", "");
            cstr = cstr.replace("*", "");
            if (dstr.equals(cstr)) {
                count = 1;
            }
        } else {
            dstr = dstr.replace("*", "");
            cstr = cstr.replace("*", "");
            if (dstr.length() <= cstr.length()) {
                for (int i = 0; i < dstr.length(); i++) {
                    for (int j = 0; j < cstr.length(); j++) {
                        if (dstr.charAt(i) == cstr.charAt(j)) {
                            count++;
                        }
                    }
                }
            }
        }
        return (float) count / ncorrect;
    }

    public static void main(String[] args) {
//        String s1 = "****,****,****,****,****,****,****,****,****";
//        String s2 = "A***,AB**,*BC*,**C*,***D,A***,*B**,*B**,***D";
//        String [] a1 = s1.split(",");
//        String [] a2 = s2.split(",");
//        for (int i = 0; i < a1.length; i++) {
//        System.out.println(Utils.gradeAQuestion(a1[i], a2[i]));
//        }
        System.out.println(Utils.gradeAQuestion("****", "A***"));
    }
}
