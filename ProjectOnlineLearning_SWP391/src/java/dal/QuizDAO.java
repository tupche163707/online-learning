/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import model.Answer;
import model.Lesson;
import model.LevelQuiz;
import model.Question;
import model.Quiz;

/**
 *
 * @author PhanQuangHuy59
 */
public class QuizDAO extends DBContext {

    /**
     * @author Naviank
     * @param id_lesson
     * @return a list of quiz and their attribute, belong to a lesson which is
     * specified by its id_Lesson
     */
    public List<Quiz> selectQuizOfLesson(int id_lesson) {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT [id_Quiz]\n"
                + "      ,[name_Quiz]\n"
                + "      ,[number_Quesson]\n"
                + "      ,[time_Limit]\n"
                + "      ,[description_Quiz]\n"
                + "      ,[id_Lesson]\n"
                + "      ,[id_Level]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Quiz]"
                + "  WHERE id_Lesson = ?"
                + "  ORDER BY id_Level ASC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_lesson);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"));
                list.add(q);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param id_lesson
     * @return an array of quiz_id belong to a lesson which is specified by its
     * id_Lesson
     */
    public int[] selectIDQuizOfLesson(int id_lesson) {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT [id_Quiz]\n"
                + "      ,[name_Quiz]\n"
                + "      ,[number_Quesson]\n"
                + "      ,[time_Limit]\n"
                + "      ,[description_Quiz]\n"
                + "      ,[id_Lesson]\n"
                + "      ,[id_Level]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_User]\n"
                + "  FROM [dbo].[Quiz]"
                + "  WHERE id_Lesson = ?"
                + "  ORDER BY id_Level ASC";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_lesson);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"));
                list.add(q);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        int[] arr = new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            arr[i] = list.get(i).getId_Quiz();
        }
        return arr;
    }

    /**
     * @author Naviank
     * @param id_quiz
     * @return a quiz and its attribute, belong to a lesson which is specified
     * by its id_Question
     */
    public Quiz selectQuizByID(int id_quiz) {
        String sql = "SELECT [id_Quiz]\n"
                + "      ,[name_Quiz]\n"
                + "      ,[number_Quesson]\n"
                + "      ,[time_Limit]\n"
                + "      ,[description_Quiz]\n"
                + "      ,[id_Lesson]\n"
                + "      ,[id_Level]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_User]\n"
                + "      ,[passpercent]\n"
                + "  FROM [dbo].[Quiz]"
                + "  WHERE id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_quiz);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("passpercent")
                );
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return list of all question of a quiz specify by its id_Quiz
     */
    public List<Question> getAllQuestionsOfQuiz(int id_Quiz) {
        List<Question> list = new ArrayList<>();
        String sql = "SELECT [id_Question]\n"
                + "      ,[content_Question]\n"
                + "      ,[description]\n"
                + "      ,[id_Quiz]\n"
                + "      ,[createDate]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_Type]\n"
                + "  FROM [dbo].[Question]"
                + "  WHERE id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Quiz);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Question(rs.getInt("id_Question"),
                        rs.getString("content_Question"),
                        rs.getInt("id_Quiz"),
                        rs.getString("description"),
                        rs.getInt("id_Type"),
                        rs.getDate("createDate"),
                        rs.getDate("update_Date")));
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public String getQuizByIdquestion(int id) {
        String quiz = null;
        String sql = "select qi.name_Quiz \n"
                + "from Quiz qi, Question q\n"
                + "where qi.id_Quiz=q.id_Quiz\n"
                + "and q.id_Question=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                quiz = rs.getString("name_Quiz");
            }
            return quiz;
        } catch (Exception e) {
        } finally {
            try {
                connection.close();
            } catch (SQLException ex) {
                Logger.getLogger(SubjectDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return
     */
    public TreeMap<Question, List<Answer>> getMapQuestionAnswerOfQuiz(int id_Quiz) {
        TreeMap<Question, List<Answer>> map = new TreeMap<>();
        AnswerDAO ad = new AnswerDAO();
        List<Question> listQues = getAllQuestionsOfQuiz(id_Quiz);
        for (Question ques : listQues) {
            List<Answer> tmp = ad.getAllAnswerOfQuestion(ques.getId_Question());
            map.put(ques, tmp);
        }
        return map;
    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return the correct Answer string of a quiz specify by its id_Quiz
     */
    public String getCorrectStringAnswerOfQuiz(int id_Quiz) {
        List<Question> listQuestion = getAllQuestionsOfQuiz(id_Quiz);
        String result = "";
        int count = 1;
        for (Question q : listQuestion) {
            AnswerDAO ad = new AnswerDAO();
            int id = q.getId_Question();
            if (count < listQuestion.size()) {
                result += ad.getCorrectStringAnswerOfQuestion(id) + ",";
            } else {
                result += ad.getCorrectStringAnswerOfQuestion(id);
            }
            count++;
        }
        return result;
    }

    /**
     * @author Naviank
     * @return all quiz level in database
     */
    public List<LevelQuiz> getAllLevelOfQuiz() {
        List<LevelQuiz> list = new ArrayList<>();
        String sql = "select * from [Level]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new LevelQuiz(rs.getInt("id_Level"),
                        rs.getString("name_Level")));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return list;
    }

    /**
     * @author Naviank
     * @return all quiz-level-name in database
     */
    public List<String> getAllLevelName() {
        List<String> list = new ArrayList<>();
        String sql = "select * from [Level]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(rs.getString("name_Level"));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return list;
    }

    public Quiz getQuizByID(int qid) {
        Quiz q = new Quiz();
        String sql = "select id_Quiz,name_Quiz,number_Quesson,time_Limit,create_Date,update_Date,passpercent from quiz where id_Quiz=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, qid);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                q.setId_Quiz(qid);
                System.out.println(rs.getString("name_Quiz"));
                q.setName_Quiz(rs.getString("name_Quiz"));
                q.setNumber_Question(rs.getInt("number_Quesson"));
                q.setTime_Limit(rs.getInt("time_Limit"));
                q.setCreateDate(rs.getDate("create_Date"));
                q.setCreateDate(rs.getDate("update_Date"));
                q.setPasspercent(rs.getFloat("passpercent"));
                q.setDescription_Quiz("");
                q.setId_Lession(1);
                q.setId_User(3);
                q.setId_Level(1);
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return q;
    }

    public void updateQuiz(String name, int timelimit, float passrate,int qid, Date date) {
        String sql = "UPDATE [dbo].[Quiz]\n"
                + "   SET [name_Quiz] = ?\n"
                + "      ,[time_Limit] = ?\n"
                + "      ,[update_Date] = ?\n"
                + "      ,[passpercent] = ?\n"
                + " WHERE id_Quiz=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, timelimit);
            st.setDate(3, date);
            st.setFloat(4, passrate);
            st.setInt(5, qid);

            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public ArrayList<Quiz> GetListQuizByListLesson(ArrayList<Lesson> list) {
        String id_Subject = "";

        ArrayList<Quiz> listquiz = new ArrayList<>();
        String sql = "SELECT  [id_Quiz],[name_Quiz],[number_Quesson],[time_Limit]\n"
                + "      ,[description_Quiz],[id_Lesson],[id_Level],[create_Date],[update_Date],[id_User]\n"
                + "  FROM [OnlineLearningSystem].[dbo].[Quiz]\n"
                + "  where id_Lesson in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Lesson() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz q = new Quiz();
                q.setId_Quiz(rs.getInt("id_Quiz"));
                q.setName_Quiz(rs.getString("name_Quiz"));
                q.setNumber_Question(rs.getInt("number_Quesson"));
                q.setTime_Limit(rs.getInt("time_Limit"));
                q.setDescription_Quiz(rs.getString("description_Quiz"));
                q.setId_Lession(rs.getInt("id_Lesson"));
                q.setId_Level(rs.getInt("id_Level"));
                q.setCreateDate(rs.getDate("create_Date"));
                q.setUpdate_Date(rs.getDate("update_Date"));
                listquiz.add(q);
            }
            return listquiz;
        } catch (SQLException e) {
            System.out.println(e);

        }
        return null;
    }

    public void DeleteQuizRange(ArrayList<Lesson> list) {
        String id_Subject = "";

        String sql = "DELETE FROM Quiz\n"
                + "      WHERE id_Lesson in";
        for (int i = 0; i < list.size(); i++) {
            if (i == 0) {
                id_Subject = id_Subject + "(" + list.get(i).getId_Lesson() + ",";
            }
            if (i != 0 && i != list.size() - 1) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ",";
            }
            if (i == (list.size() - 1)) {
                id_Subject = id_Subject + list.get(i).getId_Lesson() + ")";
            }
        }
        sql += id_Subject;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }

    }

    /**
     * @author Naviank
     * @param id_Level
     * @return
     */
    public LevelQuiz getLevelOfQuiz(int id_Level) {
        String sql = "select * from [Level] "
                + " where id_Level = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Level);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return new LevelQuiz(rs.getInt("id_Level"),
                        rs.getString("name_Level"));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return null;
    }

    /**
     * @author Naviank
     * @return
     */
    public List<Quiz> getAllQuiz() {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT [id_Quiz]\n"
                + "      ,[name_Quiz]\n"
                + "      ,[number_Quesson]\n"
                + "      ,[time_Limit]\n"
                + "      ,[description_Quiz]\n"
                + "      ,[id_Lesson]\n"
                + "      ,[id_Level]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_User]\n"
                + "      ,[passpercent]\n"
                + "  FROM [dbo].[Quiz]";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("passpercent")));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return list;
    }

    /**
     * @author Naviank
     * @param id_Level
     * @param id_Subject
     * @return
     */
    public List<Quiz> filterQuizzesByLevelAndSubject(String id_Level, String id_Subject) {
        List<Quiz> list = new ArrayList<>();
        String sql = "  SELECT q.*\n"
                + "  FROM Quiz q, Lesson l, [Subject] s\n"
                + "  WHERE q.id_Lesson = l.id_Lesson\n"
                + "  AND l.id_Subject = s.id_Subject\n";
        try {
            if (!"".equals(id_Subject)) {
                sql += ("  AND s.id_Subject = " + id_Subject + "\n");
            }
            if (!"".equals(id_Level)) {
                sql += ("  AND id_Level = " + id_Level + "\n");
            }
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("passpercent")));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return list;
    }

    /**
     * @author Naviank
     * @param lquiz
     * @return
     */
    public List<String> getAllQuizLessonName(List<Quiz> lquiz) {
        List<String> list = new ArrayList<>();
        LessonDAO ld = new LessonDAO();
        for (Quiz q : lquiz) {
            String tmp = ld.getLessonByIdLesson(q.getId_Lession()).getName_Lesson();
            list.add(tmp);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param lquiz
     * @return
     */
    public List<String> getAllQuizSubjectName(List<Quiz> lquiz) {
        List<String> list = new ArrayList<>();
        SubjectDAO sd = new SubjectDAO();
        for (Quiz q : lquiz) {
            String tmp = sd.getSubjectByIdLesson(q.getId_Lession()).getSubject_Name();
            list.add(tmp);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param lquiz
     * @return
     */
    public List<String> getAllQuizLevelName(List<Quiz> lquiz) {
        List<String> list = new ArrayList<>();
        for (Quiz q : lquiz) {
            String tmp = getLevelOfQuiz(q.getId_Level()).getName_Level();
            list.add(tmp);
        }
        return list;
    }

    /**
     * @author Naviank
     * @param lquiz
     * @return
     */
    public List<Float> getAllQuizPassTakeRatio(List<Quiz> lquiz) {
        List<Float> list = new ArrayList<>();
        for (Quiz q : lquiz) {
            QuizResultDAO qrd = new QuizResultDAO();
            list.add(qrd.calcTakePassRatioOfQuiz(q.getId_Quiz()));
        }
        return list;
    }

    /**
     * @author Naviank
     * @param id_Quiz
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     */
    public int deleteQuiz(int id_Quiz) {
        AnswerDAO ad = new AnswerDAO();
        QuestionDAO qd = new QuestionDAO();
        QuizResultDAO qrd = new QuizResultDAO();
        List<Question> listques = getAllQuestionsOfQuiz(id_Quiz);
        for (Question q : listques) {
            ad.deleteAnswerOfQuestion(q.getId_Question());
        }
        qd.deleteQuestionOfQuiz(id_Quiz);
        qrd.deleteResultOfQuiz(id_Quiz);
        String sql = "DELETE FROM [dbo].[Quiz]\n"
                + "      WHERE id_Quiz = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id_Quiz);
            return st.executeUpdate();
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return 0;
    }

    /**
     * @author Naviank
     * @param list
     * @param start
     * @param end
     * @return the sub-list of the received list from position start (inclusive)
     * to position end (exclusive)
     */
    public List<Quiz> getListByPage(List<Quiz> list, int start, int end) {
        List<Quiz> lit = new ArrayList<>();
        for (int i = start; i < end; i++) {
            lit.add(list.get(i));
        }
        return lit;
    }

    /**
     * @author Naviank
     * @param searchvalue
     * @return
     */
    public List<Quiz> searchQuizByName(String searchvalue) {
        List<Quiz> list = new ArrayList<>();
        String sql = "SELECT [id_Quiz]\n"
                + "      ,[name_Quiz]\n"
                + "      ,[number_Quesson]\n"
                + "      ,[time_Limit]\n"
                + "      ,[description_Quiz]\n"
                + "      ,[id_Lesson]\n"
                + "      ,[id_Level]\n"
                + "      ,[create_Date]\n"
                + "      ,[update_Date]\n"
                + "      ,[id_User]\n"
                + "      ,[passpercent]\n"
                + "  FROM [dbo].[Quiz]\n"
                + "  WHERE name_Quiz like ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, "%" + searchvalue + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(new Quiz(rs.getInt("id_Quiz"),
                        rs.getString("name_Quiz"),
                        rs.getInt("number_Quesson"),
                        rs.getInt("time_Limit"),
                        rs.getInt("id_Level"),
                        rs.getString("description_Quiz"),
                        rs.getInt("id_Lesson"),
                        rs.getDate("create_Date"),
                        rs.getDate("update_Date"),
                        rs.getInt("id_User"),
                        rs.getInt("passpercent")));
            }
        } catch (SQLException e) {
            e.getErrorCode();
        }
        return list;
    }

    /**
     * @author Naviank
     * @param searchvalue
     * @param list
     * @return
     */
    public List<Quiz> searchQuizByNameInAList(String searchvalue, List<Quiz> list) {
        List<Quiz> qlist = new ArrayList<>();
        for (Quiz q : list) {
            if (q.getName_Quiz().contains(searchvalue)) {
                qlist.add(q);
            }
        }
        return qlist;
    }
    
    /**
     * @author Naviank
     * @param idUser
     * @param listQ
     * @return
     */
    public List<Quiz> filterListByIdUser(int idUser, List<Quiz> listQ) {
        UserDAO ud = new UserDAO();
        int role = ud.getUserByIdUser(idUser).getId_role();
        if (role == 1) {
            return listQ;
        }
        List<Quiz> list = new ArrayList<>();
        for (Quiz q : listQ) {
            if (q.getId_User() == idUser) {
                list.add(q);
            }
        }
        return list;
    }
}

