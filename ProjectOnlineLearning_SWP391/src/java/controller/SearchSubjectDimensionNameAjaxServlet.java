/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergorySubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.CatergorySubject;
import model.Type_Catergory_Subject;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "SearchSubjectDimensionNameAjaxServlet", urlPatterns = {"/searchsubjectdimensionnameajax"})
public class SearchSubjectDimensionNameAjaxServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String text = request.getParameter("txt");
        CatergorySubjectDAO csdao = new CatergorySubjectDAO();
        List<CatergorySubject> listsearch = csdao.searchSubjectCate(text);
        List<Type_Catergory_Subject> listType = csdao.getTypeCaterSubj();
        List<Type_Catergory_Subject> listTypeSearched = new ArrayList<>();
        for (int i = 0; i < listsearch.size(); i++) {
            for (int j = 0; j < listType.size(); j++) {
                if(listsearch.get(i).getId_Type_Catergory_Subjec() == listType.get(j).getId_Type_Catergory_Subjec()){
                    Type_Catergory_Subject tcs = csdao.getTypeCateById(listType.get(j).getId_Type_Catergory_Subjec());
                    listTypeSearched.add(tcs);
                }
            }
        }

        PrintWriter out = response.getWriter();
        for (int i = 0; i < listTypeSearched.size(); i++) {
            out.println("<tr>\n"
                    + "                                                                                <td scope=\"row\">" + (i+1) + "\n"
                    + "                                                                                </td>\n"
                    + "                                                                                <td>"+listTypeSearched.get(i).getName_Catergory_Subject()+"</td>\n"
                    + "                                                                                <td>"+listsearch.get(i).getName_Catergory_Subject()+"</td>\n"
                    + "                                                                                <td>"+listsearch.get(i).getDescription_Catergory_Subject()+"</td>\n"
                    + "                                                                                <td style=\"width: 20%\">\n"
                    + "                                                                                    <button type=\"button\" onclick=\"openEdit(" + (i+1) + ")\" class=\"btn\">Edit</button>    \n"
                    + "                                                                                    <button type=\"button\" class=\"btn gradient brown\" onclick=\"deleteEdit("+listsearch.get(i).getId_Catergory_Subject()+")\">Delete</button>\n"
                    + "                                                                                </td>\n"
                    + "                                                                            </tr>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
