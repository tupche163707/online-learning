/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
import dal.CatergoryBlogDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Blog;
import model.CatergoryBlog;
import model.User;

/**
 *
 * @author vuduy
 */
@WebServlet(name = "BlogDetailsServlet", urlPatterns = {"/blogdetail"})
public class DetailBlogServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LoginServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LoginServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("id_Blog");
        BlogDAO bl = new BlogDAO();
        UserDAO udao = new UserDAO();
        Blog b = bl.getBlogById(Integer.parseInt(id_raw));
        List<Blog> list = bl.getBlogWithAuthor();
        CatergoryBlogDAO c = new CatergoryBlogDAO();
        User u = udao.getUserByIdUser(b.getId_User());
        List<CatergoryBlog> list1 = c.getAllBlogCate();
        List<CatergoryBlog> list2 = c.getCatergoryName();
        int id = Integer.parseInt(id_raw);
        CatergoryBlog ctg = new CatergoryBlog();
        for (int i = 0; i < list2.size(); i++) {
            if(id == list2.get(i).getId_Catergory_Blog()){
                ctg.setId_Catergory_Blog(list2.get(i).getId_Catergory_Blog());
                ctg.setName_Catergory_Blog(list2.get(i).getName_Catergory_Blog());
            }
        }
        bl.updateView(b.getView() + 1, id);
        
        
        request.setAttribute("xxx", "bloglist");
        //lấy tham số id_Blog từ BlogList để hiển thị chi tiết
        request.setAttribute("listhavecatergoryname", ctg);
        request.setAttribute("dayOfUpdateDate", bl.getNameOfDayOfBlog(b.getUpdate_Date()));
        request.setAttribute("views", b.getView() + 1);
        request.setAttribute("blog", b);//blog co du thuoc tinh cua blog
        request.setAttribute("listBlog", list);// list blog co du tat ca cac object blog
        request.setAttribute("author", u);//lay thong tin tac gia dua theo id_Blog da co
        request.setAttribute("listBlogCategory", list1); //list phan loai blog (blogcategory) co du het cac thuoc tinh cua blog category
        request.getRequestDispatcher("blogdetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BlogDAO bldao = new BlogDAO();
        String searchrequest = request.getParameter("searchtitle");
        List<Blog> list = bldao.searchByBlogTitle(searchrequest);
        request.setAttribute("blogsearch", list);
        request.getRequestDispatcher("blogsearch.jsp").forward(request, response);
        }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
