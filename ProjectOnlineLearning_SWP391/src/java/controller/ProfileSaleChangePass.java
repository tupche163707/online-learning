/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.User;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name="ProfileSaleChangePass", urlPatterns={"/profilesalechangepass"})
public class ProfileSaleChangePass extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfileSaleChangePass</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfileSaleChangePass at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       request.getRequestDispatcher("profilesalechangepass.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        User update = (User) session.getAttribute("user");
        UserDAO userDao = new UserDAO();
        
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        String confirmpass = request.getParameter("confirmpass");
        if (update.getPassword().equals(oldpass)) {
            if (newpass.equals(confirmpass)) {
                update.setPassword(newpass);
                userDao.updateUserById(update);
                request.setAttribute("mesaddcousrse", "Successfully change password.");
                request.setAttribute("classmes", " messagesuccess");
                request.setAttribute("user", update);
                request.getRequestDispatcher("profilesalechangepass.jsp").forward(request, response);
            } else {
                request.setAttribute("classmes", " messagefail");
                request.setAttribute("user", update);
                request.setAttribute("mesaddcousrse", "Change Pass failed, Try again!");
                request.setAttribute("mespass1", "New password and confirm password not match.");
                request.setAttribute("oldpass", oldpass);
                request.setAttribute("newpass", newpass);
                request.setAttribute("confirmpass", confirmpass);
                request.getRequestDispatcher("profilesalechangepass.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("classmes", " messagefail");
            request.setAttribute("user", update);
            request.setAttribute("mesaddcousrse", "Change Pass failed, Try again!");
            request.setAttribute("mespass", "Old Password incorrect!");
            request.setAttribute("oldpass", oldpass);
            request.setAttribute("newpass", newpass);
            request.setAttribute("confirmpass", confirmpass);
            request.getRequestDispatcher("profilesalechangepass.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
