/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.RegistrationDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.CatergoryCourse;
import model.Course;
import model.Registration;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "MycourseServlet", urlPatterns = {"/mycourse"})
public class MycourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet MycourseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet MycourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        /////////
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        String check = request.getParameter("check");
        //////////khai bai dao
        RegistrationDAO reDao = new RegistrationDAO();
        CourseDAO coDao = new CourseDAO();
        CourseCategoryDAO cadao = new CourseCategoryDAO();

        /////////// khai bao danh sách
        ArrayList<Course> listcourse = new ArrayList<>();
        ArrayList<Registration> listregistration = new ArrayList<>();
        ArrayList<CatergoryCourse> listcategory = new ArrayList<>();
        ArrayList<Integer> month1 = new ArrayList<>();
        ArrayList<Integer> numberSubject = new ArrayList<>();
        ArrayList<Boolean> checkExpire = new ArrayList<>();
        month1.add(0);
        month1.add(1);
        month1.add(3);
        month1.add(6);
        month1.add(9);
        month1.add(12);
        String tpage = request.getParameter("page");

        //// lay du lieu
        listcategory = cadao.getAllCatergory();
        //dung cho datalist

//        ArrayList<Registration> datalist = reDao.getAllRegistrationOfUser(u.getId_User());
//        ArrayList<Course> datalist1 = new ArrayList();
//        for (Registration registration : datalist) {
//            datalist1.add(coDao.getCourseByID(registration.getId_Course()));
//        }
        if (check == null) {
            check = "0";
        }

        if (check.equals("1")) {
            listregistration = reDao.getAllRegistrationOfUser(u.getId_User());

        }
        if (check.equals("2")) {

            LocalDate localdate = LocalDate.now();
            listregistration = reDao.getAllRegistrationOfUserUnfinish(u.getId_User(), Date.valueOf(localdate));
        }

        if (check.equals("3")) {
            LocalDate localdate = LocalDate.now();
            listregistration = reDao.getAllRegistrationOfUserFinish(u.getId_User(), Date.valueOf(localdate));
        }

        if (check.equals("4")) {
            ArrayList<Registration> listReNew = new ArrayList();
            ArrayList<Course> listCourseNew = new ArrayList();

            String nameSearch = request.getParameter("namecourse");
            listregistration = reDao.getAllRegistrationOfUser(u.getId_User());
            for (Registration registration : listregistration) {

                listcourse.add(coDao.getCourseByID(registration.getId_Course()));
            }

            for (int i = 0; i < listcourse.size(); i++) {
                if (listcourse.get(i).getCourse_Name().contains(nameSearch)) {
                    listReNew.add(listregistration.get(i));
                    listCourseNew.add(listcourse.get(i));
                }
            }
            request.setAttribute("listregistration", listReNew);
            request.setAttribute("listcourse", listCourseNew);
            request.setAttribute("key", nameSearch);

        }

        if (check.equals("5")) {
            String datecheck = request.getParameter("month");

            Date dateCheck = Date.valueOf(datecheck);
            
            LocalDate localdate = LocalDate.now();
            Date now = Date.valueOf(localdate);
            if(dateCheck.before(now)){
                listregistration = reDao.checkExpire(u.getId_User(), dateCheck, now);
            }else{
                listregistration = reDao.checkExpire(u.getId_User(), now, dateCheck);
            }
                
            
            request.setAttribute("month", datecheck);

        }

        if (check.equals("6")) {
            String category = request.getParameter("category");
            int categoryInt = Integer.parseInt(category);
            listregistration = reDao.getAllRegistrationFollowCatergoryCourse(u.getId_User(), categoryInt);
            request.setAttribute("category", categoryInt);
        }

        if (check.equals("0")) {
            listregistration = reDao.getAllRegistrationOfUser(u.getId_User());
        }
        // phan trang
        int numPs = listregistration.size();
        int numpage = numPs / 6 + (numPs % 6 == 0 ? 0 : 1);
        listregistration = phantrang(listregistration, tpage);
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        LocalDate now = LocalDate.now();
        for (Registration re : listregistration) {
            if(re.getValid_To().before(Date.valueOf(now))){
                
                checkExpire.add(true);
            }else{
                checkExpire.add(false);
            }
        }
        
        if (!check.equals("4") || listregistration.size() == 0) {
            for (Registration registration : listregistration) {
                listcourse.add(coDao.getCourseByID(registration.getId_Course()));
            }
        }
        for (Course course : listcourse) {
            numberSubject.add(coDao.getNumberSubejectByIdCourse(course.getId_Course()));
        }

        if (!check.equals("4")) {
            request.setAttribute("listregistration", listregistration);
            request.setAttribute("listcourse", listcourse);
        }
        request.setAttribute("numbersubject", numberSubject);
        request.setAttribute("checkexpire", checkExpire);
        request.setAttribute("page", page);
         request.setAttribute("num1", numpage);
        request.setAttribute("listcategory", listcategory);
        request.setAttribute("listmonth", month1);
        request.setAttribute("check", check);
        request.setAttribute("xxx", "mycourse");
        request.getRequestDispatcher("demomycourse.jsp").forward(request, response);
    }

    public ArrayList<Registration> phantrang(ArrayList<Registration> list, String tpage) {
        int numPs = list.size();
        int numperPage = 6;

        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        return getListByPage(list, start, end);
    }

    public ArrayList<Registration> getListByPage(ArrayList<Registration> list,
            int start, int end) {
        ArrayList<Registration> arr = new ArrayList<>();
        for (int i = start; i < end; i++) {
            arr.add(list.get(i));
        }
        return arr;
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
