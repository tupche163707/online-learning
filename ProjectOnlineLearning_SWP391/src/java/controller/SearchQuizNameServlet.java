/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.QuizDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.LevelQuiz;
import model.Quiz;
import model.Subject;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "SearchQuizName", urlPatterns = {"/searchquizname"})
public class SearchQuizNameServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchQuizName</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchQuizName at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String searchvalue = request.getParameter("txtSearch");
        if (searchvalue == null) {
            searchvalue = "";
        }
        QuizDAO qd = new QuizDAO();


        String quizlevelid = request.getParameter("quizlevelid");
        if (quizlevelid == null || "0".equals(quizlevelid)) {
            quizlevelid = "";
        }
        String subjectid = request.getParameter("subjectid");
        if (subjectid == null || "0".equals(subjectid)) {
            subjectid = "";
        }

        List<Quiz> originalListQuiz = qd.filterQuizzesByLevelAndSubject(quizlevelid, subjectid);        

        //Calculate for paging
        int numPs = originalListQuiz.size();
        int numperPage = 5;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }       
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        List<Quiz> list = qd.getListByPage(originalListQuiz, start, end);
        if (paging.equals("false")) {
            list = originalListQuiz;
        }        
        List<Quiz> listQuiz = qd.searchQuizByNameInAList(searchvalue, list);
        //End paging   

        List<String> listsubjectname = qd.getAllQuizSubjectName(listQuiz);
        List<String> listlevelname = qd.getAllQuizLevelName(listQuiz);
        List<Float> listptratio = qd.getAllQuizPassTakeRatio(listQuiz);

        PrintWriter out = response.getWriter();
        if (listQuiz.isEmpty()) {
            out.print("There is no quizzes with name contain " + searchvalue + "!!!");
        } else {
            for (int i = 0; i < listQuiz.size(); i++) {
                out.print("                                                                        <tr class=\"revertable\">\n"
                        + "                                                                            <td>" + (i + 1) + "</td>\n"
                        + "                                                                            <td>" + listQuiz.get(i).getName_Quiz() + "</td>\n"
                        //                    + "                                                                            <!--<td>${listlname.get(i)}</td>-->\n"
                        + "                                                                            <td>" + listsubjectname.get(i) + "</td>\n"
                        + "                                                                            <td>" + listlevelname.get(i) + "</td>\n"
                        + "                                                                            <td>" + listQuiz.get(i).getNumber_Question() + "</td>\n"
                        + "                                                                            <td>" + listQuiz.get(i).getTime_Limit() + "(s)</td>\n"
                        + "                                                                            <td>Multiple Choice</td>\n"
                        + "                                                                            <td>" + listQuiz.get(i).getPasspercent() + "%</td>\n"
                        + "                                                                            <td>\n"
                        + "                                                                                                   " + (float) Math.round(listptratio.get(i) * 10000) / 100 + "\n"
                        + "                                                                            </td>\n"
                        + "                                                                            <td>\n"
                        + "                                                                                <button onclick=\"\" \n"
                        + "                                                                                        class=\"btn\">\n"
                        + "                                                                                    Details\n"
                        + "                                                                                </button>    \n"
                        + "                                                                                <button class=\"btn gradient brown\" \n"
                        + "                                                                                        onclick=\"deleteQuiz(" + listQuiz.get(i).getId_Quiz() + ")\">\n"
                        + "                                                                                    Delete\n"
                        + "                                                                                </button>\n"
                        + "                                                                            </td>\n"
                        + "                                                                        </tr>");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
