/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name="ProfileSale", urlPatterns={"/profilesale"})
public class ProfileSale extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProfileSale</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProfileSale at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        UserDAO userDao = new UserDAO();
        if (u == null) {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            User uprofile = userDao.getUserByIdUser(u.getId_User());
            request.setAttribute("user", uprofile);
            request.getRequestDispatcher("profilesale.jsp").forward(request, response);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
       HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        UserDAO userDao = new UserDAO();
        if (u == null) {
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            String check = request.getParameter("check");
            String iduser = request.getParameter("iduser");
            User update = userDao.getUserByIdUser(Integer.parseInt(iduser));
            if (check.equals("1")) {

                String fullname = request.getParameter("fullname");
                String email = request.getParameter("email");
                String phone = request.getParameter("phone");
                String gender = request.getParameter("gender");
                String address = request.getParameter("address");

                Part filePart = request.getPart("avatar");
                String thumbnail = "";

                boolean checkanh = false;
                if (filePart != null && filePart.getSize() > 0 && (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png"))) {
                    String originalFileName = filePart.getSubmittedFileName();
                    String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                    thumbnail = System.currentTimeMillis() + fileExtension;

                    String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images/post\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images/post\\" + thumbnail;
                    try {
                        FileOutputStream fos = new FileOutputStream(uploadPath);
                        FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                        InputStream is = filePart.getInputStream();
                        byte[] data = new byte[is.available()];
                        is.read(data);
                        fos.write(data);
                        fos1.write(data);
                        fos.close();
                        fos1.close();
                        checkanh = true;
                        System.out.println(checkanh);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (checkanh) {
                    thumbnail = "assets/images/post/" + thumbnail;
                } else {
                    thumbnail = update.getAvatar();
                }
                update.setFull_Name(fullname);
                update.setPhone_Number(phone);
                update.setEmail(email);
                update.setGender(gender.equals("1"));
                update.setAddress(address);
                update.setAvatar(thumbnail);
                if (filePart != null && filePart.getSize() > 0
                        && (!filePart.getSubmittedFileName().endsWith(".jpg")
                        && !filePart.getSubmittedFileName().endsWith(".png"))) {
                    request.setAttribute("classmes", " messagefail");
                    request.setAttribute("user", update);
                    request.setAttribute("mesaddcousrse", "Udpate Profile failed, Try again!");
                    request.setAttribute("mes1", "The image must be in .jpg or .png format");
                    request.getRequestDispatcher("profilesale.jsp").forward(request, response);
                }
                userDao.updateUserById(update);
                request.setAttribute("mesaddcousrse", "Successfully update the profile.");
                request.setAttribute("classmes", " messagesuccess");
                request.setAttribute("user", update);
                request.getRequestDispatcher("profilesale.jsp").forward(request, response);

            } else if (check.equals("2")) {
                String oldpass = request.getParameter("oldpass");
                String newpass = request.getParameter("oldpass");
                String confirmpass = request.getParameter("confirmpass");
                if (update.getPassword().equals(oldpass)) {
                    if (newpass.equals(confirmpass)) {
                        update.setPassword(newpass);
                        userDao.updateUserById(update);
                        request.setAttribute("mesaddcousrse", "Successfully change password.");
                        request.setAttribute("classmes", " messagesuccess");
                        request.setAttribute("user", update);
                        request.getRequestDispatcher("profilesale.jsp").forward(request, response);
                    } else {
                        request.setAttribute("classmes", " messagefail");
                        request.setAttribute("user", update);
                        request.setAttribute("mesaddcousrse", "Change Pass failed, Try again!");
                        request.setAttribute("mespass1", "New password and confirm password not match.");
                        request.setAttribute("oldpass", oldpass);
                        request.setAttribute("newpass", newpass);
                        request.setAttribute("confirmpass", confirmpass);
                        request.getRequestDispatcher("profilesale.jsp").forward(request, response);
                    }
                } else {
                    request.setAttribute("classmes", " messagefail");
                    request.setAttribute("user", update);
                    request.setAttribute("mesaddcousrse", "Change Pass failed, Try again!");
                    request.setAttribute("mespass", "Old Password incorrect!");
                    request.setAttribute("oldpass", oldpass);
                    request.setAttribute("newpass", newpass);
                    request.setAttribute("confirmpass", confirmpass);
                    request.getRequestDispatcher("profilesale.jsp").forward(request, response);
                }
            }

            request.getRequestDispatcher("profilesale.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
