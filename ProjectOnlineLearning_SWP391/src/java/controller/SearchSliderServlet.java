/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Quiz;
import model.Slider;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "SearchSlider", urlPatterns = {"/searchslider"})
public class SearchSliderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchQuizName</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchQuizName at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String searchvalue = request.getParameter("txtSearch");
        if (searchvalue == null) {
            searchvalue = "";
        }
        SliderDAO sd = new SliderDAO();
        List<Slider> originalListQuiz = sd.getAllSliders();

        //Calculate for paging
        int numPs = originalListQuiz.size();
        int numperPage = 4;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        List<Slider> listforsearch = sd.getListByPage(originalListQuiz, start, end);
        if (paging.equals("false")) {
            listforsearch = originalListQuiz;
        }
        List<Slider> list = sd.searchSliderByNameInAList(searchvalue, listforsearch);
        //End paging   

        PrintWriter out = response.getWriter();
        if (list.isEmpty()) {
            out.print("There is no sliders with name contain " + searchvalue + "!!!");
        } else {
            for (int i = 0; i < list.size(); i++) {
                out.print("<tr class=\"revertable\">\n"
                        + "    <td>" + list.get(i).getId_Slider() + "</td>\n"
                        + "    <td>" + list.get(i).getTitle_Slider() + "</td>\n"
                        + "    <td><img src=\"" + list.get(i).getThumbnail_Image() + "\"></td>\n"
                        + "    <td><a href=\"" + list.get(i).getBacklink() + "\"></a></td>\n"
                        + "    <td>\n"
                        + "        <button id=\"chg-btn" + list.get(i).getId_Slider() +"\" onclick=\"changeSliderStatus("+ list.get(i).getId_Slider() + ")\">\n"
                        + "            " + (list.get(i).isStatus() ? "published" : "hidden") + ""
                        + "        </button>\n"
                        + "    </td>\n"
                        + "    <td>" + list.get(i).getCreate_Date() + "</td>\n"
                        + "    <td>" + list.get(i).getUpdate_Date() + "</td>\n"
                        + "    <td>\n"
                        + "        <button onclick=\"location.href = 'sliderdetailservlet?idslider=" + list.get(i).getId_Slider() + "'\" \n"
                        + "                class=\"btn\">\n"
                        + "            Details\n"
                        + "        </button>    \n"
                        + "    </td>\n"
                        + "</tr>");
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
