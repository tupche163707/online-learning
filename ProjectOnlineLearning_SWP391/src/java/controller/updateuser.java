/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.RoleDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import model.User;
import model1.UserJoin;

/**
 *
 * @author PCT
 */
@MultipartConfig()
@WebServlet(name = "updateuser", urlPatterns = {"/updateuser"})
public class updateuser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            UserDAO dao = new UserDAO();
            RoleDAO dao1 = new RoleDAO();
//            String id = request.getParameter("idUser");
            int id = Integer.parseInt(request.getParameter("iduser"));
            UserJoin user = dao.getIDUser(id);
            System.out.println("Lisst" + user);
            List<UserJoin> list = dao.getAllUserTop6();
            request.setAttribute("RoleName", dao1.getAll());
            request.setAttribute("userjoin", user);
            request.setAttribute("list", list);
            request.setAttribute("xxx", "user1");
            request.getRequestDispatcher("updateuser.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO dao = new UserDAO();
        String id_User = request.getParameter("id_User");
        String email = request.getParameter("email");
        String password = request.getParameter("password");
        String full_Name = request.getParameter("full_Name");
        String phone_Number = request.getParameter("phone_Number");
        boolean gender = "1".equals(request.getParameter("gender"));
        String address = request.getParameter("address");
        boolean status = "1".equals(request.getParameter("status"));
        String id_role = request.getParameter("id_role");
        String create_Date = request.getParameter("create_Date");
        String avartar = request.getParameter("avata");
        Part filePart = request.getPart("avatar");
        if (filePart.getSize() > 0) {

            String realPath = request.getServletContext().getRealPath("/");
            File parentDir = new File(realPath).getParentFile();
            File grandParentDir = parentDir.getParentFile();
            String fileName = new File(filePart.getSubmittedFileName()).getName();
            String fileExtension = fileName.substring(fileName.lastIndexOf("."));
            String thubnail = System.currentTimeMillis() + fileExtension;
            String fileWeb = grandParentDir + "\\web\\assets\\images\\courses/" + thubnail;
            try {

                FileOutputStream fos = new FileOutputStream(fileWeb);
                InputStream is = filePart.getInputStream();
                byte[] data = new byte[is.available()];
                is.read(data);
                fos.write(data);
                fos.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
            File file = new File(fileWeb);
            if (!file.exists()) {
                filePart.write(fileWeb);
                avartar = "assets/images/courses/" + thubnail;
                // File doesn't exist, write the file
                try {
                    Thread.sleep(1500); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } else {
                avartar = "assets/images/courses/" + thubnail;
                try {
                    Thread.sleep(1500); // Đợi 0.5 giây (500 milliseconds)
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                // File already exists, do not write
            }

        }

        dao.updateUser(id_User, email, password, full_Name, phone_Number, gender, avartar, address, status, id_role, create_Date); // Xử lý lỗi tại đây      
        String message = "Update User Sucessfull";
        request.getSession().setAttribute("message", message);
        response.sendRedirect("userdetailadmin?iduser=" + id_User);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
