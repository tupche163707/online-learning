/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.UserDAO;
import jakarta.security.auth.message.callback.PrivateKeyCallback;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author Admin
 */
@MultipartConfig()
@WebServlet(name = "UserProfileServlet", urlPatterns = {"/userprofile"})
public class UserProfileServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {
            UserDAO ud = new UserDAO();
            User user = ud.getProfileByEmail(u.getEmail());
            session.setAttribute("user", user);
            request.getRequestDispatcher("profile.jsp").forward(request, response);
            //response.getWriter().print(user.getFull_Name());
            //response.sendRedirect("./profile.jsp");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
   
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");
        if (u == null) {
            response.sendRedirect("login.jsp");
        } else {

            //Part filepath = request.getPart("avatar");
            String name = request.getParameter("full_name");
            boolean gender = request.getParameter("gender").equals("1");
            String address = request.getParameter("address");
            String phone = request.getParameter("phone");
//            String avatar = request.getParameter("avatar");
            User user = new User();

            Part filePart = request.getPart("avatar");
            if (filePart.getSize() > 0) {

                String realPath = request.getServletContext().getRealPath("/");
                File parentDir = new File(realPath).getParentFile();
                File grandParentDir = parentDir.getParentFile();
                String fileName = new File(filePart.getSubmittedFileName()).getName();
                String fileExtension = fileName.substring(fileName.lastIndexOf("."));
                String thubnail = "Avatar" + System.currentTimeMillis() + fileExtension;
                String fileWeb = grandParentDir + "\\web\\assestsAdmin\\images\\Avatar/" + thubnail;
                try {

                    FileOutputStream fos = new FileOutputStream(fileWeb);
                    InputStream is = filePart.getInputStream();
                    byte[] data = new byte[is.available()];
                    is.read(data);
                    fos.write(data);
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                File file = new File(fileWeb);
                if (!file.exists()) {
                    filePart.write(fileWeb);
                    user.setAvatar("assestsAdmin/images/Avatar/" + thubnail);//7
                    // File doesn't exist, write the file
                    try {
                        Thread.sleep(10); // Đợi 0.5 giây (500 milliseconds)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                } else {
                    user.setAvatar("assestsAdmin/images/Avatar/" + thubnail);
                    try {
                        Thread.sleep(10); // Đợi 0.5 giây (500 milliseconds)
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    // File already exists, do not write
                }

            }

            user.setFull_Name(name);
            user.setAddress(address);
            
            user.setGender(gender);
            user.setEmail(u.getEmail());
            user.setPhone_Number(phone);
            user.setId_User(1);
            user.setId_role(1);
            user.setPassword("");
            user.setStatus(true);
            UserDAO uDAO = new UserDAO();
            uDAO.changeProfile(user, u.getEmail());
            response.sendRedirect("userprofile");
        }
            
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
