/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.RegistrationDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.PackagePrice;
import model.Registration;
import model.User;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "CourseRegisterServlet", urlPatterns = {"/courseregister"})
public class RegisterCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseRegisterServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseRegisterServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_course_raw = request.getParameter("idcourse");
        String id_packagePrice_raw = request.getParameter("packageprice");
        PackagePriceDAO pdao = new PackagePriceDAO();
        PackagePriceDAO dao3 = new PackagePriceDAO();
        RegistrationDAO r = new RegistrationDAO();
        CourseDAO cdao = new CourseDAO();
        UserDAO udao = new UserDAO();
        if (id_packagePrice_raw == null) {
            List<PackagePrice> list = dao3.getPackagePriceOfCourse(Integer.parseInt(id_course_raw));
            id_packagePrice_raw = String.valueOf(list.get(0).getId_Package());
        }
        List<User> list = udao.getAuthorById_Course(Integer.parseInt(id_course_raw));
        Course c = cdao.getCourseByID(Integer.parseInt(id_course_raw));
        PackagePrice p = pdao.getPackagePriceById(Integer.parseInt(id_packagePrice_raw));
        ArrayList<PackagePrice> listPrice = dao3.getPackagePriceOfCourse(Integer.parseInt(id_course_raw));
        request.setAttribute("dateNow", r.getCurrentDate());// lay ngay hien tai thong qua RegistraionDAO
        request.setAttribute("author", list);// lay ten cua USER (Author) va id_course tuong ung
        request.setAttribute("listprice", listPrice);// lay gia cua 1 course dua theo course nguoi dung da chon
        request.setAttribute("course", c);//lay course theo parameter duoc truyen tu trang coursedetail
        request.setAttribute("packageprice", p);//lay thong tin gia goi theo parametter duoc truyen tu trang packageprice
        request.getRequestDispatcher("courseregister.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //valid when login
        String id_user_raw = request.getParameter("id_user");
        String id_course_raw = request.getParameter("id_course");
        String registration_date_raw = request.getParameter("registration_date");
        String id_price_package = request.getParameter("idpackageprice");
        String status_raw = request.getParameter("status");
        String valid_from_raw = request.getParameter("valid_from");
        int id_user;
        int id_course;
        Date regis_date;
        int id_packageprice;
        boolean status;
        Date validfrom;
        Date validto;
        RegistrationDAO rsdao = new RegistrationDAO();
        PackagePriceDAO pkdao = new PackagePriceDAO();
        PackagePrice p = pkdao.getPackagePriceById(Integer.parseInt(id_price_package));
        if (id_user_raw == null) {
            String email = request.getParameter("email");
            String fullname = request.getParameter("userFullName");
            String genderraw = request.getParameter("userGender");
            String phonenumber = request.getParameter("userPhoneNumber");
            UserDAO udao = new UserDAO();
            //-------
            User unew = new User();
            unew.setFull_Name(fullname);
            unew.setEmail(email);
            unew.setPassword("123");
            unew.setGender(Boolean.valueOf(genderraw));
            unew.setPhone_Number(phonenumber);
            unew.setId_role(3);
            unew.setStatus(true);
            unew.setAddress("NUll");
            unew.setAvatar("NUll");
            udao.insertNewUser(unew);
            id_user_raw = String.valueOf(udao.getTopUserId());
        }
        try {
            id_user = Integer.parseInt(id_user_raw);
            id_course = Integer.parseInt(id_course_raw);
            regis_date = Date.valueOf(registration_date_raw);
            id_packageprice = Integer.parseInt(id_price_package);
            status = Boolean.valueOf(status_raw);
            validfrom = Date.valueOf(valid_from_raw);
            validto = pkdao.calculateExpirationDate(validfrom, p.getDuration());
            Registration r = new Registration(rsdao.checkid_registration() + 1,
                    id_user, id_course, regis_date, id_packageprice, status, validfrom, validto);
            rsdao.insertToRegistration(r);
            response.sendRedirect("castout.jsp");
        } catch (NumberFormatException e) {
            System.out.println(e);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
