/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CheckBoxLearnDAO;
import dal.LessonDAO;
import dal.RegistrationDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import model.CheckBoxLearn;
import model.Lesson;
import model.Subject;
import model.User;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name = "LessonViewServlet", urlPatterns = {"/lessonview"})
public class LessonViewServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonViewServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonViewServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_subject_raw = request.getParameter("idsubject");
        String id_lesson_raw = request.getParameter("idlesson");
        HttpSession session = request.getSession();
        User u = (User) session.getAttribute("user");

        LessonDAO ldao = new LessonDAO();
        List<Lesson> listlesson = ldao.getLessonBySubjectId(Integer.parseInt(id_subject_raw));//parse id_subject_raw
        SubjectDAO sdao = new SubjectDAO();
        Subject s = sdao.getSubjectById(Integer.parseInt(id_subject_raw)); // parse id_subject_raw
        if (id_lesson_raw != null) {
            Lesson l = ldao.getLessonByIdLesson(Integer.parseInt(id_lesson_raw));
            request.setAttribute("lesson", l);
        }
        int subject_id = Integer.parseInt(id_subject_raw); //Integer.parseInt(id_subject_raw); // parse id_subject_raw
        CheckBoxLearnDAO cbldao = new CheckBoxLearnDAO();
        List<CheckBoxLearn> listcheckbox = cbldao.getAll();

        ArrayList<Boolean> resultList = new ArrayList<>();
        for (Lesson item : listlesson) {
            boolean contains = false;
            for (CheckBoxLearn listItem : listcheckbox) {
                // Kiểm tra xem giá trị id_lesson của hai phần tử có bằng nhau hay không
                if ((item.getId_Lesson() == (listItem.getId_Lesson())) 
                        && (listItem.getId_User() == u.getId_User())) {
                    contains = true;
                    break;
                }
            }

            // Thêm giá trị true hoặc false vào danh sách kết quả
            resultList.add(contains);
        }

        request.setAttribute("xxx", "mycourse");
        request.setAttribute("listboolean", resultList);
        request.setAttribute("listcheckbox", listcheckbox);
        request.setAttribute("subjectid", subject_id);
        request.setAttribute("subjectName", s);
        request.setAttribute("listlesson", listlesson);
        request.getRequestDispatcher("lessonview.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_subject_raw = request.getParameter("Id_Subject");//1
        String userId = request.getParameter("id_User");//1
        String lessonId = request.getParameter("Id_Lesson");//1
        String checkboxStatus = request.getParameter("Check_Status");//true
        String redirectLessonID = request.getParameter("redirectValue");
        LessonDAO ldao = new LessonDAO();
        List<Lesson> listlesson = ldao.getLessonBySubjectId(Integer.parseInt(id_subject_raw));//1

        SubjectDAO sdao = new SubjectDAO();
        Subject s = sdao.getSubjectById(Integer.parseInt(id_subject_raw));//1
        int subject_id = Integer.parseInt(id_subject_raw);

        RegistrationDAO rdao = new RegistrationDAO();
        Date crrd = rdao.getCurrentDate();

        CheckBoxLearn cbl = new CheckBoxLearn();

        cbl.setId_User(Integer.parseInt(userId));
        cbl.setId_Lesson(Integer.parseInt(lessonId));
        cbl.setLearn_Date(crrd);
        CheckBoxLearnDAO cbldao = new CheckBoxLearnDAO();
        CheckBoxLearn cbtest = cbldao.getIdUserAndIdLearn(Integer.parseInt(userId), Integer.parseInt(lessonId));

        boolean checkboxParse;
        if (checkboxStatus != null) {
            checkboxParse = true;
        } else {
            checkboxParse = false;
        }
        if (cbtest == null && checkboxParse) {
            cbldao.insertCheckBoxOfUser(cbl);

        }
        
        request.setAttribute("subjectid", subject_id);
        request.setAttribute("subjectName", s);
        request.setAttribute("listlesson", listlesson);
        //lessonview?idsubject=1&&idlesson=1
        if (redirectLessonID != null) {
            response.sendRedirect("lessonview?idsubject=" + Integer.parseInt(id_subject_raw) + "&&idlesson=" + Integer.parseInt(redirectLessonID));
        } else{
            response.sendRedirect("home");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
