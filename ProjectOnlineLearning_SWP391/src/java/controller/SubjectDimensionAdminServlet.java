/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CatergorySubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.CatergorySubject;
import model.Type_Catergory_Subject;

/**
 *
 * @author GangsterCao
 */
@WebServlet(name="SubjectDimensionAdminServlet", urlPatterns={"/subjectdimensionadmin"})
public class SubjectDimensionAdminServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectDimensionAdminServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectDimensionAdminServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String subjectid = request.getParameter("subjectid");

        CatergorySubjectDAO csdao = new CatergorySubjectDAO();
        List<CatergorySubject> listcs = csdao.getCatergorySubjectJoinTypeCaterSubj();

        List<Type_Catergory_Subject> listtycs = csdao.getTypeCaterSubjJoinWithCaterSubj();
        List<Type_Catergory_Subject> listnone = csdao.getTypeCaterSubj();
        int count = listcs.size();

        request.setAttribute("listDomain", listnone);
        request.setAttribute("sizeL", count);
        request.setAttribute("listTyCaSu", listtycs);
        request.setAttribute("listCateSubj", listcs);
        request.setAttribute("xxx", "SubjectManagment");
        request.setAttribute("xxxDetails", "subjectdimension");
        if (subjectid != null) {
            request.setAttribute("suId", Integer.parseInt(subjectid));
        }
        request.getRequestDispatcher("subjectdimensionAdmin.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String add = request.getParameter("addsubjectdimension");
        if (add != null) {
            String type_cater_Name_Dimension = request.getParameter("type_cater_Name_Dimension");
            String name_cater_Subject = request.getParameter("name_cater_Subject");
            String description_cater_Subject = request.getParameter("description_cater_Subject");
            CatergorySubjectDAO csdao = new CatergorySubjectDAO();
            List<Type_Catergory_Subject> listtcs = csdao.getTypeCaterSubj();
            String clearSpace = type_cater_Name_Dimension.trim();
            int id = 0;
            for (int i = 0; i < listtcs.size(); i++) {
                if (clearSpace.equalsIgnoreCase(listtcs.get(i).getName_Catergory_Subject())) {
                    id = listtcs.get(i).getId_Type_Catergory_Subjec();
                    break;
                }
            }
            if (id == 0) {
                Type_Catergory_Subject tcs = new Type_Catergory_Subject();
                tcs.setId_Type_Catergory_Subjec(listtcs.size() + 1);
                tcs.setName_Catergory_Subject(clearSpace);
                tcs.setDescription(null);
                csdao.AddNewSubjectDimension(tcs);
                CatergorySubject cs = new CatergorySubject();
                cs.setName_Catergory_Subject(name_cater_Subject);
                cs.setDescription_Catergory_Subject(description_cater_Subject);
                cs.setId_Type_Catergory_Subjec(tcs.getId_Type_Catergory_Subjec());
                csdao.AddNewCatergorySubject(cs);
            }
            CatergorySubject cs = new CatergorySubject();
            cs.setName_Catergory_Subject(name_cater_Subject);
            cs.setDescription_Catergory_Subject(description_cater_Subject);
            cs.setId_Type_Catergory_Subjec(id);
            csdao.AddNewCatergorySubject(cs);

            HttpSession session = request.getSession();
            session.setAttribute("val", 5);
            request.getRequestDispatcher("adminsubject").forward(request, response);
        }

        String edit = request.getParameter("editsubjectdimension");
        if (edit != null) {
            String iValue = request.getParameter("iValue");
            String dimensionTable = request.getParameter("dimensionTable"+iValue);
            String nameTable = request.getParameter("nameTable"+iValue);
            String descriptionTable = request.getParameter("descriptionTable"+iValue);
            String dimensionId = request.getParameter("dimensionId"+iValue);
            String catergorySubjectID = request.getParameter("catergorySubjectID"+iValue);
            CatergorySubjectDAO csdao = new CatergorySubjectDAO();
            String cleanDimensionName = dimensionTable.trim();
            int id = 0;
            List<Type_Catergory_Subject> tcs = csdao.getTypeCaterSubj();
            for (Type_Catergory_Subject tc : tcs) {
                if (cleanDimensionName.equalsIgnoreCase(tc.getName_Catergory_Subject())) {
                    id = tc.getId_Type_Catergory_Subjec();
                    break;
                }
            }
            CatergorySubject cs = csdao.getCatergorySubjectById(id);
            cs.setName_Catergory_Subject(nameTable.trim());
            cs.setDescription_Catergory_Subject(descriptionTable.trim());
            cs.setId_Type_Catergory_Subjec(id);
            cs.setId_Catergory_Subject(Integer.parseInt(catergorySubjectID));
            csdao.UpdateIntoCatergorySubject(cs);

            HttpSession session = request.getSession();
            session.setAttribute("val", 6);
            request.getRequestDispatcher("adminsubject").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
