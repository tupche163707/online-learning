/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CheckBoxLearnDAO;
import dal.QuizDAO;
import dal.QuizResultDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.Date;
import model.Quiz;
import model.Result;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "QuizSubmitServlet", urlPatterns = {"/quizsubmit"})
public class QuizSubmitServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizSubmitServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizSubmitServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Quiz cur_quiz = (Quiz) request.getSession().getAttribute("cur_quiz");
        String cur_answer_string = (String) request.getSession().getAttribute("cur_answer_string");
        long start_time = (long) request.getSession().getAttribute("start_time");
        User u = (User) request.getSession().getAttribute("user");

        QuizDAO qd = new QuizDAO();
        String correctAnswer = qd.getCorrectStringAnswerOfQuiz(cur_quiz.getId_Quiz());
        Date now = new Date();
        int duration = (int) (now.getTime() - start_time) / 1000;
        Result res = new Result(new java.sql.Timestamp(start_time),
                duration,
                cur_answer_string,
                correctAnswer,
                u.getId_User(),
                cur_quiz.getId_Quiz());
        QuizResultDAO qrd = new QuizResultDAO();
        int id_Result = qrd.addNewResult(res);

        request.setAttribute("cur_quiz", cur_quiz);
        try ( PrintWriter out = response.getWriter()) {
            out.println(id_Result);
        }
        request.getSession().removeAttribute("cur_quiz");
        request.getSession().removeAttribute("cur_answer_string");
        request.getSession().removeAttribute("start_time");
        request.getSession().removeAttribute("limit_time");
        request.getSession().removeAttribute("peek_time");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        Quiz cur_quiz = (Quiz) request.getSession().getAttribute("cur_quiz");
//        String cur_answer_string = (String) request.getSession().getAttribute("cur_answer_string");
//        Date start_time = (Date) request.getSession().getAttribute("start_time");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
