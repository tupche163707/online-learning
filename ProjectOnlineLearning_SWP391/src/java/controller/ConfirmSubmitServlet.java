/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "ConfirmSubmitServlet", urlPatterns = {"/confirmsubmit"})
public class ConfirmSubmitServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ConfirmSubmitServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ConfirmSubmitServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String cur_answer_string = (String) request.getSession().getAttribute("cur_answer_string");
        String quiz_id_raw = request.getParameter("quiz_id");
        int quiz_id;
        try {
            quiz_id = Integer.parseInt(quiz_id_raw);
        } catch (NumberFormatException e) {
            quiz_id = 0;
        }
        
        int los = cur_answer_string.length();
        int count = 0;
        for (int i = 0; i < cur_answer_string.length(); i++) {
            if (cur_answer_string.charAt(i) != '7') {
                count++;
            }
        }
        String message;
        if (count == 0) {
            message = "You have answered 0 question. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        } else if (count == los) {
            message = "You have answered all the questions. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        } else {
            message = "You have answered only " + count + " questions. By clicking on the "
                    + "[Score Exam] button below, you will "
                    + "complete your current exam and receive your score. "
                    + "You will not be able to change any answers after this point";
        }
        
        request.setAttribute("quiz_id", quiz_id);
        request.setAttribute("message", message);
        request.getRequestDispatcher("scoreexam_popup.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
