/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.SliderDAO;
import dal.UserDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Blog;
import model.Course;
import model.PackagePrice;
import model.Slider;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "HomeServlet", urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        UserDAO ud = new UserDAO();
        SliderDAO sd = new SliderDAO();
        CourseDAO cd = new CourseDAO();
        BlogDAO bd = new BlogDAO();
        PackagePriceDAO ppd = new PackagePriceDAO();

        int numofcus = ud.countCustomer();
        int numofcourse = cd.countCourse();
        List<User> listMarketer = ud.getListUserHasRole(2);
        List<Slider> listSlider = sd.getDisplayedSliders();
        List<Course> listPopularCourse = cd.getCoursesByCreatedDate(false);
        List<PackagePrice> lowestPPrice = ppd.getLowestPakageOfEach();
        List<Blog> listBlog = bd.get8BlogsByUpdateDate(true);
        request.setAttribute("xxx", "home");
        request.setAttribute("numofcus", numofcus);
        request.setAttribute("numofcourse", numofcourse);
        request.setAttribute("listSlider", listSlider);
        request.setAttribute("listMarketer", listMarketer);
        request.setAttribute("listPopularCourse", listPopularCourse);
        request.setAttribute("lowestPPrice", lowestPPrice);
        request.setAttribute("listBlog", listBlog);

        request.getRequestDispatcher("index2.jsp").forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
