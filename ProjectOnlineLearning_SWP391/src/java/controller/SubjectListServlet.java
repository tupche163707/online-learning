/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergorySubjectDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import model.CatergorySubject;
import model.Course;
import model.PackagePrice;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "SubjectListServlet", urlPatterns = {"/subjectlist"})
public class SubjectListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO dao1 = new CourseDAO();
        PackagePriceDAO dao3 = new PackagePriceDAO();
        String checksort = request.getParameter("check1");
        String id_course = request.getParameter("idcourse");
       

        SubjectDAO subDao = new SubjectDAO();
        UserDAO userDao = new UserDAO();
        CatergorySubjectDAO caDao = new CatergorySubjectDAO();
        Course cour = dao1.getCourseByID(Integer.parseInt(id_course));
        
        ArrayList<Subject> listSubject = subDao.getSubjectByCateSubjectInCourse(Integer.parseInt(id_course), Integer.parseInt(checksort));
        
       
        // lay ra cac loai subject co trong khoa hoc
        ArrayList<Integer> listidcategorySubject = subDao.getCategorySubjectInCourse(Integer.parseInt(id_course));
        ArrayList<CatergorySubject> listcater = new ArrayList<>();
        for (Integer integer : listidcategorySubject) {
            listcater.add(caDao.getCatergorySubject(integer));
        }
        
        int numPs = listSubject.size();
        int numperPage = 6;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        String tpage = request.getParameter("page");

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        List<Subject> arrsubject = subDao.getListByPage(listSubject, start, end);
        List<User> author = new ArrayList();
        
        
        for (Subject autho : arrsubject) {
            author.add(userDao.getUserByIdUser(autho.getId_User()));
        }
         
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        ArrayList<PackagePrice> listPrice = dao3.getPackagePriceOfCourse(Integer.parseInt(id_course));
        
        request.setAttribute("author", author);   
        request.setAttribute("date", f.format(cour.getCreate_Date()));
        //request.setAttribute("listca", listcater);
        
        request.setAttribute("xxx", "courselist");
        request.setAttribute("chekphantrang", 2);
        request.setAttribute("check1", checksort);
        request.setAttribute("list2", listcater);
        request.setAttribute("num1", numpage);//
        request.setAttribute("listprice", listPrice);//
        request.setAttribute("page", page);//
        request.setAttribute("list", arrsubject);//
        request.setAttribute("cour", cour);//
        request.setAttribute("idcourse", id_course);//
        request.getRequestDispatcher("SubjectList.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO dao1 = new CourseDAO();
        PackagePriceDAO dao3 = new PackagePriceDAO();
        String subjectname = request.getParameter("subjectname");
        String id_course = request.getParameter("idcourse");
        if(subjectname == null || subjectname.length() == 0){
            subjectname = "";
            
        }

        SubjectDAO subDao = new SubjectDAO();
        UserDAO userDao = new UserDAO();
        CatergorySubjectDAO caDao = new CatergorySubjectDAO();
        Course cour = dao1.getCourseByID(Integer.parseInt(id_course));
        if(subjectname == null || subjectname.length() == 0){
            subjectname = "";
        }
        ArrayList listSubject = subDao.getSubjectByKeyName(subjectname,Integer.parseInt(id_course));
        
        ArrayList<Integer> listidcategorySubject = subDao.getCategorySubjectInCourse(Integer.parseInt(id_course));
        ArrayList<CatergorySubject> listcater = new ArrayList<>();
        for (Integer integer : listidcategorySubject) {
            listcater.add(caDao.getCatergorySubject(integer));
        }
        
        int numPs = listSubject.size();
        int numperPage = 6;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start, end;

        String tpage = request.getParameter("page");

        int page;

        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        start = (page - 1) * numperPage;
        if (page * numperPage > numPs) {
            end = numPs;
        } else {
            end = page * numperPage;
        }

        List<Subject> arrsubject = subDao.getListByPage(listSubject, start, end);
        List<User> author = new ArrayList();
        
        
        for (Subject autho : arrsubject) {
            author.add(userDao.getUserByIdUser(autho.getId_User()));
        }
         
        SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
        ArrayList<PackagePrice> listPrice = dao3.getPackagePriceOfCourse(Integer.parseInt(id_course));
        request.setAttribute("status", 0);
        request.setAttribute("author", author);   
        request.setAttribute("date", f.format(cour.getCreate_Date()));
        //request.setAttribute("listca", listcater);
        
        request.setAttribute("xxx", "courselist");
        
        request.setAttribute("check", 2);
        request.setAttribute("list2", listcater);
        request.setAttribute("num1", numpage);//
        request.setAttribute("listprice", listPrice);//
        request.setAttribute("page", page);//
        request.setAttribute("key", subjectname);
        request.setAttribute("list", arrsubject);//
        request.setAttribute("cour", cour);//
        request.setAttribute("idcourse", id_course);//
        request.setAttribute("key", subjectname);
        request.getRequestDispatcher("course_detail.jsp").forward(request, response);
        
    }

    private boolean ischeck(int d, int[] id) {
        if (id == null) {
            return false;
        } else {
            for (int i = 0; i < id.length; i++) {
                if (id[i] == d) {
                    return true;
                }
            }
            return false;
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
