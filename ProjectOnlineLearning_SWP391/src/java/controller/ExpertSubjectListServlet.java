/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.CatergoryCourseDAO;
import dal.CatergorySubjectDAO;
import dal.CourseDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.CatergorySubject;
import model.Course;
import model.Subject;
import model.SubjectDTO;
import model.User;

/**
 *
 * @author PCT
 */
@WebServlet(name = "ExpertSubjectListServlet", urlPatterns = {"/expertsubjectlistservlet"})
public class ExpertSubjectListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            String categoryID = request.getParameter("categoryID");
            CatergorySubjectDAO dao2 = new CatergorySubjectDAO();
            List<CatergorySubject> list1 = dao2.getAllCatergorySubject();
            SubjectDAO dao1 = new SubjectDAO();
            String id = request.getParameter("iduser");
            if(id == null){
                HttpSession session = request.getSession();
                User user = (User)session.getAttribute("user");
                id = user.getId_User() +"";
            }
            ArrayList<SubjectDTO> list = dao1.getAllSubject(id);
            int numPs = list.size();
            int numperPage = 6;
            int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
            int start, end;

            String tpage = request.getParameter("page");

            int page;

            try {
                page = Integer.parseInt(tpage);
            } catch (NumberFormatException e) {
                page = 1;
            }
            start = (page - 1) * numperPage;
            if (page * numperPage > numPs) {
                end = numPs;
            } else {
                end = page * numperPage;
            }

            List<SubjectDTO> arrsubject = dao1.getListByPage1(list, start, end);
            request.setAttribute("num1", numpage);


            request.setAttribute("page", page);
            request.setAttribute("xxx", "SubjectManagment");

            request.setAttribute("list1", arrsubject);
            request.setAttribute("categoryList", list1);
            request.setAttribute("list", list);
            request.getRequestDispatcher("SubjectListAdmin.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
