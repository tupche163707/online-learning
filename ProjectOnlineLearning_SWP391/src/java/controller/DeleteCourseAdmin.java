/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;


import dal.AnswerDAO;
import dal.CheckBoxLearnDAO;
import dal.CourseDAO;
import dal.LessonDAO;
import dal.PackagePriceDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import dal.QuizResultDAO;
import dal.RegistrationDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.Answer;
import model.CheckBoxLearn;
import model.Lesson;
import model.Question;
import model.Quiz;
import model.Subject;

/**
 *
 * @author PhanQuangHuy59
 */
@WebServlet(name = "DeleteCourseAdmin", urlPatterns = {"/deletecourseadmin"})
public class DeleteCourseAdmin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet DeleteCourseAdmin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet DeleteCourseAdmin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String idcourse = request.getParameter("idcourse");

        CourseDAO courDao = new CourseDAO();
        SubjectDAO subDao = new SubjectDAO();
        PackagePriceDAO packDao = new PackagePriceDAO();
        RegistrationDAO regisDao = new RegistrationDAO();
        LessonDAO lessDao = new LessonDAO();
        QuizDAO quizDao = new QuizDAO();
        CheckBoxLearnDAO checkDao = new CheckBoxLearnDAO();
        QuestionDAO queDao = new QuestionDAO();
        QuizResultDAO resultDao = new QuizResultDAO();
        AnswerDAO answerDao = new AnswerDAO();

        ArrayList<Subject> listSubject = new ArrayList<>();
        ArrayList<Lesson> listLesson = new ArrayList<>();
        ArrayList<Lesson> listLessonTypeQuiz = new ArrayList<>();
        ArrayList<Quiz> listQuiz = new ArrayList<>();
        ArrayList<Question> listQuestion = new ArrayList<>();
        ArrayList<Answer> listAnswer = new ArrayList<>();

        listSubject = subDao.getAllSubjectByIdCourse(Integer.parseInt(idcourse), 0);
        if (listSubject == null || listSubject.size() == 0) {
            packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
            regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
            courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
        } else {
            listLesson = lessDao.getLessonByRangeIdSubject(listSubject);

            if (listLesson == null || listLesson.size() == 0) {
                subDao.DeleteRangeSubjectH(listSubject);
                packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
            } else {
                for (Lesson lesson : listLesson) {
                    if (lesson.getId_Type() == 1) {
                        listLessonTypeQuiz.add(lesson);
                    }
                }
                if (listLessonTypeQuiz.size() == 0) {
                    lessDao.DeleteLessonRangeH(listSubject);
                    subDao.DeleteRangeSubjectH(listSubject);
                    packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                    regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                    courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
                } else {
                    listQuiz = quizDao.GetListQuizByListLesson(listLessonTypeQuiz);
                    if (listQuiz == null || listQuiz.size() == 0) {
                        checkDao.DeleteCheckBoxLearnRange(listLesson);
                        lessDao.DeleteLessonRangeH(listSubject);
                        subDao.DeleteRangeSubjectH(listSubject);
                        packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                        regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                        courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
                    } else {
                        listQuestion = queDao.getQuesstionByRangeIdQuiz(listQuiz);
                        if (listQuestion == null || listQuestion.size() == 0) {
                            resultDao.DeleteQuizRangeH(listQuiz);
                            quizDao.DeleteQuizRange(listLesson);
                            checkDao.DeleteCheckBoxLearnRange(listLesson);
                            lessDao.DeleteLessonRangeH(listSubject);
                            subDao.DeleteRangeSubjectH(listSubject);
                            packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                            regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                            courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
                        } else {
                            listAnswer = answerDao.getAnswerByRangeIdQuesstion(listQuestion);
                            if (listAnswer == null || listAnswer.size() == 0) {
                                queDao.DeleteQuesstionByRangeIdQuiz(listQuiz);
                                resultDao.DeleteQuizRangeH(listQuiz);
                                quizDao.DeleteQuizRange(listLesson);
                                checkDao.DeleteCheckBoxLearnRange(listLesson);
                                lessDao.DeleteLessonRangeH(listSubject);
                                subDao.DeleteRangeSubjectH(listSubject);
                                packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                                regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                                courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
                            } else {
                                answerDao.DeleteAnswerByRangeidQuestion(listQuestion);
                                queDao.DeleteQuesstionByRangeIdQuiz(listQuiz);
                                resultDao.DeleteQuizRangeH(listQuiz);
                                quizDao.DeleteQuizRange(listLesson);
                                checkDao.DeleteCheckBoxLearnRange(listLesson);
                                lessDao.DeleteLessonRangeH(listSubject);
                                subDao.DeleteRangeSubjectH(listSubject);
                                packDao.deletePackagePriceByIdCourse(Integer.parseInt(idcourse));
                                regisDao.deleteRegistrationByIdCourse(Integer.parseInt(idcourse));
                                courDao.deleteCourseAdmin(Integer.parseInt(idcourse));
                            }
                        }
                    }
                }
            }
        }
        request.setAttribute("mesaddcousrse", "Successfully delete the course");
        request.setAttribute("classmes", " messagesuccess");

        request.getRequestDispatcher("admincourselist").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
