/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.BlogDAO;
import dal.CatergoryBlogDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.Blog;
import model.CatergoryBlog;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name = "AddPostMarketing", urlPatterns = {"/addpostmarketing"})
public class AddPostMarketing extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddPostMarketing</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddPostMarketing at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CatergoryBlogDAO cateDao = new CatergoryBlogDAO();

        ArrayList<CatergoryBlog> listCateBlog = new ArrayList<>();
        ArrayList<User> listMarketing = new ArrayList<>();

        listCateBlog = (ArrayList<CatergoryBlog>) cateDao.getAllBlogCate();
        listMarketing = userDao.getMarketing();
        request.setAttribute("datenow", Date.valueOf(LocalDate.now()));
        request.setAttribute("listmarketing", listMarketing);
        request.setAttribute("listcate", listCateBlog);
        request.getRequestDispatcher("addpostmarketing.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        BlogDAO blogDao = new BlogDAO();

        String titlePost = request.getParameter("titlePost");
        String createdatepost = request.getParameter("createdatepost");
        String idcatepost = request.getParameter("catepost");
        String statuspost = request.getParameter("statupost");
        String iduser = request.getParameter("ownpost");
        String brief_infor = request.getParameter("briefinfor");
        String content = request.getParameter("content");

        Part filePart = request.getPart("imagepost");
        Part filePart1 = request.getPart("imagepost");

        Blog blogcheck = blogDao.checkExistBlog(titlePost, -100);
        if (blogcheck == null) {

            String thumbnail = "";
            boolean check1 = false;
            if (filePart != null && filePart.getSize() > 0 && (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png"))) {
                String originalFileName = filePart.getSubmittedFileName();
                String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                thumbnail = System.currentTimeMillis() + fileExtension;

                 String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images\\post\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images\\post\\" + thumbnail;
                try {
                    FileOutputStream fos = new FileOutputStream(uploadPath);
                    FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                    InputStream is = filePart.getInputStream();
                    byte[] data = new byte[is.available()];
                    is.read(data);
                    fos.write(data);
                    fos1.write(data);
                    fos.close();
                    fos1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            thumbnail = "assets/images/post/" + thumbnail;

            Blog b = new Blog();
            b.setTitle(titlePost);
            b.setContent_Blog(content);
            b.setBrief_Infor_Blog(brief_infor);
            b.setId_User(Integer.parseInt(iduser));
            b.setId_CatergoryBlog(Integer.parseInt(idcatepost));
            b.setStatus(statuspost.equals("1"));
            b.setThumbnail_Blog(thumbnail);
            b.setCreate_Date(Date.valueOf(LocalDate.now()));
            if (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png")) {
                blogDao.insertBlog(b);
                request.setAttribute("post", b);
                request.setAttribute("mesaddcousrse", "Successfully add the post to the EduChamp system");
                request.setAttribute("classmes", " messagesuccess");
            } else {
                request.setAttribute("post", b);
                request.setAttribute("classmes", " messagefail");
               request.setAttribute("mesaddcousrse", "Add New Post failed, Try again!");
               request.setAttribute("mes1", "The image must be in .jpg or .png format");
            }

        } else {
            Blog b = new Blog();
            b.setTitle(titlePost);
            b.setContent_Blog(content);
            b.setBrief_Infor_Blog(brief_infor);
            b.setId_User(Integer.parseInt(iduser));
            b.setId_CatergoryBlog(Integer.parseInt(idcatepost));
            b.setStatus(statuspost.equals("1"));
            b.setCreate_Date(Date.valueOf(LocalDate.now()));
            request.setAttribute("post", b);
            request.setAttribute("mesaddcousrse", "Add New Post failed, Try again!");
            request.setAttribute("classmes", " messagefail");
            request.setAttribute("mes", "Failded add the post to the EduChamp system");
        }

        UserDAO userDao = new UserDAO();
        CatergoryBlogDAO cateDao = new CatergoryBlogDAO();

        ArrayList<CatergoryBlog> listCateBlog = new ArrayList<>();
        ArrayList<User> listMarketing = new ArrayList<>();

        listCateBlog = (ArrayList<CatergoryBlog>) cateDao.getAllBlogCate();
        listMarketing = userDao.getMarketing();

        request.setAttribute("listmarketing", listMarketing);
        request.setAttribute("listcate", listCateBlog);
        request.getRequestDispatcher("addpostmarketing.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
