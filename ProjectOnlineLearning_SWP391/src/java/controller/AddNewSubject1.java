/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.CatergorySubjectDAO;
import dal.CourseDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import model.CatergorySubject;
import model.Course;
import model.Subject;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name="AddNewSubject1", urlPatterns={"/addnewsubject1"})
public class AddNewSubject1 extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddNewSubject1</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddNewSubject1 at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CatergorySubjectDAO caDao = new CatergorySubjectDAO();
        CourseDAO couDao = new CourseDAO();

        ArrayList<Course> listcourse = (ArrayList<Course>) couDao.getAllCourses();
        ArrayList<User> listExpert = userDao.getExpert();
        ArrayList<CatergorySubject> listCa = (ArrayList<CatergorySubject>) caDao.getAllCatergorySubject();
        LocalDate date = LocalDate.now();
        request.setAttribute("listcourse", listcourse);
        request.setAttribute("liste", listExpert);
        request.setAttribute("listc", listCa);
        request.setAttribute("date", date.toString());
        request.getRequestDispatcher("addnewsubject.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CatergorySubjectDAO caDao = new CatergorySubjectDAO();
        CourseDAO couDao = new CourseDAO();
        SubjectDAO sub = new SubjectDAO();

        String courseName = request.getParameter("nameCourse");
        String statusCourse = request.getParameter("statuCourse");
        String categoryCourse = request.getParameter("categoryCourse");
        String expertCourse = request.getParameter("expert");
        String courseForSubject = request.getParameter("course");
        String description = request.getParameter("description");

        Part filePart = request.getPart("imageCourse");
        Part filePart1 = request.getPart("imageCourse");
        boolean status = statusCourse.equals("1") ? true : false;

        // check Course By Name 
        Subject course = sub.getSubjectByKeyNameForAddSubject(courseName);

        if (course == null && (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png")
                || filePart.getSubmittedFileName().endsWith(".PNG"))) {

            ArrayList<User> listExpert = userDao.getExpert();
            ArrayList<CatergorySubject> listCa = (ArrayList<CatergorySubject>) caDao.getAllCatergorySubject();
            ArrayList<Course> listcourse = (ArrayList<Course>) couDao.getAllCourses();
            LocalDate date = LocalDate.now();

            String thumbnail = "";
            if (filePart != null && filePart.getSize() > 0) {
                String originalFileName = filePart.getSubmittedFileName();
                String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                thumbnail = System.currentTimeMillis() + fileExtension;



                 String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images\\courses\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images\\courses\\" + thumbnail;

                try {
                    FileOutputStream fos = new FileOutputStream(uploadPath);
                    FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                    InputStream is = filePart.getInputStream();
                    byte[] data = new byte[is.available()];
                    is.read(data);
                    fos.write(data);
                    fos1.write(data);
                    fos.close();
                    fos1.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            Subject c = new Subject();
            c.setSubject_Name(courseName);
            c.setDescription(description);
            c.setCreate_Date(Date.valueOf(date));
            c.setUpdate_Date(Date.valueOf(date));
            c.setStatus(status);
            c.setImage_subject("./assets/images/courses/" + thumbnail);
            c.setId_Catergory_Subject(Integer.parseInt(categoryCourse));
            c.setId_User(Integer.parseInt(expertCourse));
            c.setId_Course(Integer.parseInt(courseForSubject));
            sub.InsertSubject(c);

            //User u = userDao.getUserByIdUser(Integer.parseInt(expertCourse));
            //sentMailForExpert(u, courseName);
            request.setAttribute("mesaddcousrse", "Successfully added the course to the EduChamp system");
            request.setAttribute("classmes", " messagesuccess");
            request.setAttribute("courseName", courseName);
            request.setAttribute("imageCourse", "./assets/images/courses/" + thumbnail);
            request.setAttribute("statusCourse", statusCourse);
            request.setAttribute("categoryCourse", categoryCourse);
            request.setAttribute("expertCourse", expertCourse);
            request.setAttribute("description", description);
            request.setAttribute("liste", listExpert);
            request.setAttribute("listc", listCa);
            request.setAttribute("listcourse", listcourse);
            request.setAttribute("course", course);
            request.setAttribute("date", date.toString());
            request.getRequestDispatcher("addnewsubject.jsp").forward(request, response);
        } else {
            if (!filePart.getSubmittedFileName().endsWith(".jpg") && !filePart.getSubmittedFileName().endsWith(".jpg")) {
                request.setAttribute("mes1", "The image file format is not in the format allowed by the system");
            }
            ArrayList<User> listExpert = userDao.getExpert();
            ArrayList<CatergorySubject> listCa = (ArrayList<CatergorySubject>) caDao.getAllCatergorySubject();
            ArrayList<Course> listcourse = (ArrayList<Course>) couDao.getAllCourses();
            LocalDate date = LocalDate.now();
            request.setAttribute("courseName", courseName);
            request.setAttribute("statusCourse", statusCourse);
            request.setAttribute("categoryCourse", categoryCourse);
            request.setAttribute("expertCourse", expertCourse);
            request.setAttribute("description", description);
            request.setAttribute("liste", listExpert);
            request.setAttribute("listc", listCa);
            request.setAttribute("date", date.toString());
            request.setAttribute("imageCourse", "./assets/images/courses/" + filePart.getSubmittedFileName());
            if (course != null) {
                request.setAttribute("mes", "This course currently exists and cannot be added");
            }
            request.setAttribute("mesaddcousrse", "Add courses failed, Try again");
            request.setAttribute("classmes", " messagefail");
            request.setAttribute("liste", listExpert);
            request.setAttribute("listc", listCa);
            request.setAttribute("listcourse", listcourse);
            request.setAttribute("date", date.toString());
            request.getRequestDispatcher("addnewsubject.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
