/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.SliderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Quiz;
import model.Slider;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "SlidersListServlet", urlPatterns = {"/sliderslist"})
public class SlidersListServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SlidersListServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SlidersListServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SliderDAO sd = new SliderDAO();
        String tmp = request.getParameter("status");
        int status;
        List<Slider> initList;
        try {
            status = Integer.parseInt(tmp);
        } catch (NumberFormatException e) {
            status = 2;
        }
        initList = sd.getSlidersOfStatus(status);
        List<Slider> list;
        String message = "";
//Calculate for paging
        int numPs = initList.size();
        int numperPage = 4;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start;
        int end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        if (initList.isEmpty()) {
            message = "There is no quizzes available!!!";
            request.setAttribute("paging", paging);
            request.setAttribute("message", message);
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        list = sd.getListByPage(initList, start, end);
        if (paging.equals("false")) {
            page = 1;
            numpage = 1;
            list = initList;
        }
        List<String> listUN = sd.getAllSliderAuthorName(list);
        //End paging        
        request.setAttribute("status", status);
        request.setAttribute("numpage", numpage);
        request.setAttribute("curunpet", page);
        request.setAttribute("paging", paging);
        request.setAttribute("message", message);
        request.setAttribute("listSlider", list);
        request.setAttribute("listUN", listUN);
        String temp = request.getParameter("addMessage");
        System.out.println(temp);
        request.setAttribute("addMessage", temp);
        
        request.getRequestDispatcher("sliderslist.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        SliderDAO sd = new SliderDAO();
        String tmp = request.getParameter("status");
        int status;
        List<Slider> initList;
        try {
            status = Integer.parseInt(tmp);
        } catch (NumberFormatException e) {
            status = 2;
        }
        initList = sd.getSlidersOfStatus(status);
        List<Slider> list;
        String message = "";
//Calculate for paging
        int numPs = initList.size();
        int numperPage = 4;
        int numpage = numPs / numperPage + (numPs % numperPage == 0 ? 0 : 1);
        int start;
        int end;
        String tpage = request.getParameter("page");
        String paging = request.getParameter("paging");
        if (paging == null) {
            paging = "true";
        }
        int page;
        try {
            page = Integer.parseInt(tpage);
        } catch (NumberFormatException e) {
            page = 1;
        }
        if (page <= 0) {
            page = 1;
        }
        if (page > numpage) {
            page = numpage;
        }
        if (initList.isEmpty()) {
            message = "There is no quizzes available!!!";
            request.setAttribute("paging", paging);
            request.setAttribute("message", message);
            request.getRequestDispatcher("quizzeslist.jsp").forward(request, response);
        }
        start = (page - 1) * numperPage;
        end = Math.min(page * numperPage, numPs);
        list = sd.getListByPage(initList, start, end);
        if (paging.equals("false")) {
            page = 1;
            numpage = 1;
            list = initList;
        }
        List<String> listUN = sd.getAllSliderAuthorName(list);
        //End paging        
        request.setAttribute("status", status);
        request.setAttribute("numpage", numpage);
        request.setAttribute("curunpet", page);
        request.setAttribute("paging", paging);
        request.setAttribute("message", message);
        request.setAttribute("listSlider", list);
        request.setAttribute("listUN", listUN);
        String temp = request.getParameter("addMessage");
        System.out.println(temp);
        request.setAttribute("addMessage", temp);

        request.getRequestDispatcher("sliderslist.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
