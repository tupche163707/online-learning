/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.RegisterDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Random;
import model.User;

/**
 *
 * <<<<<<<<
 * HEAD:ProjectOnlineLearning_SWP391/src/java/controller/RegisterServlet.java
 * @author vuduy
 */
@WebServlet(name = "RegisterServlet", urlPatterns = {"/registerservlet"})
public class RegisterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResetPassword</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResetPassword at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String name = request.getParameter("Name");
        String emailInput = request.getParameter("Email");
        String pass = request.getParameter("Password");
        String repass = request.getParameter("rePassword");
        String phone = request.getParameter("phone");
        String gender = request.getParameter("male");

        RegisterDAO r = new RegisterDAO();
        User u = r.checkRegister(emailInput);

        if (u != null) {
            request.setAttribute("mesfail", "This account already exists in the system."
                    + " Please register with another account");
            request.setAttribute("name", name);
            request.setAttribute("email", emailInput);
            request.setAttribute("pass", pass);
            request.setAttribute("repass", repass);
            request.setAttribute("phone", phone);
            request.setAttribute("male", gender);
            request.getRequestDispatcher("register.jsp").forward(request, response);
        } else {
            if (!pass.equals(repass)) {
                request.setAttribute("mesfail", "Repeat password does not match");
                request.setAttribute("name", name);
                request.setAttribute("email", emailInput);
                request.setAttribute("pass", pass);
                request.setAttribute("repass", repass);
                request.setAttribute("phone", phone);
                request.setAttribute("male", gender);
                request.getRequestDispatcher("register.jsp").forward(request, response);
            } else {
//                request.setAttribute("message", "Account registration successful, you can login now");
//                request.getRequestDispatcher("login.jsp").forward(request, response);
                String codesend = request.getParameter("codesend");
                if (codesend == null || codesend.length() == 0) {
                    HttpSession session = request.getSession();
                    session.setMaxInactiveInterval(300);
                    Random ran = new Random();
                    int codemail = (ran.nextInt(1000000) + 100000);
                    SendEmail(codemail +"", emailInput);
                    session.setAttribute("code", codemail);
                    request.setAttribute("check", true);
                    request.setAttribute("name", name);
                    request.setAttribute("email", emailInput);
                    request.setAttribute("pass", pass);
                    request.setAttribute("repass", repass);
                    request.setAttribute("phone", phone);
                    request.setAttribute("male", gender);
                    request.getRequestDispatcher("register.jsp").forward(request, response);
                } else {
                    String codeenter = request.getParameter("codeenter");
                    if (codeenter != null) {
                        if (codeenter.equals(codesend)) {
                            r.Register(emailInput, pass, name,phone,gender);
                            HttpSession session = request.getSession();
                            session.removeAttribute("code");
                            request.setAttribute("check", false);
                            request.getRequestDispatcher("login.jsp").forward(request, response);
                        } else {
                            request.setAttribute("codefail", "Registration code is wrong");
                            request.setAttribute("name", name);
                            request.setAttribute("email", emailInput);
                            request.setAttribute("pass", pass);
                            request.setAttribute("repass", repass);
                            request.setAttribute("code", codeenter);
                            request.setAttribute("phone", phone);
                            request.setAttribute("male", gender);
                            request.setAttribute("check", true);
                            request.getRequestDispatcher("register.jsp").forward(request, response);
                        }
                    } else {
                        request.getRequestDispatcher("register.jsp").forward(request, response);
                    }

                }

            }
        }

        // Cấu hình properties
    }

    public void SendEmail(String code, String emailUser) {
        try {

            String from = "CustomerService94321@gmail.com"; // Thay đổi địa chỉ email người nhận
            String password = "lwrtmwkgshlqaycp"; // Thay đổi mật khẩu email của bạn
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", 587);
            properties.put("mail.smtp.starttls.enable", true);
            properties.put("mail.transport.protocl", "smtp");
            Authenticator auth;

            auth = new Authenticator() {
                @Override
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new javax.mail.PasswordAuthentication(from, password);
                }
            };

            Session session = Session.getInstance(properties, auth);
            Message message = new MimeMessage(session);
            message.setSubject("Email From my EduCourse.");

            message.setContent("<div class=\"container\" style=\" width: 90%;\n"
                    + "    border: 2px solid black;\n"
                    + "    text-align: center;\n"
                    + "    border-radius: 2%;\n"
                    + "    margin: 20px auto;\n"
                    + "    display: flex;\n"
                    + "    height: fit-content;\n"
                    + "    overflow: hidden;\">\n"
                    + "      <div class=\"img\" style=\"width: 40%;\n"
                    + "      margin: auto 10px;\">\n"
                    + "        <div style=\"width: 100%; text-align: center;\">\n"
                    + "            <h1 style=\"font-family: Helvetica Neue, Arial, sans-serif;\n"
                    + "            font-size: 64px;\n"
                    + "            font-weight: bold;\n"
                    + "            color: #333;\n"
                    + "            text-align: center;\n"
                    + "            text-transform: uppercase;\n"
                    + "            letter-spacing: 2px;\n"
                    + "            margin: 20px 0;\n"
                    + "            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);\n"
                    + "            background-image: linear-gradient(to right, #8f047c, #8f047c);\n"
                    + "            -webkit-background-clip: text;\n"
                    + "            -webkit-text-fill-color: transparent;\">EduChamp</h1>\n"
                    + "            <h4 style=\"font-family: Helvetica Neue, Arial, sans-serif;\n"
                    + "            font-size: 24px;\n"
                    + "            font-weight: bold;\n"
                    + "            color: #333;\n"
                    + "            text-align: center;\n"
                    + "            text-transform: uppercase;\n"
                    + "            letter-spacing: 2px;\n"
                    + "            margin: 20px 0;\n"
                    + "            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);\n"
                    + "            background-image: linear-gradient(to right, #8f047c, #8f047c);\n"
                    + "            -webkit-background-clip: text;\n"
                    + "            -webkit-text-fill-color: transparent;\">Education & Courses</h4>\n"
                    + "        </div>\n"
                    + "      </div>\n"
                    + "      <div class=\"ma\" style=\"width: 60%;\n"
                    + "      padding: 10px;\n"
                    + "      background-color: rgb(203, 175, 229);\n"
                    + "      overflow: hidden;\">\n"
                    + "        <div class=\"thongdiep\">\n"
                    + "          <b style=\"font-size: 30px;\n"
                    + "          color: rgb(65, 61, 61);\n"
                    + "          font-weight: bold;\">EduChamp Education & Courses sends you a code.<br>Please enter it to resetpassword</br>\n"
                    + "          <div class=\"maso\" style=\"padding: 20px;\n"
                    + "          font-size: 30px;\n"
                    + "          color: rgb(7, 125, 7);\n"
                    + "          font-weight: bold;\">"
                    + code
                    + "</div>\n"
                    + "        </div>\n"
                    + "\n"
                    + "        <div class=\"thongdiep\" style=\"padding: 20px 10px;\n"
                    + "        border: 2px solid black;\n"
                    + "        border-radius: 5px;\n"
                    + "        margin-bottom: 10px;\">\n"
                    + "          <p style=\" text-decoration: none;\n"
                    + "          font-style:italic;font-size: 20px;\">\n"
                    + "            Or you can go to the change password page.Click the button below\n"
                    + "          </p>\n"
                    + "          <a href=\"http://localhost:9999/projectswp391/resetpassword\" style=\"background-color: rgb(43, 200, 43);color: white;\n"
                    + "          text-decoration: none;\n"
                    + "          padding: 20px;\n"
                    + "          display: block;\n"
                    + "          margin-top: 20px;\n"
                    + "          border: 1px solid black;\n"
                    + "          font-size: 30px;\n"
                    + "          font-weight: bold;\">xac thuc</a>\n"
                    + "        </div>\n"
                    + "      </div>\n"
                    + "    </div>", "text/html");

            Address addressTo = new InternetAddress(emailUser);
            message.setRecipient(Message.RecipientType.TO, addressTo);

            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(ResetPasswordServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}