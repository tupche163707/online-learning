package controller;

import dal.CourseCategoryDAO;
import dal.CourseDAO;
import dal.PackagePriceDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Address;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import model.CatergoryCourse;
import model.Course;
import model.User;

/**
 *
 * @author PhanQuangHuy59
 */
@MultipartConfig()
@WebServlet(name = "AddCourseServlet", urlPatterns = {"/addcourse"})
public class AddCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCourseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO caDao = new CourseCategoryDAO();
        CourseDAO couDao = new CourseDAO();
        ArrayList<User> listExpert = userDao.getExpert();
        ArrayList<CatergoryCourse> listCa = caDao.getAllCatergory();
        LocalDate date = LocalDate.now();
        request.setAttribute("liste", listExpert);
        request.setAttribute("listc", listCa);
        request.setAttribute("date", date.toString());
        request.getRequestDispatcher("addcourse.jsp").forward(request, response);

    }

    public void sentMailForExpert(User user, String nameCourse) {
        try {

            String from = "CustomerService94321@gmail.com"; // Thay đổi địa chỉ email người nhận
            String password = "lwrtmwkgshlqaycp"; // Thay đổi mật khẩu email của bạn
            Properties properties = new Properties();
            properties.put("mail.smtp.auth", "true");
            properties.put("mail.smtp.host", "smtp.gmail.com");
            properties.put("mail.smtp.port", 587);
            properties.put("mail.smtp.starttls.enable", true);
            properties.put("mail.transport.protocl", "smtp");
            Authenticator auth;

            auth = new Authenticator() {
                @Override
                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new javax.mail.PasswordAuthentication(from, password);
                }
            };

            Session session = Session.getInstance(properties, auth);
            Message message = new MimeMessage(session);
            message.setSubject("Email From my EduCourse For Expert : " + user.getFull_Name());

            message.setContent("<div class=\"container\" style=\" width: 90%;\n"
                    + "    border: 2px solid black;\n"
                    + "    text-align: center;\n"
                    + "    border-radius: 2%;\n"
                    + "    margin: 20px auto;\n"
                    + "    display: flex;\n"
                    + "    height: fit-content;\n"
                    + "    overflow: hidden;\">\n"
                    + "      <div class=\"img\" style=\"width: 40%;\n"
                    + "      margin: auto 10px;\">\n"
                    + "        <div style=\"width: 100%; text-align: center;\">\n"
                    + "            <h1 style=\"font-family: Helvetica Neue, Arial, sans-serif;\n"
                    + "            font-size: 64px;\n"
                    + "            font-weight: bold;\n"
                    + "            color: #333;\n"
                    + "            text-align: center;\n"
                    + "            text-transform: uppercase;\n"
                    + "            letter-spacing: 2px;\n"
                    + "            margin: 20px 0;\n"
                    + "            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);\n"
                    + "            background-image: linear-gradient(to right, #8f047c, #8f047c);\n"
                    + "            -webkit-background-clip: text;\n"
                    + "            -webkit-text-fill-color: transparent;\">EduChamp</h1>\n"
                    + "            <h4 style=\"font-family: Helvetica Neue, Arial, sans-serif;\n"
                    + "            font-size: 24px;\n"
                    + "            font-weight: bold;\n"
                    + "            color: #333;\n"
                    + "            text-align: center;\n"
                    + "            text-transform: uppercase;\n"
                    + "            letter-spacing: 2px;\n"
                    + "            margin: 20px 0;\n"
                    + "            text-shadow: 2px 2px 4px rgba(0, 0, 0, 0.2);\n"
                    + "            background-image: linear-gradient(to right, #8f047c, #8f047c);\n"
                    + "            -webkit-background-clip: text;\n"
                    + "            -webkit-text-fill-color: transparent;\">Education & Courses</h4>\n"
                    + "        </div>\n"
                    + "      </div>\n"
                    + "      <div class=\"ma\" style=\"width: 60%;\n"
                    + "      padding: 10px;\n"
                    + "      background-color: rgb(203, 175, 229);\n"
                    + "      overflow: hidden;\">\n"
                    + "        <div class=\"thongdiep\">\n"
                    + "          <b style=\"font-size: 30px;\n"
                    + "          color: rgb(65, 61, 61);\n"
                    + "          font-weight: bold;\">The EduChamp Learning and Course System has asked you to prepare subjects. <br>Please log in to your account to start preparing for the course : </br>\n"
                    + nameCourse
                    + "          <div class=\"maso\" style=\"padding: 20px;\n"
                    + "          font-size: 30px;\n"
                    + "          color: rgb(7, 125, 7);\n"
                    + "          font-weight: bold;\">"
                    + "</div>\n"
                    + "        </div>\n"
                    + "\n"
                    + "        <div class=\"thongdiep\" style=\"padding: 20px 10px;\n"
                    + "        border: 2px solid black;\n"
                    + "        border-radius: 5px;\n"
                    + "        margin-bottom: 10px;\">\n"
                    + "          <p style=\" text-decoration: none;\n"
                    + "          font-style:italic;font-size: 20px;\">\n"
                    + "            Edu Champ to teachers : \n"
                    + "          </p>\n"
                    + "          <a href=\"#\" style=\"background-color: rgb(43, 200, 43);color: white;\n"
                    + "          text-decoration: none;\n"
                    + "          padding: 20px;\n"
                    + "          display: block;\n"
                    + "          margin-top: 20px;\n"
                    + "          border: 1px solid black;\n"
                    + "          font-size: 30px;\n"
                    + "          font-weight: bold;\">" + user.getFull_Name() + "</a>\n"
                    + "        </div>\n"
                    + "      </div>\n"
                    + "    </div>", "text/html");

            Address addressTo = new InternetAddress(user.getEmail());
            message.setRecipient(Message.RecipientType.TO, addressTo);

            Transport.send(message);
        } catch (MessagingException ex) {
            Logger.getLogger(AddCourseServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO userDao = new UserDAO();
        CourseCategoryDAO caDao = new CourseCategoryDAO();
        CourseDAO couDao = new CourseDAO();
        String check = request.getParameter("check");

        if (check != null) {

            String courseName = request.getParameter("nameCourse");
            String statusCourse = request.getParameter("statuCourse");
            String categoryCourse = request.getParameter("categoryCourse");
            String expertCourse = request.getParameter("expert");
            String description = request.getParameter("description");
            String duration = request.getParameter("duration");
            String listprice = request.getParameter("listprice");
            String saleprice = request.getParameter("saleprice");
            Part filePart = request.getPart("imageCourse");
            Part filePart1 = request.getPart("imageCourse");
            boolean status = statusCourse.equals("1") ? true : false;

            // check Course By Name 
            Course course = couDao.getCourseName(courseName);

            if (course.getCourse_Name() == null && (filePart.getSubmittedFileName().endsWith(".jpg") || filePart.getSubmittedFileName().endsWith(".png")
                    || filePart.getSubmittedFileName().endsWith(".PNG"))) {

                ArrayList<User> listExpert = userDao.getExpert();
                ArrayList<CatergoryCourse> listCa = caDao.getAllCatergory();
                LocalDate date = LocalDate.now();
                
                 String thumbnail ="";
                if (filePart != null && filePart.getSize() > 0) {
                    String originalFileName = filePart.getSubmittedFileName();
                    String fileExtension = originalFileName.substring(originalFileName.lastIndexOf("."));
                    thumbnail = System.currentTimeMillis() + fileExtension;

                    String uploadPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\web\\assets\\images\\courses\\" + thumbnail;
                    String uploadBuildPath = "B:\\FPT_Uni\\SU23\\SWP391\\PROJECT_SWP391\\Final\\14-07-2023 clone\\summer2023-swp391.se1714-g2\\ProjectOnlineLearning_SWP391\\build\\web\\assets\\images\\courses\\" + thumbnail;
                    try {
                        FileOutputStream fos = new FileOutputStream(uploadPath);
                        FileOutputStream fos1 = new FileOutputStream(uploadBuildPath);
                        InputStream is = filePart.getInputStream();
                        byte[] data = new byte[is.available()];
                        is.read(data);
                        fos.write(data);
                        fos1.write(data);
                        fos.close();
                        fos1.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }    
                }
                Course c = new Course();
                c.setCourse_Name(courseName);
                c.setDescription_Course(description);
                c.setCreate_Date(Date.valueOf(date));
                c.setUpdate_Date(Date.valueOf(date));
                c.setStatus(status);
                c.setImage("./assets/images/courses/"+thumbnail);
                c.setId_Category(Integer.parseInt(categoryCourse));
                c.setId_User(Integer.parseInt(expertCourse));
                couDao.InsertCourse(c, Integer.parseInt(statusCourse));
                int duration1 = Integer.parseInt(duration);
                int listprice1 = Integer.parseInt(listprice);
                int saleprice1 = Integer.parseInt(saleprice);
                int idcourse = couDao.getmaxidcourse();
                PackagePriceDAO p = new PackagePriceDAO();
                p.insertPackagePrice(duration1, listprice1, saleprice1, idcourse);
                User u = userDao.getUserByIdUser(Integer.parseInt(expertCourse));
                //sentMailForExpert(u, courseName);
                request.setAttribute("mesaddcousrse", "Successfully added the course to the EduChamp system");
                request.setAttribute("classmes", " messagesuccess");
                request.setAttribute("courseName", courseName);
                request.setAttribute("imageCourse", "./assets/images/courses/"+thumbnail);
                request.setAttribute("statusCourse", statusCourse);
                request.setAttribute("categoryCourse", categoryCourse);
                request.setAttribute("expertCourse", expertCourse);
                request.setAttribute("description", description);
                request.setAttribute("liste", listExpert);
                request.setAttribute("listc", listCa);
                request.setAttribute("duration", duration);
                request.setAttribute("listprice", listprice);
                request.setAttribute("saleprice", saleprice);
                request.setAttribute("date", date.toString());
                request.getRequestDispatcher("addcourse.jsp").forward(request, response);
            } else {
                if (!filePart.getSubmittedFileName().endsWith(".jpg")&& !filePart.getSubmittedFileName().endsWith(".jpg")) {
                    request.setAttribute("mes1", "The image file format is not in the format allowed by the system");
                }
                ArrayList<User> listExpert = userDao.getExpert();
                ArrayList<CatergoryCourse> listCa = caDao.getAllCatergory();
                LocalDate date = LocalDate.now();
                request.setAttribute("courseName", courseName);
                request.setAttribute("statusCourse", statusCourse);
                request.setAttribute("categoryCourse", categoryCourse);
                request.setAttribute("expertCourse", expertCourse);
                request.setAttribute("description", description);
                request.setAttribute("liste", listExpert);
                request.setAttribute("listc", listCa);
                request.setAttribute("date", date.toString());
                request.setAttribute("duration", duration);
                request.setAttribute("listprice", listprice);
                request.setAttribute("saleprice", saleprice);
                 request.setAttribute("imageCourse", "./assets/images/courses/" + filePart.getSubmittedFileName());
                if (course.getCourse_Name() != null) {
                    request.setAttribute("mes", "This course currently exists and cannot be added");
                }
                request.setAttribute("mesaddcousrse", "Add courses failed, Try again");
                request.setAttribute("classmes", " messagefail");
                request.setAttribute("liste", listExpert);
                request.setAttribute("listc", listCa);
                request.setAttribute("date", date.toString());
                request.getRequestDispatcher("addcourse.jsp").forward(request, response);
            }
        } else {

            ArrayList<User> listExpert = userDao.getExpert();
            ArrayList<CatergoryCourse> listCa = caDao.getAllCatergory();
            LocalDate date = LocalDate.now();
            request.setAttribute("liste", listExpert);
            request.setAttribute("listc", listCa);
            request.setAttribute("date", date.toString());
            request.getRequestDispatcher("addcourse.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

