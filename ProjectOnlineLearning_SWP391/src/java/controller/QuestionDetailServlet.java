package controller;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

import dal.AnswerDAO;
import dal.CatergorySubjectDAO;
import dal.LessonDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import dal.SubjectDAO;
import dal.TypeQuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Answer;
import model.Question;
import model.Subject;
import model.Type_Question;

/**
 *
 * @author Admin
 */
@WebServlet(urlPatterns = {"/questiondetail"})
public class QuestionDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuestionDetailServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuestionDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int id = Integer.parseInt(request.getParameter("qid"));
        
        QuestionDAO qdao = new QuestionDAO();
        Question q = qdao.getQuestionById2(id);
        request.setAttribute("q", q);
        
        SubjectDAO sdao = new SubjectDAO();
        String s = sdao.getSubjectByIdQuestion(id);
        request.setAttribute("s", s);
        
        CatergorySubjectDAO csdao = new CatergorySubjectDAO();
        String cs = csdao.getCategorySubjectByIdquestion(id);
        request.setAttribute("cs", cs);
        
        LessonDAO ldao = new LessonDAO();
        String l = ldao.getLessonByIdquestion(id);
        request.setAttribute("l", l);
        
        QuizDAO qidao = new QuizDAO();
        String qi = qidao.getQuizByIdquestion(id);
        request.setAttribute("qi", qi);
        
        String type = qdao.getTypeByIdQuestion(id);
        request.setAttribute("type", type);
        
        AnswerDAO adao = new AnswerDAO();
        ArrayList<Answer> corAns = adao.getAllCorrectAnswerOfQuestion(id);
        request.setAttribute("corAns",corAns);
        ArrayList<Answer> incorAns = adao.getAllIncorrectAnswerOfQuestion(id);
        request.setAttribute("incorAns",incorAns);
        List<Answer> Ans = adao.getAllAnswerOfQuestion2(id);
        request.setAttribute("Ans", Ans);
        
        TypeQuestionDAO tqdao = new TypeQuestionDAO();
        ArrayList<Type_Question> typelist = tqdao.getALlType();
        request.setAttribute("types", typelist);
        
        request.getRequestDispatcher("expert/questiondetail.jsp").forward(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
