/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.AnswerDAO;
import dal.LessonDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import dal.QuizResultDAO;
import dal.SubjectDAO;
import dal.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.TreeMap;
import model.Answer;
import model.Question;
import model.Quiz;
import model.Result;
import model.Subject;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "QuizReviewDetailsServlet", urlPatterns = {"/reviewdetails"})
public class QuizReviewDetailsServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizReviewDetailsServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizReviewDetailsServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_Result_raw = request.getParameter("id_Result");
        int id_Result;
        try {
            id_Result = Integer.parseInt(id_Result_raw);
        } catch (NumberFormatException e) {
            id_Result = 1;
        }

        QuizResultDAO qrd = new QuizResultDAO();
        QuestionDAO qed = new QuestionDAO();
        QuizDAO qid = new QuizDAO();
        AnswerDAO ad = new AnswerDAO();
        SubjectDAO sd = new SubjectDAO();
        LessonDAO ld = new LessonDAO();

        Result r = qrd.getResultByID(id_Result);
        Quiz q = qid.selectQuizByID(r.getId_Quiz());
        Subject subjname = sd.getSubjectByID(ld.getLessonByIdLesson(q.getId_Lession()).getId_Subject());
        List<Question> listQues = qed.getAllQuestionsOfQuiz(r.getId_Quiz());

        int[] ncorrect_a = new int[listQues.size()];
        float[] quesgrade = new float[ncorrect_a.length];
        String[] dores = r.getDoResult().split(",");
        String[] coans = r.getCorrectAnwser().split(",");
        for (int i = 0; i < ncorrect_a.length; i++) {
            ncorrect_a[i] = ad.getNumOfCorrectAnswerOfQuestion(listQues.get(i).getId_Question());
            quesgrade[i] = Utils.gradeAQuestion(dores[i], coans[i]);
        }

        TreeMap<Question, List<Answer>> mapQues = qid.getMapQuestionAnswerOfQuiz(r.getId_Quiz());

        request.setAttribute("subjname", subjname);
        request.setAttribute("quiz", q);
        request.setAttribute("result", r);
        request.setAttribute("grade", qrd.gradeAResult(r));
        request.setAttribute("listQues", listQues);
        request.setAttribute("ncorrect", ncorrect_a);
        request.setAttribute("quesgrade", quesgrade);
        request.setAttribute("mapQues", mapQues);

        request.getRequestDispatcher("quizrvdetails.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
