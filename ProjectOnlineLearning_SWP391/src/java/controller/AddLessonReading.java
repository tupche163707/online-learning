/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import dal.LessonDAO;
import dal.SubjectDAO;
import dal.TypeLessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author PCT
 */
@WebServlet(name="AddLessonReading", urlPatterns={"/addlessonreading"})
public class AddLessonReading extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
             TypeLessonDAO dao1 = new TypeLessonDAO();
            SubjectDAO dao2 = new SubjectDAO();
            request.setAttribute("type", dao1.getAll());
            request.setAttribute("subject", dao2.getAllSubject());
            request.setAttribute("xxx", "SubjectManagment");
            request.getRequestDispatcher("addlessonbyreading.jsp").forward(request, response);
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        try {
            String nameLesson = request.getParameter("namelessson");
            String content = request.getParameter("content");
            String videoLink = request.getParameter("video");
           boolean status = request.getParameter("status").equals("1");
            String subjectId = request.getParameter("id_subject");
            String typeId = request.getParameter("typeid");
            String createDate = request.getParameter("create_Date");
            String updateDate = request.getParameter("update_Date");
             System.out.println("Name Lesson: " + nameLesson);
            System.out.println("Content: " + content);
            System.out.println("Video Link: " + videoLink);
            System.out.println("Status: " + status);
            System.out.println("Subject ID: " + subjectId);
            System.out.println("Type ID: " + typeId);
            System.out.println("Create Date: " + createDate);
            System.out.println("Update Date: " + updateDate);
            
            LessonDAO dao = new LessonDAO();
            dao.insertLesson(nameLesson, content, videoLink, status, subjectId, typeId, createDate, updateDate);
             String message = "Add Successful";
        request.getSession().setAttribute("message", message);
            response.sendRedirect("subjectlesson?id_Subject=" + subjectId);
        } catch (SQLException ex) {
            Logger.getLogger(AddLessonWatch.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
