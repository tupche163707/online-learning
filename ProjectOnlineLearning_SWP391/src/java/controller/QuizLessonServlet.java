/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dal.LessonDAO;
import dal.QuizDAO;
import dal.QuizResultDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Quiz;
import model.User;

/**
 *
 * @author Naviank
 */
@WebServlet(name = "QuizLessonServlet", urlPatterns = {"/quizlesson"})
public class QuizLessonServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizLessonServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizLessonServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_lesson_raw = request.getParameter("id_lesson");
        User u = (User) request.getSession().getAttribute("user");
        QuizDAO qid = new QuizDAO();
        LessonDAO ld = new LessonDAO();
        SubjectDAO sd = new SubjectDAO();
        QuizResultDAO qrd = new QuizResultDAO();

        int id_lesson = Integer.parseInt(id_lesson_raw);
        int id_subject = ld.getLessonByIdLesson(id_lesson).getId_Subject();

        List<Quiz> listQuiz = qid.selectQuizOfLesson(id_lesson);
        int[] arr = new int[listQuiz.size()];
        float[] grade = new float[listQuiz.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = qrd.countAttemptTimeOfQuizOfUser(listQuiz.get(i).getId_Quiz(), u.getId_User());
            grade[i] = qrd.getBestResultOfQuizOfUser(listQuiz.get(i).getId_Quiz(), u.getId_User());
        }

        request.setAttribute("subject", sd.getSubjectByID(id_subject));
        request.setAttribute("lesson", ld.getLessonByIdLesson(id_lesson));
        request.setAttribute("quizlist", listQuiz);
        request.setAttribute("timeatt", arr);
        request.setAttribute("arrgrade", grade);
        request.setAttribute("quizlevel", qid.getAllQuizLevelName(listQuiz));
        request.getRequestDispatcher("quizlesson.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
