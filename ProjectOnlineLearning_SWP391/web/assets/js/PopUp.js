/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */


var totalDuration = 0;

function getVideoDuration(videoIndex) {
    var video = document.getElementById('videoID' + videoIndex);
    var duration = video.duration;
    var minutes = Math.floor(duration / 60);
    var seconds = Math.round(duration % 60);

    if (seconds < 10) {
        seconds = '0' + seconds;
    }

    var formattedTime = minutes + 'min ' + seconds + "sec";
    document.getElementById('videoDuration' + videoIndex).innerHTML = formattedTime;
    totalDuration += duration;
    var hours = Math.floor(totalDuration / 3600);
    var minutes1 = Math.floor((totalDuration % 3600) / 60);
    var seconds1 = Math.floor(totalDuration % 60);
    var formattedTime1 = hours + "hr " + minutes1 + "min " + seconds1 + "sec";
    document.getElementById('dura').innerHTML = formattedTime1;
}




function closeWindow() {
    window.close();
}
function closeWindowAndPreventSubmit(event) {
    event.preventDefault();
    window.close();
}

function goToLesson1(idsubject, idlesson, formId) {
    var form = document.getElementById(formId);
    form.submit();
    if (form.submit()) {
        alert("Form submitted successfully!");
    } else {
        alert("Form Not Submitted!");
    }
    var url = "lessonview?idsubject=" + idsubject + "&&idlesson=" + idlesson;
    window.location.href = url;
}

function showRectangle() {
    var rectangle = document.getElementById("rectangle");
    rectangle.style.display = "block"; // Hiển thị hình chữ nhật
}

function hideRectangle() {
    var rectangle = document.getElementById("rectangle");
    rectangle.style.display = "none"; // Ẩn hình chữ nhật
}
function openPopupWindow(url) {
// Kích thước và cấu hình của cửa sổ popup
    var width = 1000;
    var height = 600;
    var left = (window.innerWidth - width) / 2;
    var top = (window.innerHeight - height) / 2;
    var options = 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top;
    // Mở cửa sổ popup
    window.open(url, 'popupWindow', options);
}

function openPopup(idCourse, idPackage) {
    // Tạo URL popup
    var popupUrl = "courseregister?idcourse=" + encodeURIComponent(idCourse) + "&packageprice=" + encodeURIComponent(idPackage);
    var width = 1000;
    var height = 600;
    var left = (window.innerWidth - width) / 2;
    var top = (window.innerHeight - height) / 2;
    var option = 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top;
    // Mở popup
    window.open(popupUrl, "popupWindow", option);
}

function openPopUp(url, width, height) {
    var left = (window.innerWidth - width) / 2;
    var top = (window.innerHeight - height) / 2;   
    let params = 'scrollbars=no,resizable=no,status=no,location=no,toolbar=no,menubar=no,width=' + width + ',height=' + height + ',left=' + left + ',top=' + top;
//    alert(height + ", " + width);
    open(url, 'test', params);
}

function closePopupAndRedirect(url) {
    window.opener.location.href = url;
    window.close();
}

function openPopup1(url) {
    // Tạo URL popup
    var popupUrl = url;
    var width = 1000;
    var height = 600;
    var left = (window.innerWidth - width) / 2;
    var top = (window.innerHeight - height) / 2;
    var option = 'width=' + width + ',height=' + height + ',left=' + left + ',top=' + top;
    // Mở popup
    window.open(popupUrl, "popupWindow", option);
}