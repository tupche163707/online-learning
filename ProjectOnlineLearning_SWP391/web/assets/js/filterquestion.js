/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

function filterDataBySubject() {
                        var input = document.getElementById("subject").value;
                        var objects = document.getElementsByClassName("objq");

                        for (var i = 0; i < objects.length; i++) {
                            var objectData = JSON.parse(objects[i].getAttribute("data-json"));

                            if (objectData.subject_Name.toLowerCase().includes(input.toLowerCase())) {
                                objects[i].style.display = "";
                            } else {
                                objects[i].style.display = "none";
                            }
                        }
                    }
function filterDataByCategory() {
                        var input = document.getElementById("category").value;
                        var objects = document.getElementsByClassName("objq");

                        for (var i = 0; i < objects.length; i++) {
                            var objectData = JSON.parse(objects[i].getAttribute("data-json"));

                            if (objectData.subject_Category_Name.toLowerCase().includes(input.toLowerCase())) {
                                objects[i].style.display = "";
                            } else {
                                objects[i].style.display = "none";
                            }
                        }
                    }
function filterDataByLesson() {
                        var input = document.getElementById("lesson").value;
                        var objects = document.getElementsByClassName("objq");

                        for (var i = 0; i < objects.length; i++) {
                            var objectData = JSON.parse(objects[i].getAttribute("data-json"));

                            if (objectData.lesson_Name.toLowerCase().includes(input.toLowerCase())) {
                                objects[i].style.display = "";
                            } else {
                                objects[i].style.display = "none";
                            }
                        }
                    }
function filterDataByLevel() {
                        var input = document.getElementById("level").value;
                        var objects = document.getElementsByClassName("objq");

                        for (var i = 0; i < objects.length; i++) {
                            var objectData = JSON.parse(objects[i].getAttribute("data-json"));

                            if (objectData.level_Name.toLowerCase().includes(input.toLowerCase())) {
                                objects[i].style.display = "";
                            } else {
                                objects[i].style.display = "none";
                            }
                        }
                    }
function filterDataByStatus() {
                        var input = document.getElementById("status").value;
                        var objects = document.getElementsByClassName("objq");

                        for (var i = 0; i < objects.length; i++) {
                            var objectData = JSON.parse(objects[i].getAttribute("data-json"));

                            if (objectData.status.toLowerCase().includes(input.toLowerCase())) {
                                objects[i].style.display = "";
                            } else {
                                objects[i].style.display = "none";
                            }
                        }
                    }
function  alertQuest() {
    var result = confirm("Do you want to continue?");
      if (result) {
        // Người dùng chọn "Có"
        alert("Delete successful!");
        return true;
      } else {
        // Người dùng chọn "Không"
        return false;
    }
    
}
