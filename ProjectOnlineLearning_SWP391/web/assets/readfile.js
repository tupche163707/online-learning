/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */

/* global mammoth */

function checkfile() {
    var fileInput = document.getElementById("fileInput");
    var check = false;
    var file = fileInput.files[0];
    var reader = new FileReader();
    var lines=null;
    reader.onload = function (event) {
        var fileContent = event.target.result;

        // Chuyển đổi dữ liệu file .doc thành chuỗi văn bản
        var text = readDocFile(fileContent);

        lines = text.split("\n"); // Chia nội dung thành các dòng

        if (lines.length < 2) {
            alert("This file has no data. Add data to file, then import later!");
            return check;
        } else {
            var result = confirm("Do you want to continue?");
            if (result) {
                check=true;
                alert("Import successful!");
                return true;
            } else {
                check=false;
                return false;
            }
        }
    };
    
    reader.readAsBinaryString(file);
    return check;
}

function readDocFile(fileContent) {
    var byteArray = new Uint8Array(fileContent.length);

    for (var i = 0; i < fileContent.length; i++) {
        byteArray[i] = fileContent.charCodeAt(i) & 0xff;
    }

    // Chuyển đổi mảng byte thành chuỗi văn bản
    var decoder = new TextDecoder("utf-8");
    var text = decoder.decode(byteArray);

    return text;
}