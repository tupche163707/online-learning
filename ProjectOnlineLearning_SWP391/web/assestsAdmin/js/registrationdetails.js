/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */

function showAlert() {
    alert("You don't have permission");
}

function updateStatusPackage(status) {
    const button = document.querySelector('.element-to-take-1');
    const input = document.getElementById('statusInput');
    if (status) {
        button.textContent = 'Paid';
        input.value = true;
    } else if (status === null) {
        button.textContent = 'Waiting';
        input.value = null;
    } else {
        button.textContent = 'Denided';
        input.value = false;
    }
}
function changeCursor() {
    document.getElementById("statusbutton").classList.add("hand-cursor");
}

function addFormOpen() {
    var lessonNumberDiv = document.getElementById("addFormRegistration");
    var editform = document.getElementById("editFormRegistration");
    if (lessonNumberDiv.style.display !== "block") {
        lessonNumberDiv.style.display = "block";
    }
    if (editform.style.display === "block") {
        editform.style.display = "none";
    }
}

function changeText(option, id) {
    var dropdownButton = document.querySelector('.element-to-take-2');
    dropdownButton.innerHTML = option;
    var selectedValueInput = document.getElementById('selectedValue2');
    selectedValueInput.value = id;
    getIdPackagePrice(id);
    event.preventDefault();
}

function hideAddForm() {
    var lessonNumberDiv = document.getElementById("addFormRegistration");
    var editform = document.getElementById("editFormRegistration");
    if (lessonNumberDiv.style.display !== "none") {
        lessonNumberDiv.style.display = "none";
    }
    if (editform.style.display !== "block") {
        editform.style.display = "block";
    }
}

function searchByName(param) {
    var txtSearch = param.value;
    $.ajax({
        url: "/projectswp391/searchcoursenameajaxinregistratinodetails",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data) {
            var row = document.getElementById("content");
            row.innerHTML = data;
        },
        error: function (xhr) {

        }
    });
}

function getIdPackagePrice(param) {
    var txtSearch = param;
    alert(txtSearch);
    $.ajax({
        url: "/projectswp391/changepackagepricedurationajax",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data) {
            var row = document.getElementById("pacakgePriceIdDuration");
            row.innerHTML = data;
        },
        error: function (xhr) {

        }
    });
}

function changeDuration(option, id) {
    var dropdownButton = document.querySelector('.element-to-take-8');
    dropdownButton.innerHTML = option;
    var selectedValueInput = document.getElementById('selectedValue5');
    selectedValueInput.value = id;
    event.preventDefault();
}
