<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <script src="assestsAdmin/js/subjectdetails.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <script src="assestsAdmin/js/alertSucess.js"></script>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">

        <style>
            .active{
                color: orange;
            }
            .notificationmess{
                width: 100%;
                text-align: center;
                height: 500px;
                background-color: rgba(213, 217, 221,0.4);
                margin: 20px 0px;
                align-items: center;
                font-size: 60px;
                font-weight: bold;
                opacity: 0.4;
                padding-top: 20%;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">

        <!-- header start -->
        <header class="ttr-header">
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="index.html" class="ttr-logo">
                            <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                            <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->

                    <!-- header left menu end -->
                </div>
                <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">

                        <c:if test="${sessionScope.user!=null}">

                            <li> 
                                <a href="#" class="ttr-material-button ttr-submenu-toggle"
                                   ><span class="ttr-user-avatar"
                                       ><img
                                            alt=""
                                            src="${sessionScope.user.avatar}"
                                            width="32"
                                            height="32" /></span
                                    ></a>
                                <div class="ttr-header-submenu">
                                    <ul>
                                        <li><a onclick="openPopup1('profileexpert')">My profile</a></li>
                                        <li><a onclick="openPopup1('profileexpertchangepass')">Change Password</a></li>
                                        <li><a href="logout">Logout</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="#">${user.full_Name}</a></li>
                            <li><a href="logout">Logout</a></li>

                        </c:if> 
                    </ul>
                    <!-- header right menu end -->
                </div>
                <!--header search panel start -->
                <div class="ttr-search-bar">
                    <form class="ttr-search-form">
                        <div class="ttr-search-input-wrapper">
                            <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                            <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                        </div>
                        <span class="ttr-search-close ttr-search-toggle">
                            <i class="ti-close"></i>
                        </span>
                    </form>
                </div>
                <!--header search panel end -->
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                    <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                            <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                            <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                    </div> -->
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <c:set var="currentpage" value="${requestScope.xxx}"/>
                    <ul>

                        <li>
                            <a href="expertsubjectlistservlet" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="subjectdimension" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="packageprice?pplistid=1" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                            </a>
                        </li>
                        <li>
                            <a href="quizzeslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Quizzes Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="listquestions" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Questions Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="profileexpert" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                              
                                <li>
                                    <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                 <li>
                                    <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>

                        <li class="ttr-seperate"></li>
                    </ul>
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <h4 class="breadcrumb-title">Subject</h4>
                    <ul class="db-breadcrumb-list">
                        <a href = "expertsubjectlistservlet"><li>Subject Management</li></a>
                    </ul>
                </div>	
                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <c:if test="${sessionScope.val == 1}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Edit Subject Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 2}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Add Package Price Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 3}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Update Price Package Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 4}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Delete Price Package Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 5}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Add Subject Dimension Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 6}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Update Subject Dimension Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <c:if test="${sessionScope.val == 7}">
                                <div id="success-message" class="alert alert-success">
                                    <strong>Successful</strong>, Delete Subject Dimension Completed!
                                </div>
                                <c:set var="val" value="0" scope="session" />
                            </c:if>
                            <script src="assestsAdmin/js/alertSucess.js"></script>
                            <div class="search-bar">
                                <h4>Subject Management</h4>
                            </div>
                            <div class="search-bar">
                                <form action = "search" method="get">
                                    <input type="text" name ="name" value="${requestScope.key}" placeholder="Subject Name...">
                                    <button type="submit"><i>Search</i></button>
                                </form>
                                <form action="category" method="get" style="width: 500px">
                                    <select class="col-6" name="categoryID" onchange="this.form.submit()">
                                        <option value="" disabled selected hidden>choose category</option>
                                        <c:forEach var="i" items="${requestScope.categoryList}">
                                            <option value="${i.id_Catergory_Subject}" ${i.id_Catergory_Subject == param.categoryID ? 'selected' : ''}>${i.name_Catergory_Subject}</option>
                                        </c:forEach>
                                    </select>
                                </form>
                                <form action="status" method="get" style="width: 500px">
                                    <select class="col-4" name="st" onchange="this.form.submit()">
                                        <option value="" disabled hidden>Status</option>
                                        <option value="1" ${param.st == '1' ? 'selected' : ''}>Active</option>
                                        <option value="0" ${param.st == '0' ? 'selected' : ''}>Deactive</option>
                                    </select>
                                </form>
                            </div>


                            <div class="widget-inner" id="subjectlist">

                                <c:if test="${requestScope.list1.size() == 0}">
                                    <div class="notificationmess">
                                        No Subject yet
                                    </div>
                                </c:if>
                                <c:forEach items="${requestScope.list1}" var="i">
                                    <c:if test="${sessionScope.user.id_User == i.id_User || sessionScope.user.id_role == 1}">
                                        <div class="card-courses-list admin-courses">
                                            <div class="card-courses-media">
                                                <img src="${i.image_subject}" alt=""/>
                                            </div>
                                            <div class="card-courses-full-dec">
                                                <div class="card-courses-title">
                                                    <h4>${i.subject_Name}</h4>
                                                </div>
                                                <div class="card-courses-list-bx">
                                                    <ul class="card-courses-view">
                                                        <li class="card-courses-user">
                                                            <div class="card-courses-user-info">
                                                                <h5>${i.courseName}</h5>
                                                            </div>
                                                        </li>
                                                        <li class="card-courses-categories">
                                                            <h5>${i.name_Catergory_Subject}</h5>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="card-courses-list-bx">
                                                    <ul class="card-courses-view">
                                                        <li class="card-courses-stats">
                                                            <a href="#" class="btn button-sm green radius-xl">${i.status eq true? 'active':'deactive'}</a>
                                                        </li>
                                                    </ul>
                                                </div>

                                                <div class="row card-courses-dec">
                                                    <div class="col-md-12">
                                                        <h6 class="m-b10">Course Description</h6>
                                                        <p>${i.description}</p>	
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="subjectlesson?id_Subject=${i.id_Subject}" class="btn green radius-xl outline">Lesson</a>
                                                        <a href="subjectdetails?subjectid=${i.id_Subject}" class="btn red outline radius-xl ">Edit</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <c:set var="page" value="${requestScope.page}"/>
                                <div class="pagination col-3">
                                    <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                        <a class="${(page == i)?"active":""}" href="expertsubjectlistservlet?page=${i}">${i}</a>   
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>
            </div>
        </main>
        <div class="ttr-overlay"></div>

        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assets/vendors/scroll/scrollbar.min.js'></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/vendors/chart/chart.min.js"></script>
        <script src="assets/js/admin.js"></script>
    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>
