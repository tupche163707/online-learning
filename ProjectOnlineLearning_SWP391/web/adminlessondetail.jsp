<%-- 
   Document   : PageCourse
   Created on : May 17, 2023, 3:51:55 PM
   Author     : PCT
--%>
<%    
String id_Subject = (String) request.getAttribute("id_Subject");

   
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slider.css">
        <style>
            nav ul {
                list-style: none;
                margin: 0;
                padding: 0;
            }
              .active{
                color: orange;
            }

            nav li {
                display: inline-block;
                margin-right: 1em;
            }

            nav a {
                color: #fff;
                text-decoration: none;
            }

            main {
                margin: 1em;
                display: flex;
                flex-wrap: wrap;
            }

            article {
                flex-basis: 60%;
            }

            aside {
                flex-basis: 40%;
                margin-left: 1em;
            }

            h2, h3 {
                margin-top: 0;
                display: flex ;
                justify-content: center;
            }

            video {
                display: block;
                margin-bottom: 1em;
            }

            ul {
                list-style: none;
                margin: 0;
                padding: 0;
            }

            li {
                margin-bottom: 0.5em;
            }

            button {
                background-color: #ffc107;
                color: #fff;
                padding: 0.5em 1em;
                border: none;
                border-radius: 5px;
                cursor: pointer;

            }

            button:hover {
                background-color: #fff;
                color: #1c1c1c;
                border: 1px solid #1c1c1c;
            }
            .message1 {
                position: fixed;
                color: white;
                background-color: #f5f5f5;
                height: 100vh;
                width: 100vw;
                z-index: 999;
                padding: 10px;
                background: rgba(0, 0, 0, 0.6);
                border: 1px solid #ddd;
                border-radius: 4px;
                box-shadow: 0px 2px 8px rgba(0, 0, 0, 0.15);
                font-size: 16px;
                text-align: center;
            }
            .message1.show {
                display: block;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <c:if test="${not empty sessionScope.message}">

            <div id="message1" class="message1" style="">${sessionScope.message}</div>
            <% session.removeAttribute("message"); %>
        </c:if>
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                        <a href="../index.html" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                    </li>
                </ul>
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="userprofile">
                                <img 
                                    src="${sessionScope.user.avatar}" 
                                    style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                    alt="${sessionScope.user.full_Name}"/>
                            </a>
                        </li>
                        <li><a href="userprofile">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>

                    </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
               <c:set var="currentpage" value="${requestScope.xxx}"/>
                    <ul>
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Course Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="adminsubject" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="subjectdimensionadmin" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="packagepriceadmin?pplistid=1" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                            </a>
                        </li>
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">User Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="quizzeslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Quizzes Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="listquestions" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Questions Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="profileexpert" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>

                                <li>
                                    <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Subject Detail</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="adminsubjectlesson?id_Subject=${lesson.getId_Subject()}"><i class="fa fa-home"></i>Subject Lesson</a></li>
                    <a href = "adminlessondetail?idlesson=${lesson.getId_Lesson()}"><li>Subject Detail</li></a>
                </ul>
            </div>
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <h4>Lesson detail</h4>
                        </div>
                        <div class="widget-inner">
                            <form class="edit-profile m-b30" action="adminlessondetail" method="post" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h3>1. Basic info</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <input   type="hidden" name="" value="${requestScope.lesson.getId_Lesson()}">
                                        <label class="col-form-label">Lesson Name</label>
                                        <div>
                                            <input class="form-control readonly" name="" type="text" value="${requestScope.lesson.name_Lesson}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Name Subject</label>
                                        <div>
                                            <input class="form-control readonly" name="" type="text" value="${requestScope.lesson.name_Subject}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Status</label>
                                        <div>
                                            <input class="form-control readonly" name="" type="text" value="${requestScope.lesson.status eq true ? 'Active' : 'Deactive'}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Name Type</label>
                                        <div>
                                            <input class="form-control readonly" name="" type="text" value="${requestScope.lesson.name_TypeLesson}" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Create Date</label>
                                        <div>
                                            <input class="form-control readonly" type="date" name="" value="${requestScope.lesson.create_Date}" readonly>

                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Update Date</label>
                                        <div>
                                            <input class="form-control readonly" type="date" name="" value="${requestScope.lesson.update_Date}"  readonly>

                                        </div>
                                    </div>
                                    <div class="seperator"></div>

                                    <div class="col-12 m-t20">
                                        <div class="ml-auto m-b5">
                                            <h3>2. Content Lesson</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Content Lesson </label>
                                        <div>
                                            <textarea class="" name="password" readonly  style=" background-color: transparent;">${requestScope.lesson.content_Lesson}</textarea>
                                        </div>
                                        <c:if test="${requestScope.lesson.getId_Type() eq 3}">
                                            <div class="form-group col-12">
                                                <h3>3. Video Link</h3>
                                                <label class="col-form-label">Link to Video: ${requestScope.lesson.video_Link}</label>
                                                <div>
                                                    <iframe width="560" height="315" src="${requestScope.lesson.video_Link}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </c:if>
                                        <c:if test="${requestScope.lesson.id_Type eq 1}">
                                            <div class="form-group col-12">
                                                <h3>3. Quiz</h3>
                                                <button class="btn btn-primary" type="button" onclick="">Quiz Detail</button>
                                            </div>
                                        </c:if>

                                        <c:if test="${requestScope.lesson.id_Type eq 2}">
                                            <div class="form-group col-12">
                                                <h3>3. Reading</h3>
                                                <label class="col-form-label">Reading Lesson </label>
                                                <div>
                                                    <textarea class="" name="video" readonly  style=" background-color: transparent;">${requestScope.lesson.video_Link}</textarea>
                                                </div>
                                            </div>
                                        </c:if> 
                                        <div class="col-12">
                                            <button id="edit-lesson-btn"><a href="adminupdatelesson?idlesson=${lesson.getId_Lesson()}">Update Lesson</a></button>
                                        </div>      

                                    </div>   
                                </div>


                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Your Profile Views Chart END--
    </main>
    <div class="ttr-overlay"></div>
    
    <!-- External JavaScripts -->
    <script src="assestsAdmin/js/jquery.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
    <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
    <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
    <script src="assestsAdmin/vendors/masonry/filter.js"></script>
    <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
    <script src="assestsAdmin/js/functions.js"></script>
    <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
    <script src="assestsAdmin/js/admin.js"></script>
    <script>

        var messageBox = document.getElementById("message1");
        messageBox.style.display = "none"; // Ẩn thông báo ban đầu

        function showMessage() {
            // Hiển thị thông báo
            messageBox.style.display = "block";
            messageBox.style.top = (window.innerHeight - messageBox.offsetHeight) / 2 + "px"; // Căn giữa theo chiều dọc
            messageBox.style.left = (window.innerWidth - messageBox.offsetWidth) / 2 + "px"; // Căn giữa theo chiều ngang

            // Tự động tắt thông báo sau 3 giây
            setTimeout(function () {
                messageBox.style.display = "none";
            }, 1000);
        }

        // Gọi hàm hiển thị thông báo
        showMessage();
    </script>
</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>
