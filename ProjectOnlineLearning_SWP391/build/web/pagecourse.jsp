<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

        <!-- REVOLUTION SLIDER CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/layers.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/settings.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/revolution/css/navigation.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <!-- REVOLUTION SLIDER END -->	
        <style>

            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                margin-left: 37% ;

            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
        </style>
    </head>
    <body>

        <!-- Header Top ==== -->
        <jsp:include page="header.jsp"/>
        <!-- Header Top END ==== -->
        <!-- Header Top END ==== -->
        <!-- Content -->
        <div class="page-content bg-white">
            <!-- inner page banner -->
            <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner3.jpg);">
                <div class="container">
                    <div class="page-banner-entry">
                        <h1 class="text-white">Courses List</h1>
                    </div>
                </div>
            </div>

            <!-- Breadcrumb row -->
            <div class="breadcrumb-row">
                <div class="container">
                    <ul class="list-inline">
                        <li><a href="home">Home</a></li>
                        <li><a href="courseservlet">Courses List</a></li>
                    </ul>
                </div>
            </div>
            <div class="content-block">
                <!-- About Us -->
                <div class="section-area section-sp1">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-3 col-md-4 col-sm-12 m-b30">
                                <div class="col-md-12" style="background-color: rgb(252, 251, 251); border-radius: 10px;box-shadow: 1px 2px 3px rgb(72, 68, 68); height: 200px;">
                                    <form action="searchcourse" method="get">
                                        <label for="searchName" style="display: block; background-color: rgb(93, 4, 176); margin: 10px; padding: 10px;color: white; border-radius: 5px;">Search Name Course: </label>
                                        <input type="hidden" placeholder="Enter name Course ?" name="sortID" value="${sortID}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <input type="hidden" placeholder="Enter name Course ?" name="categoryID" value="${categoryID}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <input type="text" placeholder="Enter name Course ?" name="searchName" value="${searchName}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <input type="submit" value="Search" style="display: block; background-color: #f7b205; padding: 4px; border-radius: 5px; border: 1px solid black; margin: auto;">
                                    </form>
                                </div>

                                <form action="searchcourse" method="get">
                                    <div class="col-12" style="margin: 50px 0px 150px 0px;background-color: rgb(252, 251, 251); border-radius: 10px; box-shadow: 1px 2px 3px rgb(72, 68, 68); height: fit-content" onclick="show()">
                                        <input type="hidden" placeholder="Enter name Course ?" name="searchName" value="${searchName}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <input type="hidden" placeholder="Enter name Course ?" name="categoryID" value="${categoryID}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <h4 style="background-color: rgb(93, 4, 176); padding: 10px; color: white; border-radius: 10px;text-align: center">Sort Course</h4>
                                        <select name="sortID" style="width: 100%;border-radius: 8px;margin-bottom: 4%" onchange="this.form.submit()" >
                                            <option value="" disabled selected hidden>choose 1 option to sort</option>
                                            <option disabled  hidden></option>
                                            <option value ="1" ${param.sortID == '1' ? 'selected' : ''}>Sort Name Courses ASC</option>
                                            <option value ="2" ${param.sortID == '2' ? 'selected' : ''}>Sort Name Courses DESC</option>
                                            <option value ="3" ${param.sortID == '3' ? 'selected' : ''}>Sort Courses by oldest  creation date</option>
                                            <option value ="4" ${param.sortID == '4' ? 'selected' : ''}>Sort Courses by sale price asc</option>
                                            <option value ="5" ${param.sortID == '5' ? 'selected' : ''}>Sort Courses by sale price desc</option>
                                            <option value ="" >Clear</option>
                                        </select>
                                    </div>
                                </form>

                                <form action="searchcourse" method="get">
                                    <div class="col-12" style="margin: 50px 0px 150px 0px;background-color: rgb(252, 251, 251); border-radius: 10px; box-shadow: 1px 2px 3px rgb(72, 68, 68); height: fit-content">
                                        <h4 style="background-color: rgb(93, 4, 176); padding: 10px; color: white; border-radius: 10px;text-align: center">Category</h4>
                                        <input type="hidden" placeholder="Enter name Course ?" name="searchName" value="${searchName}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <input type="hidden" placeholder="Enter name Course ?" name="sortID" value="${sortID}" style="margin: 10px; padding: 10px; border-radius: 5px;">
                                        <select name="categoryID" style="width: 100%;border-radius: 8px;margin-bottom: 4%" onchange="this.form.submit()">
                                            <option value="" disabled selected hidden>choose category</option>
                                            <c:forEach items="${categoryList}" var="category">
                                                <option value="${category.id_Catergory_Course}" ${categoryID eq category.id_Catergory_Course ? 'selected' : ''}>${category.name_Catergory_Course}</option>
                                            </c:forEach>
                                            <option value ="">Clear</option>
                                        </select>
                                    </div>
                                </form>

                            </div>

                            <div class="col-lg-9 col-md-8 col-sm-12" id="detail">
                                <div class="row">
                                    <c:forEach var="i" items="${requestScope.list}">
                                        <div class="col-md-6 col-lg-4 col-sm-6 m-b30">
                                            <div class="cours-bx" >
                                                <div style="height: 150px" class="action-box">
                                                    <img width="100%" height="100%" src="${i.image}" alt="">
                                                    <a href="coursedetail?idcourse=${i.id_Course}" class="btn">Read More</a>
                                                </div>
                                                <div style="height:  160px;" class="info-bx text-center">
                                                    <h5>${i.course_Name}</a></h5>
                                                    <a>Date created: ${i.create_Date}</a>
                                                </div>
                                                <div class="cours-more-info">
                                                    <div class="review">
                                                        <button onclick="openPopupWindow('courseregister?idcourse=${i.id_Course}&packageprice=${i.id_PackagePrice}')" class="btn">Register</button>
                                                    </div>
                                                    <div class="price"> 
                                                        <del>${i.list_Price}</del>
                                                        <h5>${i.sale_Price}</h5>
                                                    </div>
                                                </div>
                                            </div>                                                                                      
                                        </div>
                                    </c:forEach>
                                </div>
                                <c:set var="page" value="${requestScope.page}"/>
                                <div style="justify-content: center;" class="pagination col-3">
                                    <c:choose>
                                        <c:when test="${not empty searchName}">
                                            <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                                <a class="${(page == i) ? 'active' : ''}" href="searchcourse?page=${i}&searchName=${searchName}&sortID=${sortID}&categoryID=${categoryID}">${i}</a>
                                            </c:forEach>
                                        </c:when>
                                        <c:when test="${not empty sortID}">
                                            <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                                <a class="${(page == i) ? 'active' : ''}" href="searchcourse?page=${i}&sortID=${sortID}&searchName=${searchName}&categoryID=${categoryID}">${i}</a>
                                            </c:forEach>
                                        </c:when>
                                        <c:when test="${not empty categoryID}">
                                            <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                                <a class="${(page == i) ? 'active' : ''}" href="searchcourse?page=${i}&categoryID=${categoryID}&searchName=${searchName}&sortID=${sortID}">${i}</a>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>
                                            <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                                <a class="${(page == i) ? 'active' : ''}" href="courseservlet?page=${i}">${i}</a>
                                            </c:forEach>
                                        </c:otherwise>
                                    </c:choose>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Footer ==== -->
            <!-- Footer ==== -->
            <jsp:include page="footer.jsp"/>

            <!-- Footer END ==== -->
            <!-- External JavaScripts -->
            <script src="assets/js/jquery.min.js"></script>
            <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
            <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
            <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
            <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
            <script src="assets/vendors/counter/waypoints-min.js"></script>
            <script src="assets/vendors/counter/counterup.min.js"></script>
            <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
            <script src="assets/vendors/masonry/masonry.js"></script>
            <script src="assets/vendors/masonry/filter.js"></script>
            <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
            <script src="assets/js/functions.js"></script>
            <script src="assets/js/contact.js"></script>
            <!-- <script src='assets/vendors/switcher/switcher.js'></script> -->
            <!-- Revolution JavaScripts Files -->
            <script src="assets/vendors/revolution/js/jquery.themepunch.tools.min.js"></script>
            <script src="assets/vendors/revolution/js/jquery.themepunch.revolution.min.js"></script>
            <!-- Slider revolution 5.0 Extensions  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.actions.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.migration.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/revolution.extension.video.min.js"></script>
            <script src="assets/vendors/revolution/js/extensions/genjs.js"></script>        
    </body>

</html>
