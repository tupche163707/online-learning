<%-- 
    Document   : addcourse
    Created on : Jun 13, 2023, 7:19:56 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from educhamp.themetrades.com/demo/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
    <head>
        <!-- META ============================================= -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta
            property="og:description"
            content="EduChamp : Education HTML Template"
            />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no" />

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="assets/images/favicon.png"
            />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.min.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css" />
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/vendors/calendar/fullcalendar.css"
            />

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css" />

        <!-- SHORTCODES ============================================= -->
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/css/shortcodes/shortcodes.css"
            />

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css" />
        <link
            class="skin"
            rel="stylesheet"
            type="text/css"
            href="assets/css/color/color-1.css"
            />

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="assets/js/PopUp.js"></script>
        <style>
            a:hover{
                text-decoration:none;
                color: orange;
            }

            .messagesuccess {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: green;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
            .messagefail {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: orange;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
            #active{
                color: orange;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">
        <div id="myDiv" class="${requestScope.classmes}" style="display: none;">
            ${requestScope.mesaddcousrse}
        </div>
        <!-- header start -->
        
        <!-- header end -->
        <!-- Left sidebar menu start -->
        
        <!-- Left sidebar menu end -->

        <!--Main container start -->

        <main class="ttr-wrapper">
            
                <div class="container-fluid">
                    <div class="db-breadcrumb">
                        <h4 class="breadcrumb-title">Marketing</h4>
                        <ul class="db-breadcrumb-list">
                            <li><a href="profilemarketing"><i class="ti-book"></i>Profile of ${sessionScope.user.full_Name}</a></li>
                    </ul>
                </div>	
                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <div class="wc-title">
                                <h4>Change Password</h4>
                            </div>
                            <div class="widget-inner">
                                <form action="profilemarketingchagepass" method="post" class="row">
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Enter Old Password</label>
                                        <div>
                                            <input required=""  name="oldpass" class="form-control" type="password" value="${requestScope.oldpass}">
                                            <p style="color: red">${requestScope.mespass}</p>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-form-label">New Password</label>
                                        <div>
                                            <input required=""  name="newpass" class="form-control" type="password" value="${requestScope.newpass}">
                                            <p style="color: red">${requestScope.mespass1}</p>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Confirm Password</label>
                                        <div>
                                            <input required=""  name="confirmpass" class="form-control" type="password" value="${requestScope.confirmpass}">
                                            <p style="color: red">${requestScope.mespass1}</p>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <input  class="btn-secondry add-item m-r5" type="submit" value="Change Password"/>
                                        <button class="btn" onclick="closeWindowAndPreventSubmit(event)"> Cancel Change Password</button>
                                    </div>


                                </form>
                                
                            </div>
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>
            </div>
            <!-- Card -->







        </main>

        <div class="ttr-overlay"></div>

        <script>
            function deletefunction(idpost, namepost) {
                if (window.confirm("Are you sure you want to delete this " + namepost + " ?")) {
                    window.location = "deletepost?idpost=" + idpost;
                }
            }
            function previewImage(inputId, imageId) {
                var input = document.getElementById(inputId);
                var previewImage = document.getElementById(imageId);

                if (input.files && input.files[0]) {
                    var file = input.files[0];
                    var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
                    var allowedExtensions = ['.jpg', '.png'];

                    if (allowedExtensions.includes(fileExtension)) {
                        var reader = new FileReader();

                        reader.onload = function (e) {
                            previewImage.src = e.target.result;
                        };

                        reader.readAsDataURL(file);
                    } else {
                        // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
                        console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
                    }
                }
            }
            window.onload = function () {
                var id = 1;

                if (id == 1) {
                    var myDiv = document.getElementById("myDiv");

                    setTimeout(function () {
                        myDiv.style.display = "block";

                        setTimeout(function () {
                            myDiv.style.display = "none";
                        }, 3000);
                    }, 0);
                }
            };
        </script>


        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/vendors/scroll/scrollbar.min.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/vendors/chart/chart.min.js"></script>
        <script src="assets/js/admin.js"></script>
        <script src="assets/vendors/calendar/moment.min.js"></script>
        <script src="assets/vendors/calendar/fullcalendar.js"></script>

    </body>
</html>

