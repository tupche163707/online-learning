<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/chosen/style.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/chosen/prism.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/chosen/chosen.css">
        <!--<link rel="stylesheet" type="text/css" href="assets/vendors/chosen/chosen.min.css">-->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/quizzeslist.css">
        <style>
            .active{
                color: black;
            }
            .actived{
                color:orange;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar" onload="onloadquiz()">
        <!-- header start -->
        <header class="ttr-header">
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="home" class="ttr-logo">
                            <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                            <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="home" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                        </li>
                    </ul>
                    <!-- header left menu end -->
                </div>
                <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">

                        <c:if test="${sessionScope.user!=null}">

                            <li> 
                                <a href="userprofile">
                                    <img 
                                        src="${sessionScope.user.avatar}" 
                                        style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                        alt="${sessionScope.user.full_Name}"/>
                                </a>
                            </li>
                            <li><a href="userprofile">${user.full_Name}</a></li>
                            <li><a href="logout">Logout</a></li>
                            </c:if> 
                    </ul>
                    <!-- header right menu end -->
                </div>
                <!--header search panel start -->
                <div class="ttr-search-bar">
                    <form class="ttr-search-form">
                        <div class="ttr-search-input-wrapper">
                            <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                            <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                        </div>
                        <span class="ttr-search-close ttr-search-toggle">
                            <i class="ti-close"></i>
                        </span>
                    </form>
                </div>
                <!--header search panel end -->
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <c:set value="${sessionScope.user}" var="su"/>
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <c:set var="currentpage" value="${requestScope.xxx}"/>
                    <c:set var="currentpageDetails" value="${requestScope.xxxDetails}"/>
                    <ul>
                        <li>
                            <a href="#" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-home"></i></span>
                                <span class="ttr-label">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="expertsubjectlistservlet" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'actived':""}">Subject Management</span>
                            </a>
                        </li>

                        <c:if test="${requestScope.suId != null}">
                            <li style="margin-left: 60px">
                                <a href="subjectdetails?subjectid=${requestScope.suId}" class="ttr-material-button">
                                    <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                    <span class="ttr-label ${currentpageDetails eq "SubjectDetails"? 'actived':""}">Subject Detail</span>
                                </a>
                            </li>
                        </c:if>
                        <li style="margin-left: 60px">
                            <a href="subjectdimension" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </li>
                        <c:if test="${requestScope.suId != null}">
                            <li style="margin-left: 60px">
                                <a href="packageprice?subjectid=${requestScope.suId}&&pplistid=1" class="ttr-material-button">
                                    <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                    <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                                </a>
                            </li>
                        </c:if>
                        <c:if test="${requestScope.suId == null}">
                            <li style="margin-left: 60px">
                                <a href="packageprice?pplistid=1" class="ttr-material-button">
                                    <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                    <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                                </a>
                            </li>
                        </c:if>
                        <li>
                            <a href="quizzeslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label actived">Quizzes Management</span>
                            </a>
                        </li>                            
                        <li>
                            <a href="listquestions" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Questions Management</span>
                            </a>
                        </li>

                        <li class="ttr-seperate"></li>
                    </ul>
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div id="test"></div>
            <audio id="myAudio">
                <!--<source src="assestsAdmin/audio/magicaudio.ogg" type="audio/ogg">-->
                <source src="assestsAdmin/audio/magicaudio.mp3" type="audio/mpeg">
                <!--Your browser does not support the audio element.-->
            </audio>  
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <!-- Nav tabs -->
                    <!-- Tab panes -->
                    <h4 class="breadcrumb-title">Quizzes List</h4>
                    <ul class="db-breadcrumb-list">
                        <li><a href="#"><i class="fa fa-home"></i>Home</a></li>
                        <li><a href="#">Quizzes Management</a></li>
                        <li>Quizzes List</li>
                    </ul>
                </div>

                <div class="row">
                    <!-- Your Profile Views Chart -->
                    <div class="col-lg-12 m-b30">
                        <div class="widget-box">
                            <div class="wc-title">
                                <ul class="nav nav-tabs" role="tablist">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#menu2">Quizzes List</a></li>
                                        <!--<li><a href="packageprice?pplistid=1">Package Price</a></li>-->     
                                    </ul>
                                </ul>
                            </div>
                            <!--<div class="widget-inner">-->
                            <div class="tab-content">
                                <div id="home1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu1" class="container tab-pane fade"><br>
                                </div>
                                <div id="menu2" class="container tab-pane active"><br>
                                    <div id="notification">Delete successfully!</div>
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h2>Quizzes List</h2>
                                        </div>
                                    </div>
                                    <div class="form-group col-8" style="margin-bottom: 50px; margin-top: 30px">
                                        <div class="filter">
                                            <h5>Filter result:</h5>
                                            <span for="quizlevel">Quiz level:</span
                                            <!--.-->
                                            <!--.-->
                                            <select name="quizlevel" 
                                                    class="chosen-select"
                                                    onchange="setQuizLevel(this.value)">
                                                <option value="0">--Choose quiz level--</option>
                                                <c:forEach items="${requestScope.levelQuiz}" var="lvq">
                                                    <option ${requestScope.quizlevelid == lvq.id_Level ? "selected" : ""}
                                                        value="${lvq.id_Level}">
                                                        ${lvq.name_Level}
                                                    </option>
                                                </c:forEach>
                                            </select>
                                            <br/>
                                            <span for="subject">Subject:</span>
                                            <select name="subject" 
                                                    onchange="setSubject(this.value)" 
                                                    class="chosen-select"
                                                    tabindex="-1">
                                                <option value="0">--Choose subject--</option>
                                                <c:forEach items="${requestScope.listSubject}" var="s">
                                                    <option ${requestScope.subjectid == s.id_Subject ? "selected" : ""}
                                                        value="${s.id_Subject}">
                                                        ${s.subject_Name}
                                                    </option>
                                                </c:forEach>
                                            </select>                                            
                                            <!--.-->
                            
                                            <!--.-->                                            
                                            <br/>
                                            <input type="checkbox" 
                                                   ${requestScope.paging == "true" ? "checked" : ""}
                                                   value="true" 
                                                   onchange="setpagingCheckbox(this.checked)" 
                                                   id="paging-cbx"> Paginate list
                                            <button onclick="submitForm(1)">Do filter</button>
                                            <button onclick="location.href = 'quizzeslist'">Clear filter</button>
                                            <br/>                                            
                                        </div>
                                        <div class="sort">
                                            <h6>Sort result:</h6>
                                            <button onclick="sortTable1(1)">Sort by name</button>
                                            <button onclick="sortTable1(4)">Sort by number question</button>
                                            <button id="reversebtn">Reverse order</button>
                                        </div>
                                        <div class="searchform">
                                            <h5>Search quiz name:</h5>
                                            <input class="form-control" 
                                                   oninput="searchQuizName(this, ${requestScope.curunpet})" 
                                                   placeholder="Enter quiz name">
                                            <i class="fa fa-search" 
                                               style="position: relative; left: 520px; bottom: 30px" 
                                               aria-hidden="true">
                                            </i>
                                        </div>                                                   
                                    </div>
                                    <!--Unnecessary form start-->                                    
                                    <form id="addForm" 
                                          style="display: none" 
                                          action="subjectdimension" 
                                          method="post">
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <div class="container">
                                                    <div style="text-align: left;" class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-wrap">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>No.</th>
                                                                            <th>Name</th>
                                                                            <th>Lesson</th>
                                                                            <th>Subject</th>
                                                                            <th>Level</th>
                                                                            <th>Num of questions</th>
                                                                            <th>Duration</th>
                                                                            <th>Type</th>
                                                                            <th>Pass rate</th>
                                                                            <th>Pass/take rate</th>
                                                                            <th>Action</th>
                                                                        </tr>
                                                                    </thead>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!--Unnecessary form end-->                                    

                                    <!--<form class="edit-profile m-b30" action="" method="">-->
                                    <!--                                        <a href="#" 
                                                                               style="position: relative; left: 820px; width: 150px" 
                                                                               class="btn gradient green" 
                                                                               onclick="openAdd()">Add New</a>-->
                                    <center>
                                        <h3>Quizzes list</h3>
                                        <h4 class="message">${requestScope.message}</h4>
                                    </center>
                                    <div class="row">
                                        <div class="form-group col-12">
                                            <div class="container">
                                                <div style="text-align: left;" class="row">
                                                    <div class="col-md-12">
                                                        <div class="table-wrap">
                                                            <table class="table table-striped" id="myTable">
                                                                <thead>
                                                                    <tr>
                                                                        <th id="">No.</th>
                                                                        <th>Name</th>
                                                                        <!--<th>Lesson</th>-->
                                                                        <th>Subject</th>
                                                                        <th>Level</th>
                                                                        <th>Num of ques</th>
                                                                        <th>Duration</th>
                                                                        <th>Type</th>
                                                                        <th>Pass rate</th>
                                                                        <th>Pass/take rate</th>
                                                                        <th>Action</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody id="qlist-f">
                                                                    <c:set var="listquiz" value="${requestScope.quizzeslist}"/>
                                                                    <c:set var="listlname" value="${requestScope.listlname}"/>
                                                                    <c:set var="listsname" value="${requestScope.listsname}"/>
                                                                    <c:set var="listlevelname" value="${requestScope.listlevelname}"/>
                                                                    <c:set var="listptratio" value="${requestScope.listptratio}"/>
                                                                    <c:set var="i" value="0"/>
                                                                    <c:forEach items="${listquiz}" var="quiz">
                                                                        <tr class="revertable">
                                                                            <td>${i + 1}</td>
                                                                            <td>${quiz.name_Quiz}</td>
                                                                            <!--<td>${listlname.get(i)}</td>-->
                                                                            <td>${listsname.get(i)}</td>
                                                                            <td>${listlevelname.get(i)}</td>
                                                                            <td>${quiz.number_Question}</td>
                                                                            <td>${quiz.time_Limit}(s)</td>
                                                                            <td>Multiple Choice</td>
                                                                            <td>${quiz.passpercent}%</td>
                                                                            <td>
                                                                                <fmt:formatNumber maxFractionDigits="2" 
                                                                                                  minFractionDigits="2"
                                                                                                  value="${listptratio.get(i) * 100}"/>%
                                                                            </td>
                                                                            <td>
                                                                                <button onclick="location.href = 'quizdetails'" 
                                                                                        class="btn">
                                                                                    Details
                                                                                </button>    
                                                                                <button class="btn gradient brown" 
                                                                                        onclick="deleteQuiz(${quiz.id_Quiz}, ${requestScope.curunpet})">
                                                                                    Delete
                                                                                </button>
                                                                            </td>
                                                                        </tr>
                                                                        <c:set var="i" value="${i + 1}"/>
                                                                    </c:forEach>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row pagination-bx rounded-sm gray clearfix">
                                            <c:set var="numofpage" value="${requestScope.numpage}"/>
                                            <c:set var="curunpet" value="${requestScope.curunpet}"/>
                                            <form action="quizzeslist#myTable" method="get" id="pagination-f">
                                                <input type="hidden" value="${i}" name="page" id="pginput">
                                                <input type="hidden" value="${requestScope.quizlevelid}" name="quizlevelid" id="qlvinput">
                                                <input type="hidden" value="${requestScope.subjectid}" name="subjectid" id="subinput">
                                                <input type="hidden" value="${requestScope.paging}" name="paging" id="paging">
                                                <ul class="pagination align-content-end">
                                                    <c:if  test="${curunpet > 1}">
                                                        <li class="previous">
                                                            <button onclick="submitForm(${curunpet - 1})">
                                                                <i class="ti-arrow-left"></i> Prev
                                                            </button>
                                                        </li>
                                                    </c:if>
                                                    <c:forEach begin="1" end="${numofpage}" var="i">
                                                        <li class="${i == curunpet?"active":""}">
                                                            <button onclick="submitForm(${i})">${i}</button>
                                                        </li>
                                                    </c:forEach>
                                                    <c:if test="${curunpet < numofpage}">
                                                        <li class="next">
                                                            <button onclick="submitForm(${curunpet + 1})">Next 
                                                                <i class="ti-arrow-right"></i>
                                                            </button>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </form>
                                        </div>
                                    </div>
                                    <!--</form>-->
                                </div>
                            </div>
                            <!--</div>-->
                        </div>
                    </div>
                    <!-- Your Profile Views Chart END-->
                </div>

            </div>
        </main>

        <div class="ttr-overlay"></div>

        <!-- External JavaScripts -->
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/chosen/chosen.jquery.js"></script>
        <script src="assets/vendors/chosen/prism.js"></script>
        <script src="assets/vendors/chosen/init.js"></script>
        <script src="assets/vendors/chosen/jquery-3.2.1.min.js"></script>
        <!--<script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>-->
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <!--<script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>-->
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>
        <script src="assestsAdmin/js/quizzeslist.js"></script>
    </body>

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>