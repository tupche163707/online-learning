<%-- 
    Document   : updatesubject
    Created on : Jun 7, 2023, 4:46:33 PM
    Author     : PCT
--%>
<%    
String id_Subject = (String) request.getAttribute("id_Subject");

   
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/add-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:09:05 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assets/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <link rel="stylesheet" type="text/css" href="assets/css/slider.css">
        <style>
            .active{
                color: orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <ul class="ttr-header-navigation">
                    <li>
                        <a href="../index.html" class="ttr-material-button ttr-submenu-toggle">HOME</a>
                    </li>
                </ul>
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="userprofile">
                                <img 
                                    src="${sessionScope.user.avatar}" 
                                    style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                    alt="${sessionScope.user.full_Name}"/>
                            </a>
                        </li>
                        <li><a href="userprofile">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>

                    </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                    <ul>
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Course Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="adminsubject" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="subjectdimensionadmin" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </li>
                        <li style="margin-left: 60px">
                            <a href="packagepriceadmin?pplistid=1" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                            </a>
                        </li>
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">User Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="quizzeslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Quizzes Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="listquestions" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Questions Management</span>
                            </a>
                        </li>
                        <li>
                            <a href="profileexpert" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>

                                <li>
                                    <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
                <!-- sidebar menu end -->
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Edit Lesson</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="adminsubjectlesson?id_Subject=${lesson.getId_Subject()}"><i class="fa fa-home"></i>Subject Lesson</a></li>
                    <li>Edit Lesson</li>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <h4>Edit User</h4>
                        </div>
                        <div class="widget-inner">
                            <form class="edit-profile m-b30" action="adminaddlessonwatch" method="post" >
                                <div class="row">
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h3>1. Type Lesson</h3>
                                        </div>
                                    </div>

                                    <div class="form-group col-6">
                                        <label class="col-form-label">Type Name Lesson</label>
                                          <div>
                                            <select class="form-control" name="typeid">                                              
                                                    <option value="3" ${3 eq requestScope.lesson.getId_Type() ? 'selected' : ''}>Watch Video</option>                                             
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="ml-auto">
                                            <h3>2. Basic info</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <input   type="hidden" name="id_lesson" value="">
                                        <label class="col-form-label">Name Lesson</label>
                                        <div>
                                            <input class="form-control" name="namelessson" type="text" value="">
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Status</label>
                                        <div>
                                            <select class="form-control" name="status">
                                                <option value="" disabled selected hidden>choose status</option>
                                                <option value="1" ${requestScope.lesson.status eq true ? 'selected' : ''}>active</option>
                                                <option value="0" ${requestScope.lesson.status eq false ? 'selected' : ''}>deactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">
                                        <label class="col-form-label"> Subject Name</label>
                                        <div>
                                            <select class="form-control" name="id_subject">
                                                <c:forEach var="i" items="${requestScope.subject}">
                                                    <option value="${i.id_Subject}" ${i.id_Subject eq requestScope.lesson.getId_Subject() ? 'selected' : ''}>${i.subject_Name}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>                                   
                                    <div class="form-group col-6">
                                        <label class="col-form-label">Create Date</label>
                                        <div>
                                            <input id="create-date-field" class="form-control" type="date" name="create_Date" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="form-group col-6">  
                                         <label class="col-form-label">Update Date</label>
                                        <div>
                                            <input id="update-date-field" class="form-control" type="date" name="update_Date" value="" readonly>
                                        </div>
                                    </div>
                                    <div class="seperator"></div>

                                    <div class="col-12 m-t20">
                                        <div class="ml-auto m-b5">
                                            <h3>3. Content Lesson</h3>
                                        </div>
                                    </div>
                                    <div class="form-group col-12">
                                        <label class="col-form-label">Content Lesson </label>
                                        <div>
                                            <textarea class="" name="content"  style=" background-color: transparent;"></textarea>
                                        </div>
                                       
                                            <div class="form-group col-12">
                                                <h3>4. Video Link</h3>
                                                <label class="col-form-label">Link to Video</label>
                                                <div>
                                                    <input class="form-control" type="text" name="video" value="">
                                                </div>
                                            </div>
<!--                                                <label class="col-form-label">Video </label>
                                                <div>
                                                    <input type="hidden" value="${requestScope.lesson.video_Link}" name="old_video" >
                                                    <video id="previewVideo" style="width: 320px" controls>
                                                        <source src="${requestScope.lesson.video_Link}" type="video/mp4">
                                                        Your browser does not support the video tag.
                                                    </video>
                                                    <input class="form-control" name="new_video" type="file" id="videoInput" onchange="previewVideo('videoInput', 'previewVideo')">
                                                </div>-->
                                            <!--</div>-->
                                                                                  

                                        <div class="col-12">
                                            <button type="submit" class="btn-secondry m-r5" onclick="return confirm('Are you sure you want to update?')"><i class="fa fa-fw fa-plus-circle"></i>Add lesson</button>
                                        
                                        </div>      
                                    </div>     
                                        </div> 
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Your Profile Views Chart END-->
    </div>
</div>
</main>
<div class="ttr-overlay"></div>

<!-- External JavaScripts -->
<script src="assestsAdmin/js/jquery.min.js"></script>
<script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
<script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
<script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
<script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assestsAdmin/vendors/masonry/masonry.js"></script>
<script src="assestsAdmin/vendors/masonry/filter.js"></script>
<script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
<script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
<script src="assestsAdmin/js/functions.js"></script>
<script src="assestsAdmin/vendors/chart/chart.min.js"></script>
<script src="assestsAdmin/js/admin.js"></script>
<script>
                                                function previewVideo(inputId, videoId) {
                                                    var input = document.getElementById(inputId);
                                                    var previewVideo = document.getElementById(videoId);

                                                    if (input.files && input.files[0]) {
                                                        var file = input.files[0];
                                                        var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
                                                        var allowedExtensions = ['.mp4', '.avi', '.mov', '.wmv'];

                                                        if (allowedExtensions.includes(fileExtension)) {
                                                            var reader = new FileReader();

                                                            reader.onload = function (e) {
                                                                previewVideo.setAttribute('src', e.target.result);
                                                            };

                                                            reader.readAsDataURL(file);
                                                        } else {
                                                            // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
                                                            console.log('Chỉ cho phép chọn tệp tin có đuôi .mp4, .avi, .mov hoặc .wmv.');
                                                        }
                                                    }
                                                }
</script>
<script>
    var today = new Date().toISOString().substr(0, 10);
    document.getElementById("create-date-field").value = today;
</script>
<script>
    var today = new Date().toISOString().substr(0, 10);
    document.getElementById("update-date-field").value = today;
</script>

</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/add-listing.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:09:05 GMT -->
</html>


