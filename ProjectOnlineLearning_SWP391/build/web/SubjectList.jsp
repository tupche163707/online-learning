<%-- 
    Document   : course_detail
    Created on : May 17, 2023, 1:50:51 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

    <head>
         <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <style>
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                justify-content: center;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            .active{
                color: orange;
            }
            .phone-icon {
                position: fixed;
                bottom: 20px;
                right: 20px;
                background-color: #ff9800;
                color: #fff;
                width: 200px;
                height: 50px;
                border-radius: 50%;
                text-align: center;
                line-height: 50px;
                font-size: 24px;
                z-index: 9999;
                text-decoration: none;
                transition: background-color 0.3s ease;
            }

            .phone-icon:hover {
                background-color: #f57c00;
            }
            .active{
                color: #fff;
            }
        </style>
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>

            <!-- Header Top ==== -->
           <jsp:include page="header.jsp"/>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white" >
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px" >
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top: 50px">Course Detail</h1>
                        </div>
                    </div>
                </div>
                <c:set var="cour" value="${requestScope.cour}"/>
                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <li><a href="home">Home</a></li>
                            <li><a href="courseservlet">Course List</a></li>
                            <li><a href="coursedetail?idcourse=${cour.id_Course}">Course Details</a></li>
                        </ul>
                    </div>
                </div>
                <!-- Breadcrumb row END -->
                <!-- inner page banner END -->
                <div class="content-block" style="margin-left: 150px;">
                    <!-- About Us -->
                    <div class="section-area section-sp2">
                        <div class="container">


                            <div class="row">
                                <div class="col-md-12 heading-bx text-center">
                                    <h2 class="title-head text-uppercase m-b0">${cour.course_Name} <br/> </h2>
                                    <h2 class="title-head text-uppercase m-b0">Create Date : ${ requestScope.date}<br/> </h2>
                                    <p class="description">${cour.description_Course}</p>
                                </div>
                            </div>



                            <div class="pricingtable-row row">

                                <div class="col-md-3 row">

                                    <div class="col-md-12" style="background-color: rgb(252, 251, 251); border-radius: 10px;box-shadow: 1px 2px 3px rgb(72, 68, 68); height: 200px;">
                                        <form action="subjectlist" method="post">
                                            <input type="hidden" value="${requestScope.cour.id_Course}" name="idcourse"/>
                                            <label for="namesubject" style="display: block; background-color: rgb(93, 4, 176);
                                                   margin: 10px; padding: 10px;color: white; border-radius: 5px;">Search Name Subject In Course: </label>
                                            <input type="text" placeholder="Enter name subject ?" name="subjectname" value="${requestScope.key}"
                                                   
                                                   style="margin: 10px; padding: 10px; border-radius: 5px;">
                                            <input type="submit" value="Search" onclick="this.form.submit()" 
                                            style="display: block; background-color: #f7b205;padding: 4px; border-radius: 5px; border: 1px solid black; margin: 10px;">
                                        </form>
                                    </div>
                                    <div class="col-12" style="margin: 50px 0px 150px 0px;background-color: rgb(252, 251, 251); border-radius: 10px;
                                         box-shadow: 1px 2px 3px rgb(72, 68, 68);  height: fit-content">
                                        <H4 style="background-color: rgb(93, 4, 176); padding: 10px; color: white; border-radius: 10px;text-align: center">Sort Subject Course</H4>
                                        <form action="subjectlist1" method="get">
                                            <input type="hidden" value="${cour.id_Course}" name="idcourse">
                                            <select name="check" onchange="this.form.submit()">
                                                 <option disabled  hidden></option>
                                                <option value="" disabled selected >choose 1 option to sort</option>
                                                
                                                <option ${requestScope.check == 1 ?"selected":""} value="1">Sort Subject From A - Z</option>
                                                <option ${requestScope.check == 2 ?"selected":""} value="2">Sort Subject From Z - A</option>
                                                <option ${requestScope.check == 3 ?"selected":""} value="3">Sort subjects by oldest  creation date</option>
                                                <option ${requestScope.check == 4 ?"selected":""} value="4">Sort subjects by latest  creation date</option>

                                            </select>
                                        </form>
                                    </div>

                                    <div class="col-md-12"  style="background-color: rgb(252, 251, 251); border-radius: 10px;
                                         box-shadow: 1px 2px 3px rgb(72, 68, 68); margin-top: -90px; height: fit-content">
                                        <H4 style="background-color: rgb(93, 4, 176); padding: 10px; color: white; border-radius: 10px;text-align: center">Catergory Subject In Course</H4>
                                        <form action="subjectlist" method="get">
                                            <input type="hidden" value="${cour.id_Course}" name="idcourse">
                                            <option value="" disabled selected hidden>choose 1 option to sort</option>
                                            <select name="check1" onchange="this.form.submit()">

                                                <c:forEach var="ca" items="${requestScope.list2}">
                                                    <option disabled  hidden></option>
                                                    <option ${requestScope.check1 == ca.id_Catergory_Subject ?"selected":""} value="${ca.id_Catergory_Subject}">
                                                        ${ca.name_Catergory_Subject}
                                                    </option>

                                                </c:forEach>

                                            </select>
                                        </form>


                                    </div>

                                </div>

                                <div class="row col-md-9">
                                    <c:if test="${requestScope.list.size() == 0 || requestScope.list == null}">
                                        <div class="col-12" style="background-color: aliceblue; width: 100%; height: 100%;margin-left: 20px;
                                             text-align: center;box-sizing: border-box; padding-top: 30%; font-size: 30px;
                                             color: rgb(28, 28, 26); font-weight: bold;">
                                            There are no suitable courses
                                        </div>
                                    </c:if>
                                    <c:if test="${requestScope.list.size() != 0 }">
                                        <form action="" class="col-12 row">
                                            <c:set var="sub" value="${requestScope.list}"/>
                                            <c:set var="author" value="${requestScope.author}"/>

                                            <c:forEach begin="0" end="${sub.size() - 1}" var="i" >
                                                <div class="col-sm-12 col-md-4 col-lg-4">
                                                    <div class="pricingtable-wrapper">
                                                        <div class="pricingtable-inner">
                                                            <div class="pricingtable-main"> 
                                                                <div class="pricingtable-price"> 
                                                                    <img src="${sub.get(i).image_subject}" alt="EduSourse">
                                                                </div>
                                                                <div class="pricingtable-title">
                                                                    <h2> ${sub.get(i).subject_Name}</h2>
                                                                </div>
                                                            </div>
                                                            <ul class="pricingtable-features">
                                                                <li>Number Lesson : ${sub.get(i).numberLesson}</li>   
                                                                <li>Subject creation date :<br> ${sub.get(i).formatCreateDate()}</li>     
                                                                <li>Author Subject : ${author.get(i).full_Name}</li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </c:forEach>

                                        </form>
                                    </c:if>


                                </div>



                            </div>



                            <div class="row" style="margin-top: 30px;margin-left: -30px">
                                <div class="col-9 m-b30">
                                    <form action="courseregister" method="get">
                                        <div style="display: flex">
                                            <div>
                                                <div style="padding:5px 10px; background-color: rgb(93, 4, 176)
                                                     ;border-radius: 4px;color: white;font-weight: 500;">Course Package Price </div> 
                                                <input type="hidden" name="idcourse" value="${cour.id_Course}"/>
                                                <c:set var="listprice" value="${requestScope.listprice.get(0).list_Price}"></c:set>
                                                <c:set var="saleprice" value="${requestScope.listprice.get(0).sale_Price}"></c:set>
                                                <c:set var="duration" value="${requestScope.listprice.get(0).duration}"></c:set>
                                                <div style="margin-left: 5px">
                                                    ${listprice > saleprice ?  saleprice : listprice} $
                                                    ${listprice > saleprice ? "(Price Sale)":""}
                                                    -   Use Course in ${duration <= 12 ? duration   : "forever"} ${duration <= 12 ? " Month": " "}
                                                </div>
                                                <input type="hidden" name="packageprice" value="${requestScope.listprice.get(0).id_Package}"/>
                                            </div>
                                            <div style="margin-top: 0px;margin-left: 40px">
                                                <a href="#" onclick="openPopupWindow('courseregister?idcourse=${cour.id_Course}')" class="btn button-md">Course Register</a>
                                            </div>
                                        </div>
                                    </form>

                                </div>


                                <c:if test="${requestScope.chekphantrang == 1}">
                                    <c:set var="page" value="${requestScope.page}"/>
                                    <div class="pagination col-3">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i)?"active":""}" href="subjectlist1?page=${i}&idcourse=${requestScope.idcourse}&check=${requestScope.check}">${i}</a>   
                                        </c:forEach>
                                    </div>
                                </c:if>

                                <c:if test="${requestScope.chekphantrang == 2}">
                                    <c:set var="page" value="${requestScope.page}"/>
                                    <div class="pagination col-3">
                                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                                            <a class="${(page == i)?"active":""}" href="subjectlist?page=${i}&idcourse=${requestScope.idcourse}&check1=${requestScope.check1}">${i}</a>   
                                        </c:forEach>
                                    </div>
                                </c:if>

                            </div>

                        </div>
                    </div>
                    <!-- Our Services ==== -->

                </div>
                <!-- contact area END ==== -->

                <!-- Our Status END ==== -->
            </div>
            
            <!-- Content END-->
            <!-- Footer ==== -->
            <jsp:include page="footer.jsp"/>
            <!-- Footer END ==== -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>
        <!-- External JavaScripts -->
        <!--        <script src="assets/js/jquery.min.js"></script>
                <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
                <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
                <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
                <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
                <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
                <script src="assets/vendors/counter/waypoints-min.js"></script>
                <script src="assets/vendors/counter/counterup.min.js"></script>
                <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
                <script src="assets/vendors/masonry/masonry.js"></script>
                <script src="assets/vendors/masonry/filter.js"></script>
                <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
                <script src="assets/js/functions.js"></script>
                <script src="assets/js/contact.js"></script>-->

    </body>

</html>

