<%-- 
    Document   : addcourse
    Created on : Jun 13, 2023, 7:19:56 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from educhamp.themetrades.com/demo/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
    <head>
        <!-- META ============================================= -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta
            property="og:description"
            content="EduChamp : Education HTML Template"
            />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no" />

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="assets/images/favicon.png"
            />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.min.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css" />
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/vendors/calendar/fullcalendar.css"
            />

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css" />

        <!-- SHORTCODES ============================================= -->
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/css/shortcodes/shortcodes.css"
            />

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css" />
        <link
            class="skin"
            rel="stylesheet"
            type="text/css"
            href="assets/css/color/color-1.css"
            />
        <link rel="stylesheet" href="assets/css/dashboardmarketing.css" />
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="assestsAdmin/css/adminlistcourse.css"/>
        <style>
            .wc-item{
                text-align: center
            }
            .wc-item h5{
                font-size: 20px;

            }
            .notificationmess{
                width: 100%;
                text-align: center;
                height: 500px;
                background-color: rgba(213, 217, 221,0.4);
                margin: 20px 0px;
                align-items: center;
                font-size: 60px;
                font-weight: bold;
                opacity: 0.4;
                padding-top: 20%;
            }
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                justify-content: center;
                margin-left: 37% ;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            .messagesuccess {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: green;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
            .messagefail {
                display: none;
                position: fixed;
                top: 5%;
                left: 50%;
                transform: translate(-50%, -50%);
                background-color: orange;
                padding:  20px 50px;
                border: 1px solid #ccc;
                border-radius: 4px;
                text-align: center;
                color: white;
                border: 1px solid orange;
                z-index: 100;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">
        <div id="myDiv" class="${requestScope.classmes}" style="display: none;">
            ${requestScope.mesaddcousrse}
        </div>
        <!-- header start -->
        <header class="ttr-header">
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="index.html" class="ttr-logo">
                            <img
                                class="ttr-logo-mobile"
                                alt=""
                                src="assets/images/logo-mobile.png"
                                width="30"
                                height="30"
                                />
                            <img
                                class="ttr-logo-desktop"
                                alt=""
                                src="assets/images/logo-white.png"
                                width="160"
                                height="27"
                                />
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a
                                href="../index.html"
                                class="ttr-material-button ttr-submenu-toggle"
                                >HOME</a
                            >
                        </li>
                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               >QUICK MENU<i class="fa fa-angle-down"></i
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a href="../courses.html">Dashboard</a></li>
                                    <li><a href="../event.html">Course Management</a></li>
                                    <li><a href="../membership.html">User Management</a></li>
                                    <li><a href="../membership.html">Setting Management</a></li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <!-- header left menu end -->
                </div>
                <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a href="#">My profile</a></li>
                                    <li><a href="#">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li style="background-color: orange">
                            <a href="logout" class="ttr-material-button">Logout</a>
                        </li>
                    </ul>
                    <!-- header right menu end -->
                </div>
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"
                       ><img alt="" src="assets/images/logo.png" width="122" height="27"
                          /></a>
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <ul>
                        <li>
                            <a href="dashboardmarketing" class="ttr-material-button">
                                <span  class="ttr-icon"
                                       ><i class="ti-home"></i
                                    ></span>
                                <span class="ttr-label">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active" class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Management Course</span>
                            </a>
                        </li>
                        <li>
                            <a href="expertsubjectlistservlet" class="ttr-material-button">
                                <span  class="ttr-icon"><i class="ti-book"></i></span>
                                <span   class="ttr-label">Management Subject</span>
                            </a>
                        </li>
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">Management User</span>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">Management Setting</span>
                            </a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a href="#" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
                    <!-- sidebar menu end -->
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <h4 class="breadcrumb-title">Admin</h4>
                    <ul class="db-breadcrumb-list">
                        <li><a href="dashboardmarketing"><i class="fa fa-home"></i>Dashboard</a></li>
                        <li><a href="admincourselist"><i class="fa fa-book"></i>Management Course</a></li>
                        <li> <a href="adminsubjectcourse?idcourse=${requestScope.idcourse}"><i class="fa fa-book"></i>Subject of Course</a></li>
                    </ul>
                </div>
                <!--Card-->  
                <div class="row">
                    <div class="col-lg-12 m-b30">

                        <!--Your Profile Views Chart--> 

                        <div class="widget-box">
                            <c:set var="cour" value="${requestScope.cour}"/>
                            <div class="col-lg-12 m-b30">
                                <div class="search-bar">
                                    <h4>Subject Of Course : ${cour.course_Name}</h4>
                                    <a href="addcourse" style="color: white" class="add-subject">Add new Subject</a>
                                </div>


                                <div class="search-bar">
                                    <div class="search">
                                        <form action = "adminsubjectcourse" method="get">
                                            <input type="hidden" name="idcourse" value="${requestScope.idcourse}"/>
                                            <input type="hidden" name="check" value="1"/>
                                            <input type="text"  name ="nameSubject" value="${requestScope.key}" placeholder="Course Name...">
                                            <button type="submit"><i>Search</i></button>
                                        </form>
                                    </div>

                                    <div class="category">
                                        <div style="padding-left: 20px">Filter Subject By Status</div>
                                        <form action="adminsubjectcourse" method="get">
                                            <input type="hidden" name="check" value="2"/>
                                            <input type="hidden" name="idcourse" value="${requestScope.idcourse}"/>
                                            <select  name="status" onchange="this.form.submit()">
                                                <option value="" disabled selected hidden>Status</option>
                                                <option  ${requestScope.status == 1?"selected":""} value="1">Active</option>
                                                <option ${requestScope.status == 0?"selected":""} value="0">DeActive</option>
                                            </select>
                                        </form>
                                    </div>


                                </div>
                                <div id="content" class="widget-inner">

                                    <c:set var="listsub" value="${requestScope.listsub}"/>
                                    <c:set var="listteacher" value="${requestScope.listexpert}"/>
                                    <c:set var="listcategory" value="${requestScope.listcate}"/>
                                    <c:if test="${listsub.size() == 0}">
                                        <div class="notificationmess">
                                            No Subject Match
                                        </div>
                                    </c:if>
                                    <c:forEach begin="0" end="${listsub.size() -1}" var="i">
                                        <div class="card-courses-list admin-courses">
                                            <div class="card-courses-media">
                                                <img src="${listsub.get(i).image_subject}" alt="educhamp"/>
                                            </div>
                                            <div class="card-courses-full-dec">
                                                <div class="card-courses-title">
                                                    <h4>${listsub.get(i).subject_Name}</h4>
                                                </div>
                                                <div class="card-courses-list-bx">
                                                    <ul class="card-courses-view">
                                                        <li class="card-courses-user">
                                                            <div class="card-courses-user-pic">
                                                                <img src="${listteacher.get(i).avatar}" alt="teacher"/>
                                                            </div>
                                                            <div class="card-courses-user-info">
                                                                <h5>Teacher</h5>
                                                                <h4>${listteacher.get(i).full_Name}</h4>
                                                            </div>
                                                        </li>
                                                        <li class="card-courses-categories">
                                                            <h5>Categories of Subject</h5>
                                                            <h4>${listcategory.get(i).name_Catergory_Subject}</h4>
                                                        </li>
                                                        <li class="card-courses-review">
                                                            <h5>Status Course</h5>

                                                            <h4 href="#" class="btn button-sm green radius-xl">${listsub.get(i).status?"Public":"Unpublic"}</h4>
                                                        </li>
                                                    </ul>
                                                </div>
                                                <div class="row card-courses-dec">
                                                    <div class="col-md-12">
                                                        <h6 class="m-b10">Subject Description</h6>
                                                        <p>${listsub.get(i).description}</p>	
                                                    </div>
                                                    <div class="col-md-12">
                                                        <a href="updatecourse?idcourse=${listcourse.get(i).id_Course}" class="btn green radius-xl outline">Update Subject</a>
                                                        <a  onclick="deletefunction('${listcourse.get(i).id_Course}', '${listcourse.get(i).course_Name}')" class="btn red outline radius-xl ">Delete Subject</a>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                        <!--Your Profile Views Chart END-->
                    </div>
                </div>
                <c:set var="page" value="${requestScope.page}"/>
                <div class="pagination col-3">
                    <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                        <a class="${(page == i)?"active":""}" href="admincourselist?check=${requestScope.check}&expert=${requestScope.expert}&categoryID=${requestScope.categoryID}&status=${requestScope.status}&page=${i}">${i}</a>   
                    </c:forEach>
                </div>

        </main>

        <div class="ttr-overlay"></div>
        <div style="height: 100px;width: 100%;background-color: rgb(93, 4, 176);margin-top: 100px"></div>
        <!-- External JavaScripts -->
        <script type="text/javascript">
            function deletefunction(idcourse, namecourse) {
                if (window.confirm("Are you sure you want to delete this " + namecourse + " ?")) {
                    window.location = "deletecourseadmin?idcourse=" + idcourse;
                }
            }


            window.onload = function () {
                var id = 1;

                if (id == 1) {
                    var myDiv = document.getElementById("myDiv");

                    setTimeout(function () {
                        myDiv.style.display = "block";

                        setTimeout(function () {
                            myDiv.style.display = "none";
                        }, 3000);
                    }, 0);
                }
            }
        </script>
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assestsAdmin/vendors/scroll/scrollbar.min.js"></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>
        <script src="assestsAdmin/vendors/calendar/moment.min.js"></script>
        <script src="assestsAdmin/vendors/calendar/fullcalendar.js"></script>


    </body>
</html>
