<%-- 
    Document   : explanation
    Created on : Jun 4, 2023, 1:58:32 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Peek at answer pop-up</title>
    </head>
    <body>
        <c:if test="${requestScope.peekmess == null}">
            <c:set var="cur_question" value="${requestScope.cur_question}"/>
            <c:set var="correct_ans" value="${requestScope.correct_ans}"/>
            <h1 class="explain-title"> Explanation!</h1>
            <!--<p class="explain-answer">The correct answer is ${correct_ans}</p>-->
            <p class="explain-content">${cur_question.description}</p>
        </c:if>
        <c:if test="${requestScope.peekmess != null}">
            <h1 class="explain-title"> Alert!</h1>
            <!--<p class="explain-answer">The correct answer is ${correct_ans}</p>-->
            <p class="explain-content">${requestScope.peekmess}</p>
        </c:if>
            
    </body>
</html>
