<%-- 
    Document   : mycoursedetail
    Created on : Jun 3, 2023, 11:14:48 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" href="assets/css/mycourse.css">
        <style>
            #message{
                background-color: rgba(197, 197, 191, 0.2);
                color: black;
                width: 100%;
                box-sizing: border-box;
                padding:  100px 100px 100px 200px;
                text-align: center;
                align-items: center;
                margin-top: 0px;
                font-size: 50px;
                font-weight: 1000;
                height: 90%;

            }
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                justify-content: center;
                margin-left: 48% ;
            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            .active{
                color: orange;
            }
        </style>
    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>

            <!-- Header Top ==== -->
            <jsp:include page="header.jsp"/>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white" >
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px" >
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top:50px;">My Course Details</h1>
                        </div>
                    </div>
                </div>

                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <li><a href="home">Home</a></li>
                            <li><a href="mycourse">My course</a></li>
                            <li><a href="mycoursedetail?idcourse=${requestScope.idcourse}">My Course Detail</a></li>
                        </ul>
                    </div>
                </div>

                <!-- Breadcrumb row END -->
                <!-- inner page banner END -->
                <div class="content-block" >
                    <!-- About Us -->
                    <div class="section-area section-sp2">
                        <div class="container">
                            <c:set var="course" value="${requestScope.course}"></c:set>
                                <div class="row">
                                    <div class="col-md-12 heading-bx text-center">
                                        <h2 class="title-head text-uppercase m-b0">${course.course_Name} <br/> </h2>
                                    <h2 class="title-head text-uppercase m-b0">Create Date : ${course.formatDateValidTo()}<br/> </h2>
                                    <p class="description">${course.description_Course}</p>
                                </div>
                            </div>
                        </div>
                    </div>    

                </div>

            </div>
            <div class="pricingtable-row">

                <div class="function" >

                    <div  id="search1" >
                        <form action="mycoursedetail" method="get">
                            <input type="hidden" value="${course.id_Course}"  name="idcourse"/>
                            <label  class="lableSearch"  for="namesubject" >Search Name Subject In Course Or Teacher : </label>
                            <input class="searchkey" type="text" placeholder="What course are you looking for?" name="subjectname" value="${requestScope.key}" >
                            <input class="submitSearch" type="submit" value="Search">
                        </form>
                    </div>



                </div>
                <div class="listproduct">
                    <c:if test="${requestScope.listsubject.size() == 0}">
                        <div id="message">
                            There are no suitable Subjects!
                        </div>
                    </c:if>
                    <c:if test="${requestScope.listsubject.size() != 0}">
                        <c:set var="listsubject" value="${requestScope.listsubject}"/>
                        <c:set var="listauthor" value="${requestScope.listauthor}"/>
                        <c:set var="process" value="${requestScope.process}"/>
                        <c:forEach begin="0" end="${listsubject.size() - 1}" var="i">
                            <div class="item">
                                <div class="pricingtable-wrapper">
                                    <div class="pricingtable-inner">
                                        <div class="pricingtable-main row"> 
                                            <div class="pricingtable-price"> 
                                                <img src= ${listsubject.get(i).image_subject} alt="EduSourse">
                                            </div>
                                            <div class="pricingtable-title col-12">
                                                <h2>${listsubject.get(i).subject_Name}</h2>
                                            </div>

                                            <div class="container1">
                                                <div class="createDate">
                                                    <h5>Teacher : ${listauthor.get(i).full_Name} </h5>
                                                </div>
                                                <div class="Process">
                                                    <h5>Process : </h5><div>${process.get(i)}%</div>
                                                </div>

                                            </div>  
                                            <div class="optionShow">
                                                <button class="Learn"><a href="lessonview?idsubject=${listsubject.get(i).id_Subject}">Go To Learn</a></button>
                                                
                                            </div>                                        
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </c:forEach>

                    </c:if>


                </div>
            </div>    
        </div>

        <p class="phone-icon">
            <a class="support" href="http://127.0.0.1:5500/EduChamp-Education-HTML-Template-Admin-Dashboard/mycourse.html"><i class="fa fa-user"> </i></a>
        </p>
        <c:set var="page" value="${requestScope.page}"/>
        <div class="pagination col-3">
            <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                <a class="${(page == i)?"active":""}" href="mycoursedetail?subjectname=${requestScope.key}&idcourse=${course.id_Course}">${i}</a>   
            </c:forEach>
        </div>


        <div class="overlay1" id="overlayDiv">
            <div class="overlay-content">
                <h2>Course Description Java Web Programming</h2>
                <p id="contentdescrip">
                    Function Description : In My Course, the registered courses will 
                    be displayed, displaying information about the registered course 
                    such as course photo, course name, course creation date, registration
                    date, course end date. When selecting a course, the subjects will be
                    displayed, including the following information: Subject name, subject
                    image, learning progress
                </p>
                <button id="closeBtn" onclick="hiddenDescription()">X</button>
            </div>
        </div> 
        <!-- Content END-->

        <!-- Footer ==== -->
        <!-- <jsp:include page="footer.jsp"/> -->
        <!-- Footer END ==== -->
        <button class="back-to-top fa fa-chevron-up" ></button>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assets/vendors/counter/waypoints-min.js"></script>
    <script src="assets/vendors/counter/counterup.min.js"></script>
    <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assets/vendors/masonry/masonry.js"></script>
    <script src="assets/vendors/masonry/filter.js"></script>
    <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
    <script src="assets/js/functions.js"></script>
    <script src="assets/js/contact.js"></script>
    <script src='assets/vendors/switcher/switcher.js'></script>
    <script src="assets/js/mycourse.js"></script>
    <script>
                // Hàm xử lý sự kiện khi người dùng click vào nút "Hiện miêu tả chi tiết"


    </script>
</body>

</html>


