<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/subjectlist.css">
        <script src="assestsAdmin/js/subjectdetails.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <link href="assetsAdmin/select2/select2.min.css" rel="stylesheet" />
        <script src="assetsAdmin/select2/select2.min.js"></script>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/style.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/SubjectAdmin.css">
        <link rel="stylesheet" type="text/css" href="assestsAdmin/css/page.css">
        <style>
            .active{
                color: black;
            }
            .actived{
                color:orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">
    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-open-icon"></i>
                <i class="ti-menu ttr-close-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profileexpert')">My profile</a></li>
                                    <li><a onclick="openPopup1('profileexpertchangepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li><a href="#">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>
                        </c:if>
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <c:set value="${sessionScope.user}" var="su"/>
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <c:set var="currentpageDetails" value="${requestScope.xxxDetails}"/>
                <ul>
                    <li>
                        <a href="admincourselist" class="ttr-material-button">
                            <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                            <span  id="active" class="ttr-label">Course Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="adminsubject" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label actived">Subject Management</span>
                        </a>
                    </li>
                    <c:if test="${requestScope.suId != null}">
                        <li style="margin-left: 60px">
                            <a href="subjectdetailsadmin?subjectid=${requestScope.suId}" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "SubjectDetails"? 'actived':""}">Subject Detail</span>
                            </a>
                        </li>
                    </c:if>
                    <li style="margin-left: 60px">
                        <c:if test="${requestScope.suId != null}">
                            <a href="subjectdimensionadmin?subjectid=${requestScope.suId}" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </c:if>
                        <c:if test="${requestScope.suId == null}">
                            <a href="subjectdimensionadmin" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label ${currentpageDetails eq "subjectdimension"? 'actived':""}">Subject Dimension</span>
                            </a>
                        </c:if>
                    </li>
                    <li style="margin-left: 60px">
                        <a href="packagepriceadmin?pplistid=1" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                            <span class="ttr-label ${currentpageDetails eq "packageprice"? 'actived':""}">Price Package</span>
                        </a>
                    </li>
                    <li>
                        <a href="useradmin" class="ttr-material-button">
                            <span class="ttr-icon"
                                  ><i class="ti-layout-list-post"></i
                                ></span>
                            <span class="ttr-label">User Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="quizzeslist" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Quizzes Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Questions Management</span>
                        </a>
                    </li>
                    <li>
                        <a href="profileexpert" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-user"></i></span>
                            <span class="ttr-label">My Profile</span>
                            <span class="ttr-arrow-icon"
                                  ><i class="fa fa-angle-down"></i
                                ></span>
                        </a>
                        <ul>

                            <li>
                                <a onclick="openPopup1('profileexpert')" class="ttr-material-button"
                                   ><span class="ttr-label">User Profile</span></a
                                >
                            </li>
                            <li>
                                <a onclick="openPopup1('profileexpertchangepass')" class="ttr-material-button"
                                   ><span class="ttr-label">Change Password</span></a
                                >
                            </li>
                            <li>
                                <a href="logout" class="ttr-material-button"
                                   ><span class="ttr-label">Logout</span></a
                                >
                            </li>
                        </ul>
                    </li>
                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <!-- Nav tabs -->


                <!-- Tab panes -->

                <h4 class="breadcrumb-title">Subject Management</h4>
                <ul class="db-breadcrumb-list">
                    <li><a href="expertsubjectlistservlet">Subject Detail</a></li>
                    <li>Package Price</li>
                </ul>
            </div>

            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">
                        <div class="wc-title">
                            <ul class="nav nav-tabs" role="tablist">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a data-toggle="tab" href="#menu1">Package Price</a></li>
                                        <c:if test="${requestScope.suId != null}">
                                        <li><a href="subjectdetailsadmin?subjectid=${requestScope.suId}">Subject Detail</a></li>
                                        </c:if>
                                        <c:if test="${requestScope.suId != null}">
                                        <li><a href="subjectdimensionadmin?subjectid=${requestScope.suId}">Dimension</a></li>
                                        </c:if>
                                        <c:if test="${requestScope.suId == null}">
                                        <li><a href="subjectdimensionadmin">Dimension</a></li>
                                        </c:if>
                                </ul>
                            </ul>
                        </div>
                        <div class="widget-inner">
                            <div class="tab-content">
                                <div id="menu1" class="container tab-pane active"><br>
                                    <form class="edit-profile m-b30" action="packagepriceadmin" method="post">
                                        <div class="row">
                                            <div class="col-12">
                                                <div class="ml-auto">
                                                    <h2>Package Price</h2>
                                                </div>
                                            </div>
                                            <div class="form-group col-12" id="addfunction" style="display: none">
                                                <h6>Add a package price</h6>
                                                <table>
                                                    <tr>
                                                        <th id="thinpricepackageNumber">Number</th>
                                                        <th id="thinpricepackagePackage">Package</th>
                                                        <th id="thinpricepackageDuration">Duration</th>
                                                        <th id="thinpricepackageDuration">List Price</th>
                                                        <th id="thinpricepackageDuration">Sale Price</th>
                                                        <th id="thinpricepackageDuration">Status</th>
                                                        <th id="thinpricepackagePackage">Action</th>        
                                                    </tr>
                                                    <tr>
                                                        <td id="tdinpricepackage">${requestScope.maxID}
                                                            <input hidden="" name="packageIdAdd" value="${requestScope.maxID}">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <div class="dropdown">
                                                                <button id="selectButtonSubjectName" class="btn-default element-to-take-5" style="width: 220px; height: 50px" type="button" data-toggle="dropdown">
                                                                    Please Select</button>
                                                                <ul class="dropdown-menu scrollable1">
                                                                    <li class="input-container-todata">
                                                                        <input oninput="searchPackagePriceSubjectName(this)" type="text" style="width: 270px;margin-top: 5px; border: 2px solid #cccccc" value="">
                                                                        <i class="fa fa-search" aria-hidden="true"></i>
                                                                    </li>
                                                                    <li id="packagecontent">
                                                                        <c:forEach items="${requestScope.normalSList}" var="nsl">
                                                                            <a href="#" onclick="changeSubjectName('${nsl.course_Name}', '${nsl.id_Course}')">${nsl.course_Name}</a>
                                                                        </c:forEach>
                                                                    </li>
                                                                </ul>
                                                                <input hidden="" type="text" name="coursewillnotnullagain" id="isthisnotnull" value="">
                                                            </div>
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" name="pricePackageDuration" type="text" style="width: 80px" value="">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" name="pricePackageListPrice" type="text" style="width: 80px" value="">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" name="pricePackageSalePrice" type="text" style="width: 80px" value="">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <div class="dropdown">
                                                                <button id="selectedStatus" class="btn element-to-take-status" type="button" data-toggle="dropdown">
                                                                    Select
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><button class="btn" type="button" onclick="AddStatusPP(false)">Deactive</button></li>
                                                                    <br>
                                                                    <li><button class="btn" type="button" onclick="AddStatusPP(true)">Active</button></li>
                                                                </ul>
                                                                <input hidden="" id="pricePackageStatus" value="">
                                                            </div>
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input hidden="" id="addfunctionforpp" name="addfunctionpp" value="">
                                                            <button type="submit" class="btn gradient green" onclick="validateFormAdd(event)">Add</button>&nbsp;&nbsp;&nbsp;&nbsp;
                                                            <button class="btn gradient red" onclick="cancel(${requestScope.suId}, ${requestScope.tag})">Cancel</button>
                                                        </td>
                                                    <input hidden="" type="text" name="tagvalue" value="${requestScope.tag}">
                                                    <input hidden="" type="text" name="suIdvalue" value="${requestScope.suId}">
                                                    </tr>
                                                </table>
                                            </div>
                                            <div class="form-group col-6">
                                                <h6>Search Package Price</h6>
                                                <input class="form-control" oninput="searchPackagePriceAtFirst(this)" placeholder="Enter package price name"><i class="fa fa-search" id="inputfilterpackageprice" aria-hidden="true"></i>
                                            </div>
                                            <c:set value="${requestScope.cPicking}" var="c"/>
                                            <c:set value="${requestScope.pPicking}" var="p"/>
                                            <c:set value="${requestScope.forForeach}" var="psize"/>
                                            <div class="form-group col-12">
                                                <c:if test="${sessionScope.user.id_role == 1}">
                                                    <a href="#" style="position: relative; left: 820px; width: 150px" class="btn gradient green" onclick="showLessonNumber()"><strong>Add</strong></a>
                                                </c:if>
                                                <div class="container">
                                                    <div style="text-align: left;" class="row">
                                                        <div class="col-md-12">
                                                            <div class="table-wrap">
                                                                <table class="table table-striped">
                                                                    <thead>
                                                                        <tr>
                                                                            <th>Number</th>
                                                                            <th>Package</th>
                                                                            <th>Duration</th>
                                                                            <th>List Price</th>
                                                                            <th>Sale Price</th>
                                                                            <th>Status</th>
                                                                                <c:if test="${sessionScope.user.id_role == 1}">
                                                                                <th>Action</th>
                                                                                </c:if>
                                                                        </tr>
                                                                    </thead>
                                                                    <tbody id="searchcontent">
                                                                        <c:set value="${(requestScope.tag - 1) * 5}" var="j"/>
                                                                        <c:forEach begin="1" end="${psize}" var="i">
                                                                            <c:set value="${j+1}" var="e"/>
                                                                            <c:set value="${e}" var="j"/>
                                                                            <tr>
                                                                                <th style="width: 5%" scope="row">${e}</th>
                                                                                <td style="width: 20%">${c[i-1].course_Name}</td>
                                                                                <td style="width: 10%">${p[i-1].duration}</td>
                                                                                <td>${p[i-1].list_Price}&nbsp;$</td>
                                                                                <td>${p[i-1].sale_Price}&nbsp;$</td>
                                                                                <td>${p[i-1].status == "true" ? "Active" : "Deactive"}</td>
                                                                                <c:if test="${sessionScope.user.id_role == 1}">
                                                                                    <td style="width: 20%">
                                                                                        <button value="edit" class="btn" onclick="editCourseNumber(${i})">Edit</button>
                                                                                        <button type="button" class="btn gradient brown" onclick="doDeletePackagePriceAdmin(${p[i-1].id_Package})">Delete</button>
                                                                                    </td>
                                                                                </c:if>
                                                                            </tr>
                                                                        </c:forEach>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div>
                                                    <c:set value="${requestScope.PrevLoca}" var="pl" />
                                                    <c:set value="${requestScope.nextLoca}" var="nl" />
                                                    <div class="pagination-bx rounded-sm gray clearfix">
                                                        <ul class="pagination">
                                                            <c:if test="${requestScope.suId != null}">
                                                                <c:if test="${requestScope.tag != 1}">
                                                                    <li class="previous"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=1"><i class="ti-angle-double-left"></i>First</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag != 1}">
                                                                    <li class="previous"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.tag - 1}"><i class="ti-arrow-left"></i> Prev</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == 1}">
                                                                        <c:forEach begin="1" end="3" var="i">        
                                                                        <li class="${requestScope.tag == i ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${i}">${i}</a></li>
                                                                        </c:forEach> 
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == 2}">
                                                                    <li class="${pl}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == 2 ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=2">2</a></li>
                                                                    <li class="${nl}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag >= 3 && requestScope.tag <= requestScope.endP - 2}">
                                                                    <li class="${pl == 1 ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.nLocation ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.tag}">${requestScope.tag}</a></li>
                                                                    <li class="${nl == 1 ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == requestScope.endP - 1}">
                                                                    <li class="${pl}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.endP - 1 ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.endP - 1}">${requestScope.endP - 1}</a></li>
                                                                    <li class="${nl}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == requestScope.endP}">
                                                                    <li class="${pl-1}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${pl-1}">${pl-1}</a></li>
                                                                    <li class="${pl}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.endP ? "active" : ""}"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.endP}">${requestScope.endP}</a></li>

                                                                </c:if>
                                                                <c:if test="${requestScope.tag < requestScope.endP}">
                                                                    <li class="next"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.tag + 1}">Next <i class="ti-arrow-right"></i></a></li>
                                                                        </c:if>
                                                                        <c:if test="${requestScope.tag < requestScope.endP}">
                                                                    <li class="next"><a href="packagepriceadmin?subjectid=${requestScope.suId}&&pplistid=${requestScope.endP}"><i class="ti-angle-double-right"></i>Last</a></li>
                                                                    </c:if>
                                                                </c:if>
                                                                <c:if test="${requestScope.suId == null}">
                                                                    <c:if test="${requestScope.tag != 1}">
                                                                    <li class="previous"><a href="packagepriceadmin?pplistid=1"><i class="ti-angle-double-left"></i>First</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag != 1}">
                                                                    <li class="previous"><a href="packagepriceadmin?pplistid=${requestScope.tag - 1}"><i class="ti-arrow-left"></i> Prev</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == 1}">
                                                                        <c:forEach begin="1" end="3" var="i">        
                                                                        <li class="${requestScope.tag == i ? "active" : ""}"><a href="packagepriceadmin?pplistid=${i}">${i}</a></li>
                                                                        </c:forEach> 
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == 2}">
                                                                    <li class="${pl}"><a href="packagepriceadmin?pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == 2 ? "active" : ""}"><a href="packagepriceadmin?pplistid=2">2</a></li>
                                                                    <li class="${nl}"><a href="packagepriceadmin?pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag >= 3 && requestScope.tag <= requestScope.endP - 2}">
                                                                    <li class="${pl == 1 ? "active" : ""}"><a href="packagepriceadmin?pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.nLocation ? "active" : ""}"><a href="packagepriceadmin?pplistid=${requestScope.tag}">${requestScope.tag}</a></li>
                                                                    <li class="${nl == 1 ? "active" : ""}"><a href="packagepriceadmin?pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == requestScope.endP - 1}">
                                                                    <li class="${pl}"><a href="packagepriceadmin?pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.endP - 1 ? "active" : ""}"><a href="packagepriceadmin?pplistid=${requestScope.endP - 1}">${requestScope.endP - 1}</a></li>
                                                                    <li class="${nl}"><a href="packagepriceadmin?pplistid=${nl}">${nl}</a></li>
                                                                    </c:if>
                                                                    <c:if test="${requestScope.tag == requestScope.endP}">
                                                                    <li class="${pl-1}"><a href="packagepriceadmin?pplistid=${pl-1}">${pl-1}</a></li>
                                                                    <li class="${pl}"><a href="packagepriceadmin?pplistid=${pl}">${pl}</a></li>
                                                                    <li class="${requestScope.tag == requestScope.endP ? "active" : ""}"><a href="packagepriceadmin?pplistid=${requestScope.endP}">${requestScope.endP}</a></li>

                                                                </c:if>
                                                                <c:if test="${requestScope.tag < requestScope.endP}">
                                                                    <li class="next"><a href="packagepriceadmin?pplistid=${requestScope.tag + 1}">Next <i class="ti-arrow-right"></i></a></li>
                                                                        </c:if>
                                                                        <c:if test="${requestScope.tag < requestScope.endP}">
                                                                    <li class="next"><a href="packagepriceadmin?pplistid=${requestScope.endP}"><i class="ti-angle-double-right"></i>Last</a></li>
                                                                    </c:if>
                                                                </c:if>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <c:forEach begin="1" end="${psize}" var="i">
                                        <form id="PackagePriceForm${i}" action="packagepriceadmin" method="post">

                                            <input hidden="" name="editfunctionpp" value="edit">
                                            <div class="form-group col-12" id="editfunction${i}" style="display: none">
                                                <h6>Edit a package price</h6>
                                                <table style="border-collapse: collapse;">
                                                    <tr>
                                                        <th id="thinpricepackageNumber">Number</th>
                                                        <th id="thinpricepackagePackage">Package</th>
                                                        <th id="thinpricepackageDuration">Duration</th>
                                                        <th id="thinpricepackageDuration">List Price</th>
                                                        <th id="thinpricepackageDuration">Sale Price</th>
                                                        <th id="thinpricepackageDuration">Status</th>
                                                        <th id="thinpricepackagePackage">Action</th>        
                                                    </tr>
                                                    <tr>
                                                        <td id="tdinpricepackage">${i}
                                                            <input hidden="" name="editId${i}" type="text" value="${p[i-1].id_Package}">
                                                            <input hidden="" name="ivalue" type="text" value="${i}">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <div class="dropdown">
                                                                <textarea required="" type="text" name="editName${i}" value="${c[i-1].course_Name}">${c[i-1].course_Name}</textarea>
                                                                <input hidden="" type="text" name="editNameId${i}" value="${c[i-1].id_Course}">
                                                            </div>
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" type="text" name="editDuration${i}" style="width: 80px" value="${p[i-1].duration}">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" type="text" name="editListPrice${i}" style="width: 80px" value="${p[i-1].list_Price}">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input required="" type="text" name="editSalePrice${i}" style="width: 80px" value="${p[i-1].sale_Price}">
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <div class="dropdown">
                                                                <button class="element-to-take-edit${p[i-1].id_Package} btn gradient green" type="button" data-toggle="dropdown">
                                                                    ${p[i-1].status == "true" ? "Active" : "Deactive"}
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li><button type="button" onclick="editStatusPP(false, ${p[i-1].id_Package}, ${p[i-1].id_Package})">Deactive</button></li>
                                                                    <li><button type="button" class="btn gradient green" onclick="editStatusPP(true, ${p[i-1].id_Package}, ${p[i-1].id_Package})">Active</button></li>
                                                                </ul>
                                                                <input hidden="" name="editStatus${i}" id="editPPStatus${p[i-1].id_Package}" value="${p[i-1].status}">
                                                            </div>
                                                        </td>
                                                        <td id="tdinpricepackage">
                                                            <input hidden="" id="editfunctionforpp" name="editfunctionpp" value="">
                                                            <button type="submit" class="btn" onclick="validateFormEdit(${i})">Update</button>
                                                            <button class="btn gradient red" onclick="cancel(${requestScope.suId}, ${requestScope.tag})">Cancel</button>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </form>
                                    </c:forEach>
                                </div>
                                <div id="menu2" class="container tab-pane fade"><br>
                                    <h3>Menu 2</h3>
                                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Your Profile Views Chart END-->
            </div>

        </div>
    </main>

    <div class="ttr-overlay"></div>

    <!-- External JavaScripts -->
    <script src="assestsAdmin/js/jquery.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
    <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
    <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
    <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
    <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
    <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
    <script src="assestsAdmin/vendors/masonry/filter.js"></script>
    <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
    <script src='assestsAdmin/vendors/scroll/scrollbar.min.js'></script>
    <script src="assestsAdmin/js/functions.js"></script>
    <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
    <script src="assestsAdmin/js/admin.js"></script>

</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>