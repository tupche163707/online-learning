/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */


function openAdd() {
    var formOpen = document.getElementById('addForm');
    formOpen.style.display = 'block';
}

function cancel() {
    var formOpen = document.getElementById('addForm');
    window.location.reload();
}

function openEdit(id) {
    var formOpen = document.getElementById('editForm' + id);
    formOpen.style.display = 'block';
}

function showValue(i) {
    document.getElementById("myDropdown"+i).style.display = "block";
}
function changeValue(i) {
    var selectBox = document.getElementById("mySelect"+i);
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    document.getElementById("myInput"+i).value = selectedValue;
    document.getElementById("myDropdown"+i).style.display = "none";
}

function deleteEdit(id){
    if(confirm("Are you sure to delete Price Package With ID: "+id)){
        window.location="subjectdimensiondelete?id="+id; 
    }
}

function deleteEditAdmin(id){
    if(confirm("Are you sure to delete Price Package With ID: "+id)){
        window.location="subjectdimensiondeleteadmin?id="+id; 
    }
}

function searchSubjecetDimensionName(text) {
    var txtSearch = text.value;
    $.ajax({
        url: "/projectswp391/searchsubjectdimensionnameajax",
        type: "get",
        data: {
            txt: txtSearch
        },
        success: function (data1) {
            var row = document.getElementById("subjectnamesearch");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}