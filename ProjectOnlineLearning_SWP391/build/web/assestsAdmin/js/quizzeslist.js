/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/ClientSide/javascript.js to edit this template
 */
function openAdd() {
    var formOpen = document.getElementById('addForm');
    formOpen.style.display = 'block';
}
function cancel() {
    var formOpen = document.getElementById('addForm');
    window.location.reload();
}
function deleteQuiz(idquiz, curunpet) {
    if (confirm("Are you sure want to delete this quiz of id_Quiz = " + idquiz + " and all results of it?")) {
        $.ajax({
            url: "/projectswp391/deletequiz",
            type: "get",
            data: {
                id_Quiz: idquiz,
                page: curunpet
            },
            success: function (response) {
                reloadP();
//                location.reload();
//                var x = document.getElementById("qlist-f");
//                x.innerHTML = response;
//                x.setAttribute("display", "block");

            },
            error: function (xhr) {

            }
        });
    }
}

function myFunction() {
    var x = setInterval(showNotification("Delete successfully!", 5000), 5000);
    clearInterval(x);
}

function onloadquiz() {
    var reloading = sessionStorage.getItem("reloading");
    if (reloading) {
        sessionStorage.removeItem("reloading");
        myFunction();
    }
}

function reloadP() {
    sessionStorage.setItem("reloading", "true");
    document.location.reload();
}

function showNotification(message, duration) {
    var notification = document.getElementById('notification');
    notification.innerHTML = message;
    notification.style.display = 'block';
    var x = document.getElementById("myAudio");
    x.play();
    var y = setTimeout(function () {
        notification.style.display = 'none';
    }, duration);
}

function autoSubmitForm(value) {
    document.getElementById("pginput").setAttribute("value", value);
    document.getElementById("pagination-f").submit();
}


function searchQuizName(text, page) {
    var txtSearch = text.value;
    var paging = document.getElementById("paging").value;
    var quizlevelid = document.getElementById("qlvinput").value;
    var subjectid = document.getElementById("subinput").value;

    $.ajax({
        url: "/projectswp391/searchquizname",
        type: "get",
        data: {
            txtSearch: txtSearch,
            page: page,
            paging: paging,
            quizlevelid: quizlevelid,
            subjectid: subjectid
        },
        success: function (data1) {
            var row = document.getElementById("qlist-f");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}

function setpagingCheckbox(checked) {
    var ckbx = document.getElementById("paging-cbx");
    var ipt = document.getElementById("paging");
    if (ckbx.checked) {
        ipt.setAttribute("value", checked);
    } else {
        ipt.setAttribute("value", checked);
    }
    autoSubmitForm(1);
}

function setQuizLevel(value) {
    var tmp = document.getElementById("qlvinput");
    tmp.setAttribute("value", value);
}
function setSliderStatus(value) {
    var tmp = document.getElementById("slstatus");
    tmp.setAttribute("value", value);
}
function setSubject(value) {
    var tmp = document.getElementById("subinput");
    tmp.setAttribute("value", value);
}

function sortQuizTable(colorder) {
    var table, rows, switching, i, x, y, shouldSwitch;
    table = document.getElementById("qlist-f");
    switching = true;
    /*Make a loop that will continue until
     no switching has been done:*/
//                var rows = table.rows;
//                alert((rows[1].getElementsByTagName("TD")[3]).innerHTML.toLowerCase());
    while (switching) {
        //start by saying: no switching is done:
        switching = false;
        rows = table.rows;
        /*Loop through all table rows (except the
         first, which contains table headers):*/
        for (i = 0; i < (rows.length - 1); i++) {
            //start by saying there should be no switching:
            shouldSwitch = false;
            /*Get the two elements you want to compare,
             one from current row and one from the next:*/
            x = rows[i].getElementsByTagName("TD")[colorder];
            y = rows[i + 1].getElementsByTagName("TD")[colorder];
            //check if the two rows should switch place:
            if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                //if so, mark as a switch and break the loop:
                shouldSwitch = true;
                break;
            }
        }
        if (shouldSwitch) {
            /*If a switch has been marked, make the switch
             and mark that a switch has been done:*/
            rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
            switching = true;
        }
    }

}

function searchSlider(text, page) {
    var txtSearch = text.value;
    var paging = document.getElementById("paging").value;
    $.ajax({
        url: "/projectswp391/searchslider",
        type: "get",
        data: {
            txtSearch: txtSearch,
            page: page,
            paging: paging
        },
        success: function (data1) {
            var row = document.getElementById("qlist-f");
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}

function previewImage(inputId, imageId) {
    var input = document.getElementById(inputId);
    var previewImage = document.getElementById(imageId);

    if (input.files && input.files[0]) {
        var file = input.files[0];
        var fileExtension = file.name.substring(file.name.lastIndexOf('.')).toLowerCase();
        var allowedExtensions = ['.jpg', '.png'];

        if (allowedExtensions.includes(fileExtension)) {
            var reader = new FileReader();

            reader.onload = function (e) {
                previewImage.src = e.target.result;
            };

            reader.readAsDataURL(file);
        } else {
            // Đuôi tệp tin không hợp lệ, xử lý theo nhu cầu của bạn
            console.log('Chỉ cho phép chọn tệp tin có đuôi .jpg hoặc .png.');
        }
    }
}


function changeSliderStatus(id) {
//    var btn = document.getElementById("chg-btn" + id);
//    if (btn.innerHTML === "hidden") {
//        btn.innerHTML = "published";
//    } else if (btn.innerHTML === "published") {
//        btn.innerHTML = "hidden";
//    }
    $.ajax({
        url: "/projectswp391/changesliderstatus",
        type: "get",
        data: {
            sliderid: id
        },
        success: function (data1) {
            var row = document.getElementById("chg-btn" + id);
            row.innerHTML = data1;
        },
        error: function (xhr) {

        }
    });
}

function onloadSlidersList() {
    var addMess = document.getElementById("addMess");
    addMess.setAttribute("background-color", "red");
    addMess.setAttribute("color", "white");
    addMess.style.display = 'block';
    setTimeout(function () {
        addMess.style.display = 'none';
    }, 5000);
}

$('#reversebtn').on('click', function () {
    var tbody = $('table tbody');
    tbody.html($('.revertable', tbody).get().reverse());
});