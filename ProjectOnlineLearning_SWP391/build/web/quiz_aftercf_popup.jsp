<%-- 
    Document   : newjsp
    Created on : Jun 18, 2023, 12:55:44 AM
    Author     : Naviank
--%>
<%--<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quiz confirm popup</title>
    </head>
    <body>
        <c:set var="cur_quiz" value="${requestScope.cur_quiz}"/>
        <c:set var="cur_less" value="${sessionScope.cur_less}"/>

        <div id="congrat" >
            <p>Congratulations! You have finish the quiz! </p>
            <div class="">
                ${requestScope.cur_less.id_Subject}
                <button onclick="closePopupAndRedirect('reviewdetails?')">View result</button>
                <button onclick="closePopupAndRedirect('quizlesson?id_lesson=${cur_quiz.id_Lession}')">Try other quiz</button>
                <button onclick="closePopupAndRedirect('lessonview?${cur_less.id_Subject}')">Back to lesson</button>
            </div>
        </div>
        <script 
            src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js">

        </script>

<!--        <script>
            function changeLayout() {
                document.getElementById("cfsubmit").style.display = "none";
                document.getElementById("congrat").style.display = "block";
                $.ajax({
                    url: "/projectswp391/quizsubmit",
                    type: "get", //send it through get method
                    data: {
                    },
                    success: function (response) {
                        //Do Something
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            }
        </script>-->

        <script 
            src="assets/js/PopUp.js">
        </script>
    </body>
</html>
