<%-- 
    Document   : addcourse
    Created on : Jun 13, 2023, 7:19:56 PM
    Author     : PhanQuangHuy59
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <!-- Mirrored from educhamp.themetrades.com/demo/admin/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:08:15 GMT -->
    <head>
        <!-- META ============================================= -->
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta
            property="og:description"
            content="EduChamp : Education HTML Template"
            />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no" />

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link
            rel="shortcut icon"
            type="image/x-icon"
            href="assets/images/favicon.png"
            />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <!--[if lt IE 9]>
          <script src="assets/js/html5shiv.min.js"></script>
          <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css" />
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/vendors/calendar/fullcalendar.css"
            />

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css" />

        <!-- SHORTCODES ============================================= -->
        <link
            rel="stylesheet"
            type="text/css"
            href="assets/css/shortcodes/shortcodes.css"
            />

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css" />
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css" />
        <link
            class="skin"
            rel="stylesheet"
            type="text/css"
            href="assets/css/color/color-1.css"
            />
        <link rel="stylesheet" href="assets/css/dashboardmarketing.css" />
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.6.0/Chart.min.js"></script>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="assets/js/PopUp.js"></script>
        <style>
            .wc-item{
                text-align: center
            }
            .wc-item h5{
                font-size: 20px;

            }
            .notificationmess{
                width: 100%;
                text-align: center;
                height: 500px;
                background-color: rgba(213, 217, 221,0.4);
                margin: 20px 0px;
                align-items: center;
                font-size: 60px;
                font-weight: bold;
                opacity: 0.4;
                padding-top: 20%;
            }
            .pagination {
                display: inline-block;
                margin-left: 50px auto;
                display: flex;
                justify-content: center;
                margin-left: 37% ;

            }
            .pagination a {
                color: black;
                font-size: 22px;
                float: left;
                padding: 8px 16px;
                border-radius: 4px;
                text-decoration: none;
            }
            .pagination a.active {
                background-color: rgb(93, 4, 176);
                border: 1px solid black;
                color: white;
            }
            .pagination a:hover:not(.active) {
                background-color: rgb(93, 4, 176);
            }
            #flag{
                background-color: orange;
                color: white;
                border: 1px solid black;
            }
        </style>
    </head>
    <body class="ttr-opened-sidebar ttr-pinned-sidebar">
        <!-- header start -->
        <header class="ttr-header">
            <div class="ttr-header-wrapper">
                <!--sidebar menu toggler start -->
                <div class="ttr-toggle-sidebar ttr-material-button">
                    <i class="ti-close ttr-open-icon"></i>
                    <i class="ti-menu ttr-close-icon"></i>
                </div>
                <!--sidebar menu toggler end -->
                <!--logo start -->
                <div class="ttr-logo-box">
                    <div>
                        <a href="index.html" class="ttr-logo">
                            <img
                                class="ttr-logo-mobile"
                                alt=""
                                src="assets/images/logo-mobile.png"
                                width="30"
                                height="30"
                                />
                            <img
                                class="ttr-logo-desktop"
                                alt=""
                                src="assets/images/logo-white.png"
                                width="160"
                                height="27"
                                />
                        </a>
                    </div>
                </div>
                <!--logo end -->
                <div class="ttr-header-menu">
                    <!-- header left menu start -->
                    <ul class="ttr-header-navigation">

                    </ul>
                    <!-- header left menu end -->
                </div>
                <div class="ttr-header-right ttr-with-seperator">
                    <!-- header right menu start -->
                    <ul class="ttr-header-navigation">
                        <li>
                            <a href="#" class="ttr-material-button">${sessionScope.user.full_Name}</a>
                        </li>

                        <li>
                            <a href="#" class="ttr-material-button ttr-submenu-toggle"
                               ><span class="ttr-user-avatar"
                                   ><img
                                        alt=""
                                        src="${sessionScope.user.avatar}"
                                        width="32"
                                        height="32" /></span
                                ></a>
                            <div class="ttr-header-submenu">
                                <ul>
                                    <li><a onclick="openPopup1('profilemarketing1')">My profile</a></li>
                                    <li><a onclick="openPopup1('profilemarketingchagepass')">Change Password</a></li>
                                    <li><a href="logout">Logout</a></li>
                                </ul>
                            </div>
                        </li>
                        <li style="background-color: orange">
                            <a href="logout" class="ttr-material-button">Logout</a>
                        </li>
                    </ul>
                    <!-- header right menu end -->
                </div>
            </div>
        </header>
        <!-- header end -->
        <!-- Left sidebar menu start -->
        <div class="ttr-sidebar">
            <div class="ttr-sidebar-wrapper content-scroll">
                <!-- side menu logo start -->
                <div class="ttr-sidebar-logo">
                    <a href="#"
                       ><img alt="" src="assets/images/logo.png" width="122" height="27"
                          /></a>
                    <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                                                  <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                                                  <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                                          </div> -->
                    <div class="ttr-sidebar-toggle-button">
                        <i class="ti-arrow-left"></i>
                    </div>
                </div>
                <!-- side menu logo end -->
                <!-- sidebar menu start -->
                <nav class="ttr-sidebar-navi">
                    <ul>
                        <li>
                            <a href="dashboardmarketing" class="ttr-material-button">
                                <span id="active" class="ttr-icon"
                                      ><i class="ti-home"></i
                                    ></span>
                                <span id="active"  class="ttr-label">Dashboard</span>
                            </a>
                        </li>
                        <li>
                            <a href="sliderslist" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-book"></i></span>
                                <span class="ttr-label">Sliders List</span>
                            </a>
                        </li>

                        <li>
                            <a href="postlistmarketing" class="ttr-material-button">
                                <span  class="ttr-icon"
                                       ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span  class="ttr-label">Posts List</span>
                            </a>
                        </li>

                        <li>
                            <a href="" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-user"></i></span>
                                <span class="ttr-label">My Profile</span>
                                <span class="ttr-arrow-icon"
                                      ><i class="fa fa-angle-down"></i
                                    ></span>
                            </a>
                            <ul>
                                <li>
                                    <a onclick="openPopup1('profilemarketing1')" class="ttr-material-button"
                                       ><span class="ttr-label">User Profile</span></a
                                    >
                                </li>
                                <li>
                                    <a onclick="openPopup1('profilemarketingchagepass')" class="ttr-material-button"
                                       ><span class="ttr-label">Change Password</span></a
                                    >
                                </li>
                                <li>
                                    <a href="logout" class="ttr-material-button"
                                       ><span class="ttr-label">Logout</span></a
                                    >
                                </li>
                            </ul>
                        </li>
                        <li class="ttr-seperate"></li>
                    </ul>
                    <!-- sidebar menu end -->
                </nav>
                <!-- sidebar menu end -->
            </div>
        </div>
        <!-- Left sidebar menu end -->

        <!--Main container start -->
        <main class="ttr-wrapper">
            <div class="container-fluid">
                <div class="db-breadcrumb">
                    <h4 class="breadcrumb-title">Marketing</h4>
                    <ul class="db-breadcrumb-list">
                        <li><a href="dashboardmarketing"><i class="ti-book"></i>Dashboard Marketings</a></li>
                    </ul>
                </div>
                <!-- Card -->
                <div class="row">
                    <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                        <div class="widget-card widget-bg1">
                            <div class="wc-item">
                                <h5 id="content1">Number of System Subject</h5>
                                <div class="content2">
                                    <h3 class="content2_1">${requestScope.numbercourse}</h3>
                                    <h3 class="content2_1">Course</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                        <div class="widget-card widget-bg2">
                            <div class="wc-item">
                                <h5 id="content1">Number of Registrations</h5>
                                <div class="content2">
                                    <h3 class="content2_1">${requestScope.numberregister}</h3>
                                    <h3 class="content2_1">Registrations</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                        <div class="widget-card widget-bg3">
                            <div class="wc-item">
                                <h5 id="content1">Successful Registrations</h5>
                                <div class="content2">
                                    <div class="content2">
                                        <h3 class="content2_1">${requestScope.numberregissucces}</h3>
                                        <h3 class="content2_1">Registrations</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3 col-xl-3 col-sm-6 col-12">
                        <div class="widget-card widget-bg4">
                            <div class="wc-item">
                                <h5 id="content1">Total Revenue Of System</h5>
                                <div class="content2">
                                    <h3 class="content2_1">${requestScope.revenue}</h3>
                                    <h3 class="content2_1">$</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container_content">
                <div class="container_left">
                    <div class="menuThongKe">
                        <div style="text-align: center">
                            <div style="${(requestScope.flag  == 7)?"color:orange":""}"><p>Click below to view:</p></div>
                            <div id ="${requestScope.flag == 7?"flag":""}" class="newregistration">
                                <a href="dashboardmarketing?flag=7">Recently newly registered</a>
                            </div>
                        </div>

                        <div class="dropdown"style="${(requestScope.flag  == 1 || requestScope.flag == 2)?"color:orange":""}">
                            <div style="text-align: center">
                                <p>Filter By Subject : </p>
                                <button class="dropdown-btn ${(requestScope.flag  == 1 || requestScope.flag == 2)?"dropdown-btn1":""}">Subject Statistics <span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span></button>
                            </div>

                            <div class="dropdown-content">
                                <a id ="${requestScope.flag == 1?"flag":""}" href="dashboardmarketing?flag=1">New Subject</a>
                                <a id ="${requestScope.flag == 2?"flag":""}" href="dashboardmarketing?flag=2">All Subject</a>
                                <button class="closeMenu" onclick="closeAllDropdowns()">
                                    X
                                </button>
                            </div>
                        </div>

                        <div style="border: 1px solid black;display: flex; justify-content: space-between;" class ="${(requestScope.flag == 3 || requestScope.flag == 4|| requestScope.flag == 5|| requestScope.flag == 6)?"dropdown-btn1":""}">

                            <div class="dropdown">

                                <div style="text-align: center">
                                    <p style="color: red;font-size:20px;">${requestScope.mess1}</p>
                                    <p>Filter By Status Register : </p><!-- comment -->
                                    <button class="dropdown-btn ${(requestScope.flag == 3 || requestScope.flag == 4|| requestScope.flag == 5|| requestScope.flag == 6)?"dropdown-btn1":""}">New Registration Statistics<span class="ttr-arrow-icon"><i class="fa fa-angle-down"></i></span></button>
                                </div>


                                <div class="dropdown-content">
                                    <a id ="${requestScope.flag == 3?"flag":""}" href="dashboardmarketing?flag=3">Subject Registrations Success</a>
                                    <a id ="${requestScope.flag == 4?"flag":""}" href="dashboardmarketing?flag=4">Subject Registrations Canceled</a>
                                    <a id ="${requestScope.flag == 5?"flag":""}" href="dashboardmarketing?flag=5">Subject Registrations Submitted</a>
                                    <a id ="${requestScope.flag == 6?"flag":""}" href="dashboardmarketing?flag=6">All Subject Registrations</a>
                                    <button class="closeMenu" onclick="closeAllDropdowns()">
                                        X
                                    </button>
                                </div>
                            </div>
                            <div style="margin-left: 20px">
                                <div class="tktheongay">
                                    <form action="dashboardmarketing">
                                        <div class="submitdate" >
                                            <h5>Filter By Day</h5>
                                            <button type="submit"> Filter</button>
                                        </div>
                                        <input type="hidden" value="${requestScope.idcate}" name="idcategory"/>
                                        <input type="hidden" name="flag" value="${requestScope.flag}"/>
                                        <div class="setDate setdatefrom">
                                            <h7>Set Date From :</h7>
                                            <input required type="date" name="dateFrom1" value="${requestScope.dateFrom1}"/>
                                        </div>
                                        <div class="setDate setdateto">
                                            <h7>Set Date To :</h7>
                                            <input required type="date" name="dateTo1" value="${requestScope.dateTo1}"/>
                                        </div>
                                    </form>
                                </div>
                            </div>     
                        </div>
                    </div>
                    <c:set var="listcourse" value="${requestScope.listcourse}"/>
                    <c:set var="listexpert" value="${requestScope.listexpert}"/>
                    <c:set var="listrevenue" value="${requestScope.listrevenuecourse}"/>

                    <!-- Registration -->
                    <c:set var="listregistration" value="${requestScope.listregistration}"/>
                    <c:set var="listuser" value="${requestScope.listuser}"/>
                    <c:set var="listcourse1" value="${requestScope.listcourse1}"/>
                    <c:set var="listpackage" value="${requestScope.listpackage}"/>
                    <c:set var="liststatus" value="${requestScope.liststatus}"/>
                    <c:set var="listnumber" value="${requestScope.listnumberregistration}"/>
                    <c:if test="${listcourse.size() == 0 && (requestScope.flag == 1 || requestScope.flag == 2)}">
                        <div class="notificationmess">
                            No course registrations yet
                        </div>
                    </c:if>
                    <c:if test="${(listregistration== null ||listcourse1 == null ||listregistration.size() == 0 || listcourse1.size() == 0 )&& (requestScope.flag == 3 || requestScope.flag == 4 ||
                                  requestScope.flag == 5 || requestScope.flag == 6 || requestScope.flag == 7 || requestScope.flag == 8|| requestScope.flag == 9 || requestScope.flag == 10)}">

                          <div class="notificationmess">
                              No course registrations yet
                          </div>
                    </c:if>

                    <c:if test="${listregistration.size() > 0 || listcourse.size() > 0}">
                        <div class="bangthongke">
                            <!--for demo wrap-->
                            <h1>Statistical tables</h1>
                            <div class="container">
                                <div style="text-align: left;" class="row">
                                    <div class="col-md-12">
                                        <div class="table-wrap">

                                            <table class="table table-striped">
                                                <thead>
                                                    <tr>
                                                        <c:if test="${requestScope.flag == 1 || requestScope.flag == 2}">
                                                            <th>ID Subject</th>
                                                            <th>Name Subject</th>
                                                            <th>Create Date Subject</th>
                                                            <th>Teacher of Subject</th>
                                                            <th>Number of Registration</th>
                                                            <th>Revenue of Subject</th>
                                                            <th>Status Subject</th>
                                                            </c:if>
                                                            <c:if test="${requestScope.flag == 3 || requestScope.flag == 4 ||
                                                                          requestScope.flag == 5 || requestScope.flag == 6 || requestScope.flag == 7 
                                                                          || requestScope.flag == 8|| requestScope.flag == 9 || requestScope.flag == 10}">
                                                            <th>ID Registration</th>
                                                            <th>Name Course Register</th>
                                                            <th>Register Date</th>
                                                            <th>Package Price Register</th>
                                                            <th>Duration</th>
                                                            <th>Subscriber Course</th>
                                                                <c:if test="${requestScope.flag != 5}">
                                                                <th>Status Registration</th>
                                                                </c:if>

                                                        </c:if>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:if test="${requestScope.flag == 1 || requestScope.flag == 2}">
                                                        <c:forEach begin="0" end="${listcourse.size() - 1}" var="i">
                                                            <tr>
                                                                <th scope="row">${listcourse.get(i).id_Course}</th>
                                                                <td>${listcourse.get(i).course_Name}</td>
                                                                <td>${listcourse.get(i).formatDate()}</td>
                                                                <td>${listexpert.get(i).full_Name}</td>
                                                                <td>${listnumber.get(i)}</td>
                                                                <td>${listrevenue.get(i)} $</td>
                                                                <td><a href="#" class="btn ${listcourse.get(i).status?"btn-success":"btn-warning"}">${listcourse.get(i).status?"Public":"Unpublic"}</a></td>
                                                            </tr>

                                                        </c:forEach>
                                                    </c:if>
                                                    <c:if test="${requestScope.flag == 3 || requestScope.flag == 4 ||
                                                                  requestScope.flag == 5 || requestScope.flag == 6 
                                                                  || requestScope.flag == 7|| requestScope.flag == 8|| requestScope.flag == 9 || requestScope.flag == 10}">
                                                        <c:forEach begin="0" end="${listregistration.size() - 1}" var="i">
                                                            <tr>
                                                                <th>${listregistration.get(i).id_Register}</th>
                                                                <td>${listcourse1.get(i).course_Name}</td>
                                                                <td>${listregistration.get(i).formatDateRegistration()}</td>
                                                                <td>${listpackage.get(i).sale_Price} $</td>
                                                                <c:if test="${listpackage.get(i).duration > 12}">
                                                                    <td>No time limit</td>
                                                                </c:if>
                                                                <c:if test="${listpackage.get(i).duration <=12}">
                                                                    <td>${listpackage.get(i).duration} Month</td>
                                                                </c:if>
                                                                <td>${listuser.get(i).full_Name}</td>


                                                                <c:if test="${requestScope.flag != 5}">
                                                                    <td>
                                                                        <a href="#" class="btn ${liststatus.get(i)?"btn-success":"btn-warning"}">
                                                                            ${liststatus.get(i)?"Finished":"Unfinished"}
                                                                        </a>
                                                                    </td>
                                                                </c:if>

                                                            </tr>

                                                        </c:forEach>

                                                    </c:if>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:set var="page" value="${requestScope.page}"/>
                    <div class="pagination col-3">
                        <c:forEach begin="${1}" end="${requestScope.num1}" var="i">
                            <a class="${(page == i)?"active":""}" href="dashboardmarketing?flag=${requestScope.flag}&dateFrom=${requestScope.from}&dateTo=${requestScope.to}&idcategory=${requestScope.idcate}&page=${i}">${i}</a>   
                        </c:forEach>
                    </div>
                    <div class="thongkeandmenu">
                        <div class="ketquathongke">
                            <div class="tttk">
                                <div class="title">
                                    <h5>Number Total Registrations : </h5>
                                </div>
                                <div class="ttkq">${requestScope.totalregistration} Register</div>
                            </div>
                            <div class="tttk">
                                <div class="title">
                                    <h5>Number of Successful Registrations : </h5>
                                </div>
                                <div class="ttkq">${requestScope.totalregistrationsuccess} Register</div>
                            </div>
                            <div class="tttk">
                                <div class="title">
                                    <h5>Number of Canceled Registrations : </h5>
                                </div>

                                <div class="ttkq">${requestScope.totalregistrationprocess} Register</div>
                            </div>
                            <div class="tttk">
                                <div style="padding-top: 8%;" class="title">
                                    <h5>Revenue Total</h5>
                                </div>
                                <div class="ttkq">${requestScope.totalRevenue} $</div>
                            </div>
                        </div>



                        <div class="container_right">
                            <div class="tktheongay ${(requestScope.flag  == 8)?"dropdown-btn1":""}">

                                <h5>Filter By Day</h5>
                                <form action="dashboardmarketing">
                                    <input type="hidden" name="flag"  value="8"/>
                                    <div class="setDate setdatefrom">
                                        <h7>Date From :</h7>
                                        <input type="date" name="dateFrom" value="${requestScope.from}" />
                                    </div>
                                    <div class="setDate setdateto">
                                        <h7>Date To :</h7>
                                        <input type="date" name="dateTo" value="${requestScope.to}"/>
                                    </div>
                                    <div class="thongkedt">
                                        <input type="submit" value="Statistical">
                                    </div>

                                </form>

                            </div>
                            <div class="doanhthuLoaiKhoa ${(requestScope.flag  == 9)?"dropdown-btn1":""}">

                                <h5>Filter By Subject Category</h5>
                                <form action="dashboardmarketing">
                                    <input type="hidden" name="flag"  value="9"/>
                                    <select name="idcategory" id="" onchange="this.form.submit()">
                                        <option value="" disabled selected hidden>Choose 1 option to Statical</option>
                                        <c:forEach var="category" items="${requestScope.listcater}">
                                            <option ${requestScope.idcate == category.id_Catergory_Course ?"selected":""} value="${category.id_Catergory_Course}">${category.name_Catergory_Course}</option>
                                        </c:forEach>

                                    </select>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="chart">
                <div class="titlechart">Revenue statistics chart</div>
                <div class="optionchart">
                    <!--                    <a class="active1" href="">Statistics by Month</a>
                                        <a href="">Statistics of the last 7 days</a>-->
                </div>
                <div style="margin-top: 20px" class="chartmonthly">
                    <div class=" chartleft">
                        <canvas id="myChart"></canvas>
                    </div>
                    <div class=" chartright1">
                        <canvas id="pie-chart" ></canvas>
                    </div>
                </div>

                <!-- thong ke theo loai khoa hoc -->
                <div style="margin-top: 20px" class="chartmonthly">
                    <div style="height: 200px" class=" chartleft">
                        <canvas style="height: 100%" id="myChart12"></canvas>
                    </div>
                    <div class=" chartright1">
                        <canvas id="pie-chart12" ></canvas>
                    </div>
                </div>


            </div>
        </main>

        <div class="ttr-overlay"></div>
        <div style="height: 100px;width: 100%;background-color: rgb(93, 4, 176);margin-top: 100px"></div>
        <!-- External JavaScripts -->
        <script src="assestsAdmin/js/jquery.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assestsAdmin/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assestsAdmin/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assestsAdmin/vendors/counter/waypoints-min.js"></script>
        <script src="assestsAdmin/vendors/counter/counterup.min.js"></script>
        <script src="assestsAdmin/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assestsAdmin/vendors/masonry/masonry.js"></script>
        <script src="assestsAdmin/vendors/masonry/filter.js"></script>
        <script src="assestsAdmin/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assestsAdmin/vendors/scroll/scrollbar.min.js"></script>
        <script src="assestsAdmin/js/functions.js"></script>
        <script src="assestsAdmin/vendors/chart/chart.min.js"></script>
        <script src="assestsAdmin/js/admin.js"></script>
        <script src="assestsAdmin/vendors/calendar/moment.min.js"></script>
        <script src="assestsAdmin/vendors/calendar/fullcalendar.js"></script>
        <script>
                                        // L?y t?t c? các drop-down buttons
                                        const dropdownButtons = document.querySelectorAll(".dropdown-btn");

                                        // L?p qua các drop-down buttons và thêm s? ki?n click
                                        dropdownButtons.forEach((button) => {
                                            button.addEventListener("click", () => {
                                                // ??m b?o ch? có m?t drop-down ???c m? cùng m?t th?i ?i?m
                                                closeAllDropdowns();

                                                // M? ho?c ?óng drop-down t??ng ?ng
                                                const dropdownContent = button.nextElementSibling;
                                                dropdownContent.style.display =
                                                        dropdownContent.style.display === "block" ? "none" : "block";
                                            });
                                        });

                                        // ?óng t?t c? các drop-down
                                        function closeAllDropdowns() {
                                            const dropdownContents = document.querySelectorAll(".dropdown-content");
                                            dropdownContents.forEach((content) => {
                                                content.style.display = "none";
                                            });
                                        }
                                        // Bieu Do duong doanh thu hang thangv
            <c:set var="color" value="${requestScope.listcolor}"/>
            <c:set var="month" value="${requestScope.titlemonth}"/>
            <c:set var="revenuemonth" value="${requestScope.chartmonth}"/>
            <c:set var="revenuemonthcircle" value="${requestScope.chartmonthcircle}"/>
                                        var data = {
                                            labels: [
                                                "${month[0]}", "${month[1]}", "${month[2]}", "${month[3]}", "${month[4]}", "${month[5]}",
                                                "${month[6]}", "${month[7]}", "${month[8]}", "${month[9]}", "${month[10]}", "${month[11]}"
                                            ],
                                            datasets: [
                                                {
                                                    label: "Revenue Unit($)",
                                                    data: [${revenuemonth[0]},${revenuemonth[1]},${revenuemonth[2]},${revenuemonth[3]},${revenuemonth[4]},${revenuemonth[5]},
            ${revenuemonth[6]},${revenuemonth[7]},${revenuemonth[8]},${revenuemonth[9]},${revenuemonth[10]},${revenuemonth[11]}],
                                                    fill: true,
                                                    borderColor: "orange",

                                                    tension: 0.1,

                                                },
                                            ],
                                        };

                                        // Tùy ch?n cho bi?u ??
                                        var options = {
                                            scales: {
                                                y: {
                                                    beginAtZero: true,
                                                },
                                            },
                                            title: {
                                                display: true,
                                                text: "Monthly revenue chart",
                                                fontSize: 25,

                                            },
                                        };

                                        // T?o bi?u ?? ???ng
                                        var ctx = document.getElementById("myChart").getContext("2d");
                                        var myChart = new Chart(ctx, {
                                            type: "line",
                                            data: data,
                                            options: options,

                                        });

                                        // bieu do tron doanh thu
                                        new Chart(document.getElementById("pie-chart"), {
                                            type: "pie",
                                            data: {
                                                labels: ["${month[0]}", "${month[1]}", "${month[2]}", "${month[3]}", "${month[4]}", "${month[5]}",
                                                    "${month[6]}", "${month[7]}", "${month[8]}", "${month[9]}", "${month[10]}", "${month[11]}"],
                                                datasets: [
                                                    {
                                                        label: "Unit %",

                                                        data: [${revenuemonthcircle[0]},${revenuemonthcircle[1]},${revenuemonthcircle[2]},${revenuemonthcircle[3]},${revenuemonthcircle[4]},${revenuemonthcircle[5]},
            ${revenuemonthcircle[6]},${revenuemonthcircle[7]},${revenuemonthcircle[8]},${revenuemonthcircle[9]},${revenuemonthcircle[10]},${revenuemonthcircle[11]}],
                                                        backgroundColor: [
                                                            "${color[0]}", "${color[1]}", "${color[2]}", "${color[3]}", "${color[4]}", "${color[5]}",
                                                            "${color[6]}", "${color[7]}", "${color[8]}", "${color[9]}", "${color[10]}", "${color[11]}"
                                                        ],

                                                        borderWidth: 1,
                                                        borderColor: '#777',
                                                        hoverBorderWidth: 3,
                                                        hoverBorderColor: '#000'
                                                    },
                                                ],
                                            },
                                            options: {
                                                title: {
                                                    display: true,
                                                    fontSize: 25,
                                                    text: "Percentage revenue of monthly",
                                                },
                                            },
                                        });

                                        // bieu do tron thu 2
                                        new Chart(document.getElementById("pie-chart12"), {
                                            type: "pie",
                                            data: {
                                                labels: [${requestScope.lablecate}],
                                                datasets: [
                                                    {
                                                        label: "%",
                                                        backgroundColor: [${requestScope.colorcate}
                                                        ],
                                                        data: [${requestScope.solieu}],
                                                        borderWidth: 1,
                                                        borderColor: '#777',
                                                        hoverBorderWidth: 3,
                                                        hoverBorderColor: '#000'
                                                    },
                                                ],
                                            },
                                            options: {
                                                title: {
                                                    display: true,
                                                    fontSize: 25,
                                                    text: "revenue statistics chart by course category",
                                                },
                                            },
                                        });
                                        // thong ke theo loaij san pham
                                        let myChart12 = document.getElementById('myChart12').getContext('2d');
                                        // Global Options
                                        let massPopChart = new Chart(myChart12, {
                                            type: 'bar', // bar, horizontalBar, pie, line, doughnut, radar, polarArea
                                            data: {
                                                labels: [${requestScope.lablecate}],
                                                datasets: [{
                                                        label: ['Unit $'],
                                                        data: [${requestScope.solieucot}
                                                        ],
                                                        //backgroundColor:'green',
                                                        backgroundColor: [
            ${requestScope.colorcate}
                                                        ],
                                                        borderWidth: 1,
                                                        borderColor: '#777',
                                                        hoverBorderWidth: 3,
                                                        hoverBorderColor: '#000'
                                                    }]
                                            },
                                            options: {
                                                title: {
                                                    display: true,
                                                    text: 'Statistical chart of revenue by course category',
                                                    fontSize: 25
                                                },
                                                legend: {
                                                    display: true,
                                                    position: 'right',
                                                    labels: {
                                                        fontColor: '#000'
                                                    }
                                                },
                                                layout: {
                                                    padding: {
                                                        left: 50,
                                                        right: 0,
                                                        bottom: 0,
                                                        top: 0
                                                    }
                                                },
                                                tooltips: {
                                                    enabled: true
                                                }
                                            }
                                        });
        </script>
    </body>
</html>
