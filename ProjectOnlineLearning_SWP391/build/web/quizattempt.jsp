<%-- 
    Document   : attemptquiz
    Created on : Jun 3, 2023, 8:13:36 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="Online Learning System - Group 2 SE1714" />

        <!-- OG -->
        <meta property="og:title" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:description" content="Online Learning System - Group 2 SE1714" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>Online Learning System - Group 2 SE1714</title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- VanPK CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/vanpk_style.css">

    </head>
    <c:set var="start_time" value="${sessionScope.start_time}"/>
    <c:set var="limit_time" value="${sessionScope.limit_time}"/>
    <body onload="callTimerServlet(${limit_time});">
<!--        <span>${start_time}</span>
        <span>${limit_time}</span>-->
        <div class="page-wraper">
            <div class="quiz-screen">
                <c:set var="cur_quiz" value="${sessionScope.cur_quiz}"/>
                <c:set var="cur_number" value="${requestScope.cur_number}"/>
                <c:set var="cur_answer_string" value="${sessionScope.cur_answer_string}"/>

                <div class="nana">
                    Start at:
                    <span id="quiz-starttime">${sessionScope.start_time}</span>
                    Duration:
                    <span id="quiz-timelimit">${cur_quiz.time_Limit}</span>
                    String answer:
                    <span id="abcxyz123">${cur_answer_string}</span>
                    To:
                    <span id="to"></span>
                    Quiz ID:
                    <span id="quiz-id">${cur_quiz.id_Quiz}</span>
                </div>
                <div class="quiz-pos">
                    <!--<button >Start Quiz</button>-->
                    <div class="quiz-homelogo"> 
                        <img src="assets/images/logo-white.png" alt="">
                    </div>
                    <span class="quiz-back"> 
                        <i class="fa fa-home"></i>
                        <a href="home">Back to Home</a>
                    </span>
                    <span class="quiz-position"> 
                        <i class="fa fa-location-arrow"></i>
                        <span>${cur_number}/${cur_quiz.number_Question}</span>
                    </span>
                    <span class="quiz-time">
                        <i class="fa fa-clock-o"></i>
                        <span id="remaining-time"></span>
                    </span>
                </div>
                <c:set var="cur_question" value="${requestScope.cur_question}"/>
                <c:set var="cur_answerlist" value="${requestScope.cur_answerlist}"/>
                <div class="quiz-content">
                    <div class="question-content">
                        <div class="quiz-quesid">
                            <div class="number">
                                <span>
                                    ${cur_less.name_Lesson}
                                </span>                            
                            </div>
                            <div class="qid">
                                <span>
                                    Subject: ${cur_subject.subject_Name}
                                </span>                            
                            </div>
                        </div>
                        <div class="quiz-quesid">
                            <div class="number">
                                <span>Question number: ${cur_number}</span>
                            </div>
                            <div class="qid">
                                <span>Quiz name: ${cur_quiz.name_Quiz}</span>
                            </div>
                        </div>
                        <div class="quiz-qa">
                            <div class="quiz-question">
                                <p>Question content: ${cur_question.content_Question}</p>
                            </div>
                            <div class="quiz-answer">
                                <ul>
                                    <c:set var="cur_ncorrect" value="${requestScope.cur_ncorrect}"/>
                                    <c:set var="cur_apos" value="${requestScope.cur_apos}"/>
                                    <p class="">Choose ${cur_ncorrect} ${cur_ncorrect == 1 ? "answer" : "answers"}</p>
                                    <c:if test="${cur_ncorrect > 1}">
                                        <c:forEach items="${cur_answerlist}" var="cur_answer">
                                            <li>
                                                <div class="answer">
                                                    <c:if test="${(cur_answer.answer_Content.charAt(0)) != cur_answer_string.charAt(cur_apos)}">
                                                        <input type="checkbox" 
                                                               name="ans" 
                                                               value="${cur_answer.answer_Content.charAt(0)}" 
                                                               onclick="modifyCheckboxAnswer(${cur_ncorrect})"> 
                                                    </c:if>
                                                    <c:if test="${(cur_answer.answer_Content.charAt(0)) == cur_answer_string.charAt(cur_apos)}">
                                                        <input type="checkbox" 
                                                               name="ans" 
                                                               checked
                                                               value="${cur_answer.answer_Content.charAt(0)}" 
                                                               onclick="modifyCheckboxAnswer(${cur_ncorrect})"> 
                                                    </c:if>
                                                    <span>
                                                        &emsp;${cur_answer.answer_Content}
                                                    </span>
                                                    <c:set var="cur_apos" value="${cur_apos + 1}"/>
                                                </div>   
                                            </li>
                                        </c:forEach>
                                    </c:if>

                                    <c:if test="${cur_ncorrect == 1}">
                                        <c:forEach items="${cur_answerlist}" var="cur_answer">
                                            <li>
                                                <div class="answer">
                                                    <c:if test="${(cur_answer.answer_Content.charAt(0)) == cur_answer_string.charAt(cur_apos)}">
                                                        <input type="radio" 
                                                               name="ans" 
                                                               checked
                                                               value="${cur_answer.answer_Content.charAt(0)}" 
                                                               onclick="modifyAnswer(this.value, ${cur_apos})"> 
                                                    </c:if>
                                                    <c:if test="${(cur_answer.answer_Content.charAt(0)) != cur_answer_string.charAt(cur_apos)}">
                                                        <%--<c:if test="${('*') == cur_answer_string.substring(cur_apos, cur_apos + 1)}">--%>
                                                        <input type="radio" 
                                                               name="ans" 
                                                               value="${cur_answer.answer_Content.charAt(0)}" 
                                                               onclick="modifyAnswer(this.value, ${cur_apos})"> 
                                                    </c:if>
                                                    <span>
                                                        &emsp;${cur_answer.answer_Content}
                                                    </span>
                                                    <c:set var="cur_apos" value="${cur_apos + 1}"/>
                                                </div>   
                                            </li>
                                        </c:forEach>
                                    </c:if>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="quiz-nav">
                    <div class="quiz-move">
                        <c:if  test="${cur_number > 1}">
                            <button 
                                onclick="location.href = 'attemptquiz?cur_number=${cur_number - 1}'">
                                <i class="ti-arrow-left"></i> Prev
                            </button>
                        </c:if>
                        <c:if test="${cur_number < cur_quiz.number_Question}">
                            <button 
                                onclick="location.href = 'attemptquiz?cur_number=${cur_number + 1}'">
                                Next <i class="ti-arrow-right"></i>
                            </button>
                        </c:if>  
                    </div>
                    <c:set var="boolarr" value="${requestScope.boolarr}"/>
                    <div class="quiz-review">
                        <c:forEach begin="1" end="${cur_quiz.number_Question}" var="i">
                            <c:if test="${boolarr[i - 1]}">
                                <button onclick="location.href = 'attemptquiz?cur_number=${i}'" 
                                        class="btn-type1">
                                    ${i}
                                </button>
                            </c:if>
                            <c:if test="${!boolarr[i - 1]}">
                                <button onclick="location.href = 'attemptquiz?cur_number=${i}'" 
                                        class="btn-type2">
                                    ${i}
                                </button>
                            </c:if>
                        </c:forEach>
                    </div>
                    <div  class="quiz-util">
                        <button
                            onclick="openPopUp('peekatanswer?id_question=${cur_question.id_Question}', 400, 300)">
                            Peek At Answer: ${sessionScope.peek_time} left
                        </button>
                        <button
                            onclick="openPopUp('confirm', 600, 400)">
                            Submit Answer
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="assets/js/quizattempt.js"></script>
        <script 
            src="assets/js/PopUp.js">
        </script>

        <script>
            function modifyAnswer() {
                var checkBoxes = document.querySelectorAll('input[type="radio"]'); // get all the check box
                var res = "";
                checkBoxes.forEach(item => {
                    if (item.checked) {
                        res += item.value;
                    } else {
                        res += "*";
                    }
                });
                $.ajax({
                    url: "/projectswp391/modify",
                    type: "get", //send it through get method
                    data: {
                        value: res,
                        cur_number: ${cur_number}
                    },
                    success: function (response) {
                    },
                    error: function (xhr) {
                    }
                });
            }
        </script>
        <script>
            function modifyCheckboxAnswer(ncorrect) {
                var checkBoxes = document.querySelectorAll('input[type="checkbox"]'); // get all the check box
                var res = "";
                checkBoxes.forEach(item => {
                    if (item.checked) {
                        res += item.value;
                    } else {
                        res += "*";
                    }
                });
                $.ajax({
                    url: "/projectswp391/modify",
                    type: "get", //send it through get method
                    data: {
                        value: res,
                        cur_number: ${cur_number}
                    },
                    success: function (response) {
                        //Do Something
                    },
                    error: function (xhr) {
                        //Do Something to handle error
                    }
                });
            }
        </script>
        <script>
            var d = sessionStorage.getItem("limit_time");
        </script>
    </body>
</html>
