<%-- 
    Document   : blogdetails
    Created on : May 21, 2023, 2:20:56 AM
    Author     : GangsterCao
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">

    <head>

        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="assets/images/favicon.ico" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">

    </head>
    <body id="bg">
        <div class="page-wraper">
            <div id="loading-icon-bx"></div>

            <!-- Header Top ==== -->
            <%@ include file="header.jsp" %>
            <!-- header END ==== -->
            <!-- Content -->
            <div class="page-content bg-white">
                <!-- inner page banner -->
                <div class="page-banner ovbl-dark" style="background-image:url(assets/images/banner/banner2.jpg); height: 150px">
                    <div class="container">
                        <div class="page-banner-entry">
                            <h1 class="text-white" style="margin-top: 50px">Blog Details</h1>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb row -->
                <div class="breadcrumb-row">
                    <div class="container">
                        <ul class="list-inline">
                            <li><a href="home">Home</a></li>
                            <li><a href="bloglist">Blog List</a></li>
                            <li>Blog Details</li>
                        </ul>
                    </div>
                </div>
                <!-- Breadcrumb row END -->
                <div class="content-block">
                    <div class="section-area section-sp1">
                        <div class="container">
                            <div class="row">
                                <!-- Left part start -->
                                <div class="col-lg-8 col-xl-8">
                                    <!-- blog start -->
                                    <c:set value="${requestScope.blog}" var="b"/>
                                    <c:set value="${requestScope.author}" var="a"/>
                                    <c:set value="${requestScope.listhavecatergoryname}" var="n"/>
                                    <div class="recent-news blog-lg">
                                        <div class="action-box blog-lg">
                                            <img src="${b.thumbnail_Blog}" alt="art">
                                        </div>
                                        <div class="info-bx">
                                            <ul class="media-post">
                                                <li><i></i>${n.getName_Catergory_Blog()}</li>&nbsp;&nbsp;
                                                <li><i class="fa fa-calendar"></i>${dayOfUpdateDate}-${b.update_Date}</li>&nbsp;&nbsp;
                                                <li><i class="fa fa-eye"></i>${requestScope.views}</li>&nbsp;&nbsp;
                                                <li><i class="fa fa-user"></i>${a.full_Name}</li>&nbsp;&nbsp;
                                            </ul>
                                            <h5 class="post-title"><a href="#">${b.title}</a></h5>
                                            <p>${b.content_Blog}</p>        
                                            <div class="ttr-divider bg-gray"><i class="icon-dot c-square"></i></div>
                                        </div>
                                    </div>
                                    <!-- blog END -->
                                </div>
                                <!-- Left part END -->
                                <!-- Side bar start -->
                                <div class="col-lg-4 col-xl-4">
                                    <aside  class="side-bar sticky-top">
                                        <div class="widget">
                                            <h6 class="widget-title">Search</h6>
                                            <div class="search-bx style-1">
                                                <form action="bloglist" method="get">
                                                    <div class="input-group">
                                                        <input name="searchvalue" class="form-control" placeholder="Enter your keywords..." type="text">
                                                        <input name="filter" value="${4}" type="hidden">
                                                        <span class="input-group-btn">
                                                            <button type="submit" class="fa fa-search text-primary"></button>
                                                        </span> 
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                        <div class="widget recent-posts-entry">
                                            <h6 class="widget-title">Recent Posts</h6>
                                            <div class="widget-post-bx">
                                                <div class="widget-post clearfix">
                                                    <c:forEach items="${requestScope.listBlog}" var="d" begin="1" end="6">
                                                        <div class="ttr-post-media"> 
                                                            <a href="blogdetail?id_Blog=${d.id_Blog}"><img src="${d.thumbnail_Blog}" width="200" height="143" alt="">  </a>
                                                        </div>
                                                        <div class="ttr-post-info">
                                                            <div class="ttr-post-header">
                                                                <h6 class="post-title">${d.title}</h6>
                                                            </div>
                                                            <ul class="media-post">
                                                                <li><i class="fa fa-calendar"></i>${d.update_Date}</li>
                                                                    <c:if test="${d.id_Blog == b.id_Blog}">
                                                                    <li><i class="fa fa-eye"></i>${requestScope.views}</li>
                                                                    </c:if>
                                                                    <c:if test="${d.id_Blog != b.id_Blog}">
                                                                    <li><i class="fa fa-eye"></i>${d.view}</li>
                                                                    </c:if>
                                                            </ul>
                                                        </div>
                                                        </br>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </div>  
                                        <div class="widget widget_tag_cloud">
                                            <h6 class="widget-title">INTERESTING CATEGORY</h6>
                                            <div class="tagcloud"> 
                                                <c:forEach items="${requestScope.listBlogCategory}" var="c">
                                                    <a href="bloglist?filter=2&cate=${c.id_Catergory_Blog}">${c.name_Catergory_Blog}</a> 
                                                </c:forEach>
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                                <!-- Side bar END -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Content END-->
            <!-- Footer ==== -->
            <p class="phone-icon">
                <a href="https://www.youtube.com/@giaolang" class="fa fa-external-link">Contact link</a>
            </p>
            <%@ include file="footer.jsp" %>
            <!-- Footer END ==== -->
            <!-- scroll top button -->
            <button class="back-to-top fa fa-chevron-up" ></button>
        </div>
        <!-- External JavaScripts -->
        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/vendors/bootstrap/js/popper.min.js"></script>
        <script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
        <script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
        <script src="assets/vendors/counter/waypoints-min.js"></script>
        <script src="assets/vendors/counter/counterup.min.js"></script>
        <script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
        <script src="assets/vendors/masonry/masonry.js"></script>
        <script src="assets/vendors/masonry/filter.js"></script>
        <script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
        <script src="assets/js/functions.js"></script>
        <script src="assets/js/contact.js"></script>
    </body>

</html>
