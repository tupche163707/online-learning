<%-- 
    Document   : PageCourse
    Created on : May 17, 2023, 3:51:55 PM
    Author     : PCT
--%>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:10:19 GMT -->
    <head>

        <script src="assets/js/PopUp.js"></script>
        <!-- META ============================================= -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <meta name="robots" content="" />

        <!-- DESCRIPTION -->
        <meta name="description" content="EduChamp : Education HTML Template" />

        <!-- OG -->
        <meta property="og:title" content="EduChamp : Education HTML Template" />
        <meta property="og:description" content="EduChamp : Education HTML Template" />
        <meta property="og:image" content="" />
        <meta name="format-detection" content="telephone=no">

        <!-- FAVICONS ICON ============================================= -->
        <link rel="icon" href="../error-404.html" type="image/x-icon" />
        <link rel="shortcut icon" type="image/x-icon" href="assets/images/favicon.png" />

        <!-- PAGE TITLE HERE ============================================= -->
        <title>EduChamp : Education HTML Template </title>

        <!-- MOBILE SPECIFIC ============================================= -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!--[if lt IE 9]>
        <script src="assets/js/html5shiv.min.js"></script>
        <script src="assets/js/respond.min.js"></script>
        <![endif]-->

        <!-- All PLUGINS CSS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/assets.css">
        <link rel="stylesheet" type="text/css" href="assets/vendors/calendar/fullcalendar.css">

        <!-- TYPOGRAPHY ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/typography.css">

        <!-- SHORTCODES ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/shortcodes/shortcodes.css">

        <!-- STYLESHEETS ============================================= -->
        <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        <link rel="stylesheet" type="text/css" href="assets/css/dashboard.css">
        <link class="skin" rel="stylesheet" type="text/css" href="assets/css/color/color-1.css">
        <link href="assets/css/SubjectAdmin.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" type="text/css" href="assets/css/page.css">
        <style>
            .active{
                color: orange;
            }
        </style>
    </head>
</head>
<body class="ttr-opened-sidebar ttr-pinned-sidebar">

    <!-- header start -->
    <header class="ttr-header">
        <div class="ttr-header-wrapper">
            <!--sidebar menu toggler start -->
            <div class="ttr-toggle-sidebar ttr-material-button">
                <i class="ti-close ttr-close-icon"></i>
                <i class="ti-menu ttr-open-icon"></i>
            </div>
            <!--sidebar menu toggler end -->
            <!--logo start -->
            <div class="ttr-logo-box">
                <div>
                    <a href="index.html" class="ttr-logo">
                        <img alt="" class="ttr-logo-mobile" src="assets/images/logo-mobile.png" width="30" height="30">
                        <img alt="" class="ttr-logo-desktop" src="assets/images/logo-white.png" width="160" height="27">
                    </a>
                </div>
            </div>
            <!--logo end -->
            <div class="ttr-header-menu">
                <!-- header left menu start -->
                <!-- header left menu end -->
            </div>
            <div class="ttr-header-right ttr-with-seperator">
                <!-- header right menu start -->
                <ul class="ttr-header-navigation">

                    <c:if test="${sessionScope.user!=null}">

                        <li> 
                            <a href="userprofile">
                                <img 
                                    src="${sessionScope.user.avatar}" 
                                    style="height: 48px; width: 48px; overflow: hidden; border-radius: 50%;"
                                    alt="${sessionScope.user.full_Name}"/>
                            </a>
                        </li>
                        <li><a href="userprofile">${user.full_Name}</a></li>
                        <li><a href="logout">Logout</a></li>

                    </c:if> 
                </ul>
                <!-- header right menu end -->
            </div>
            <!--header search panel start -->
            <div class="ttr-search-bar">
                <form class="ttr-search-form">
                    <div class="ttr-search-input-wrapper">
                        <input type="text" name="qq" placeholder="search something..." class="ttr-search-input">
                        <button type="submit" name="search" class="ttr-search-submit"><i class="ti-arrow-right"></i></button>
                    </div>
                    <span class="ttr-search-close ttr-search-toggle">
                        <i class="ti-close"></i>
                    </span>
                </form>
            </div>
            <!--header search panel end -->
        </div>
    </header>
    <!-- header end -->
    <!-- Left sidebar menu start -->
    <div class="ttr-sidebar">
        <div class="ttr-sidebar-wrapper content-scroll">
            <!-- side menu logo start -->
            <div class="ttr-sidebar-logo">
                <a href="#"><img alt="" src="assets/images/logo.png" width="122" height="27"></a>
                <!-- <div class="ttr-sidebar-pin-button" title="Pin/Unpin Menu">
                        <i class="material-icons ttr-fixed-icon">gps_fixed</i>
                        <i class="material-icons ttr-not-fixed-icon">gps_not_fixed</i>
                </div> -->
                <div class="ttr-sidebar-toggle-button">
                    <i class="ti-arrow-left"></i>
                </div>
            </div>
            <!-- side menu logo end -->
            <!-- sidebar menu start -->
            <nav class="ttr-sidebar-navi">
                <c:set var="currentpage" value="${requestScope.xxx}"/>
                <ul>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="admincourselist" class="ttr-material-button">
                                <span id="active"  class="ttr-icon"><i class="ti-book"></i></span>
                                <span  id="active" class="ttr-label">Course Management</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                        <a href="adminsubject" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                        </a>
                    </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==2}">
                        <li>
                        <a href="expertsubjectlistservlet" class="ttr-material-button">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label ${currentpage eq "SubjectManagment"? 'active':""}">Subject Management</span>
                        </a>
                    </li>
                    </c:if>
                    <c:if test = "${sessionScope.user.id_role==1}">
                        <li>
                            <a href="useradmin" class="ttr-material-button">
                                <span class="ttr-icon"
                                      ><i class="ti-layout-list-post"></i
                                    ></span>
                                <span class="ttr-label">User Management</span>
                            </a>
                        </li>
                    </c:if>
                    <li>
                        <a href="" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label active">Quizzes Management</span>
                        </a>
                    </li>
                    <li style="margin-left: 60px">
                            <a href="subjectdetails?subjectid=${requestScope.suId}" class="ttr-material-button">
                                <span class="ttr-icon"><i class="ti-pencil-alt"></i></span>
                                <span class="ttr-label active">Quiz Detail</span>
                            </a>
                        </li>
                    <li>
                        <a href="listquestions" class="ttr-material-button ">
                            <span class="ttr-icon"><i class="ti-book"></i></span>
                            <span class="ttr-label">Questions Management</span>
                        </a>
                    </li>
                    <li class="ttr-seperate"></li>
                </ul>
            </nav>
            <!-- sidebar menu end -->
        </div>
    </div>
    <!-- Left sidebar menu end -->

    <!--Main container start -->
    <main class="ttr-wrapper">
        <div class="container-fluid">
            <div class="db-breadcrumb">
                <h4 class="breadcrumb-title">Quiz Management</h4>
                <ul class="db-breadcrumb-list">
                    <a><li>Quizzes Detail</li></a>
                </ul>
            </div>	
            <div class="row">
                <!-- Your Profile Views Chart -->
                <div class="col-lg-12 m-b30">
                    <div class="widget-box">

                        <div class="search-bar">
                            <h4>Quiz Detail</h4>
                        </div>
                        <div class="widget-inner">
                            <div class="card-courses-list admin-courses" >
                                <div style="display: block">
                                    <div class="card-courses-media">
                                        <img src="${requestScope.subject.image_subject}" alt=""/>
                                        
                                    </div>
                                    <button class="btn" style="margin-top: 50px;margin-left: 20%; border-radius: 5px" class="btn" data-toggle="modal" href="#edit-quiz">Edit Quiz</button>
                                </div>
                                <div class="card-courses-full-dec" style="margin-left: 10%">
                                    <div class="" style="width: 720px">
                                        <div class="form-group row">
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-10 ml-auto">
                                                <h3>1. Quiz Detail</h3>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Quiz's ID:</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.quiz.id_Quiz}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Quiz's Name:</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.quiz.name_Quiz}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Number of Questions:</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.quiz.number_Question}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Duration</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.quiz.time_Limit}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Pass rate of Quiz:</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.quiz.passpercent}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Lesson's Quiz</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.lesson}</div>
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Subject's Quiz</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <div class="form-control">${requestScope.subject.subject_Name}</div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Last Update Date:</label>
                                            <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                                                <c:if test="${requestScope.quiz.update_Date==null}"><div class="form-control">${requestScope.quiz.createDate}</div></c:if>
                                                <c:if test="${requestScope.quiz.update_Date!=null}"><div class="form-control">${requestScope.quiz.update_Date}</div></c:if>
                                            </div>
                                        </div>
                                        <a href="quizquestions?qid=1" class="btn" style="margin-top: 50px;margin-left: 60%">More details</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Your Profile Views Chart END-->
        </div>
    </div>
</main>

<div class="modal fade" id="edit-quiz" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="margin-top: 100px">
    <div class="modal-dialog" role="document">
        <div class="modal-content clearfix">
            <right><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><div class="form-control" style="background-color: red; color: black">X</div></span></button></right>
            <form action="quizdetail" method="post" class="edit-profile" style="width: 600px">
                <div class="modal-body">
                    <div class="profile-head">
                        <h4>Edit Quiz</h4>
                    </div>
                    <input type="hidden" name="qid" value="${requestScope.quiz.id_Quiz}"/>
                    <div class="form-group row" style="margin-top: 5px">
                        <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Quiz's Name:</label>
                        <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                            <input class="form-control" name="name" type="text" value="${requestScope.quiz.name_Quiz}" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Duration</label>
                        <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                            <input class="form-control" name="dur" type="text" value="${requestScope.quiz.time_Limit}" required/>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-12 col-sm-3 col-md-3 col-lg-2 col-form-label">Pass rate of Quiz:</label>
                        <div class="col-12 col-sm-9 col-md-9 col-lg-7">
                            <input class="form-control" name="rate" type="text" value="${requestScope.quiz.passpercent}" required/>
                        </div>
                    </div>
                        <button class="btn" onclick="return alertSave()" style="width: 120px">Save changes</button>
                    <button class="btn" data-dismiss="modal" aria-label="Close" style="width: 120px;margin-left: 25%"><span aria-hidden="true" >Cancel</span></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="ttr-overlay"></div>

<!-- External JavaScripts -->
<script src="assets/js/jquery.min.js"></script>
<script src="assets/vendors/bootstrap/js/popper.min.js"></script>
<script src="assets/vendors/bootstrap/js/bootstrap.min.js"></script>
<script src="assets/vendors/bootstrap-select/bootstrap-select.min.js"></script>
<script src="assets/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.js"></script>
<script src="assets/vendors/magnific-popup/magnific-popup.js"></script>
<script src="assets/vendors/counter/waypoints-min.js"></script>
<script src="assets/vendors/counter/counterup.min.js"></script>
<script src="assets/vendors/imagesloaded/imagesloaded.js"></script>
<script src="assets/vendors/masonry/masonry.js"></script>
<script src="assets/vendors/masonry/filter.js"></script>
<script src="assets/vendors/owl-carousel/owl.carousel.js"></script>
<script src='assets/vendors/scroll/scrollbar.min.js'></script>
<script src="assets/js/functions.js"></script>
<script src="assets/vendors/chart/chart.min.js"></script>
<script src="assets/js/admin.js"></script>
<script src="assets/js/filterquestion.js" type="text/javascript"></script>
<script src="assets/alertDelete.js" type="text/javascript"></script>
<script src="assets/js/contact.js"></script>
<script src="assets/alertEditQuestion.js" type="text/javascript"></script>
</body>

<!-- Mirrored from educhamp.themetrades.com/demo/admin/courses.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 22 Feb 2019 13:11:35 GMT -->
</html>