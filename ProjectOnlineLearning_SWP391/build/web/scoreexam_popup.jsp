<%-- 
    Document   : scoreexam_popup
    Created on : Jun 4, 2023, 2:01:28 PM
    Author     : Naviank
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Score exam pop-up</title>
    </head>
    <body>
        <h1>Score exam!?</h1>
        <p class="description">
            ${requestScope.message}
        </p>
        <div class="score-button">
            <button onclick="closeWindowAndPreventSubmit(event)">Back</button>
            <button onclick="closePopupAndRedirect('test?quiz_id=${requestScope.quiz_id}&remain=')">Score exam</button>
        </div>

        <!--<script src="assets/js/quizattempt.js"></script>-->
        <script src="assets/js/PopUp.js"></script>        
    </body>
</html>
